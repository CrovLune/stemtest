// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo

using System;
using ST.BL.Domain.Partijen;

namespace ST.UI.CA.ExtensionMethods
{
    internal static class ExtensionMethods
    {
        internal static string GetInfo(this Partij p)
        {
            return $"[{p.VolledigeNaam}({p.Naam})] \n[{p.Beschrijving}]\n";
        }
    }
}