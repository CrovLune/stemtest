"use strict";
import * as signalR from '@microsoft/signalr';

var connection = new signalR.HubConnectionBuilder().withUrl("/partijKeuzeHub").build();


connection.invoke("PartijKeuze", sessieId, partijId, cookie);
document.getElementById("keuzePartij").addEventListener("click", function (event) {
    var partijId = document.getElementById("partijId").textContent;
    var sessieId = document.getElementById("sessieId").textContent;
    var cookie = document.getElementById("cookie").textContent;
    var location =document.getElementById("location").textContent;
    connection.invoke("PartijKeuze", sessieId, partijId, cookie,location);

})

document.getElementById("startSpel").addEventListener("click", function (event) {
    var location = document.getElementById("location").textContent;
    var sessieId = document.getElementById("sessieId").textContent;
    var cookie = document.getElementById("cookie").textContent;
    var locationTeacher = document.getElementById("locationTeacher").textContent;

    sessieId = Number(sessieId);
    console.log(`sessieId ${sessieId} ${location}`);
    connection.invoke("OpenFirstQuestion", sessieId, cookie);
    connection.invoke("StartSpel", sessieId, location, locationTeacher).catch(function (err) {
        return console.error(err.toString());
    })
});
connection.on("startGame", function (location) {
    console.log(`show me ${location}`);
    window.location.replace(location);
});