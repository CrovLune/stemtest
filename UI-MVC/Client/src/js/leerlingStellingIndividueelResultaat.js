console.log("leerlingStellingIndividueelResultaat loaded");

const eensCol = document.getElementById("eensCol");
const oneensCol = document.getElementById("oneensCol");

const cdv = '../dist/images/cdv.png';
const groen = '../dist/images/groen.png';
const nva = '../dist/images/nva.png';
const openvld = '../dist/images/openvld.png';
const pvda = '../dist/images/pvda.jpg';
const spa = '../dist/images/spa.png';
const vb = '../dist/images/vb.jpg';

const cdvImg = new Image();
cdvImg.src = cdv;
cdvImg.setAttribute("style","padding:10px");

const groenImg = new Image();
groenImg.src = groen;
groenImg.setAttribute("style","padding:10px");

const nvaImg = new Image();
nvaImg.src = nva;
nvaImg.setAttribute("style","padding:10px");

const openvldImg = new Image();
openvldImg.src = openvld;
openvldImg.setAttribute("style","padding:10px");

const pvdaImg = new Image();
pvdaImg.src = pvda;
pvdaImg.setAttribute("style","padding:10px");

const spaImg = new Image();
spaImg.src = spa;
spaImg.setAttribute("style","padding:10px");

const vbImg = new Image();
vbImg.src = vb;
vbImg.setAttribute("style","padding:10px");

document.addEventListener('DOMContentLoaded', addPartijenFotos);

function addPartijenFotos() {
    eensCol.appendChild(pvdaImg);
    eensCol.appendChild(vbImg);
    oneensCol.appendChild(nvaImg);
    oneensCol.appendChild(openvldImg);
}