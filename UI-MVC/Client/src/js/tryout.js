"use strict";
import * as signalR from '@microsoft/signalr';

var connection = new signalR.HubConnectionBuilder().withUrl("/myhub").build();

//*********[BACK] Update Spelers *********/
connection.on("UpdateDeelnemers", (count, location) => {
  console.log(`show me ${location}`);
  console.log("Auth invoked UpdateDeelnemers in JS");
  var span = document.getElementById("studentCount");
  span.innerText = count;
});


//*********[BACK] Start Game *********/
connection.on("Game", (location) => {
  console.log(`show me ${location}`);
  window.location.replace(location);
});



//*********[FRONT] At first Load *********/
connection.start().then(() => {
  var sessieId = document.getElementById("sessieId").textContent;
  var cookie = document.getElementById("cookie").textContent;
  sessieId = Number(sessieId);

  connection.invoke("Auth", sessieId, cookie)
  console.log("Connection Start + Auth Provoked");
}).catch(function (err) {
  return console.error(err.toString());
});


//*********[FRONT] Start Button Click Functionality *********/
var startButton = document.getElementById("startSpel");
startButton.addEventListener("click", () => {
  console.log("Start Button Clicked!");
  var location, sessieId, cookie;

  location = document.getElementById("location").textContent;
  sessieId = Number(document.getElementById("sessieId").textContent);
  cookie = document.getElementById("cookie").textContent;

  connection.invoke("OpenFirstQuestion", sessieId, cookie).then(() => {
    console.log("[StartButton] OpeFirstQuestion Invoked");
  });
  connection.invoke("StartSpel", sessieId, location);
  console.log("[StartButton] StartSpel Invoked");
});