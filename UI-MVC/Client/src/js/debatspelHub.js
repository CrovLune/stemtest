const signalR = require("@microsoft/signalr");
let beginKnop = document.getElementById("begin");
let aantal_leerlingen = document.getElementById("aantal_leerlingen");

let volgendeKnop = document.getElementById("volgende");
let overslaanKnop = document.getElementById("overslaan");
let vraag = document.getElementById("vraag");
let speelMeeKnop = document.getElementById("speelMee");
let eens = document.getElementById("eens");
let oneens = document.getElementById("oneens");
let comment = document.getElementById("comment");
let eindeKnop = document.getElementById("einde");
var antwoorden = [];
var argumenten = [];
let position = 0;
var stellingen = [];
let activeSession;

let connection = new signalR.HubConnectionBuilder()
    .withUrl("/DebatspelHub")
    .configureLogging(signalR.LogLevel.Trace)
    .build();

connection.on("Send", data => {
    console.log(data);
});

let startscherm = document.getElementById("startscherm");
let wachtscherm = document.getElementById("wachtscherm");
let stellingscherm = document.getElementById("stellingscherm");

function resetInputs() {
    eens.checked = false;
    oneens.checked = false;
    comment.value = "";
}

function disableVolgende() {
    volgendeKnop.disabled = true;
    document.getElementById("eens").addEventListener("click", function () {
        volgendeKnop.disabled = false;
    });
    document.getElementById("oneens").addEventListener("click", function () {
        volgendeKnop.disabled = false;
    });
}

connection.start()
    .then(() => connection.invoke("Send", "Hello"));
    //.then(() => beginKnop.disabled = true)
    //.then(() => aantal_leerlingen.innerHTML = "1 deelnemer")
    //.then(() => connection.invoke("enteredRoom"));


connection.on("enableBegin", function () {
    beginKnop.disabled = false;
});

connection.on("updateAantal", function (data) {
    aantal_leerlingen.innerHTML = data + " deelnemer(s)";
    console.log(data + " aanwezig");
});

connection.on("DebatspelStart", function (data) {
    console.log("Debatspel");
    activeSession = data;
    startscherm.style.display = "none";
    wachtscherm.style.display = "block";
});

connection.on("VolgendeVraag", function (data) {
    if (eens.checked === true) {
        antwoorden[position] = "eens";
    } else {
        antwoorden[position] = "oneens";
    }
    argumenten[position] = comment.value;

    if (stellingen[position + 1] === undefined){
        volgendeKnop.style.display = "none";
        eindeKnop.style.display = "block";
    } else {
        position++;
        vraag.innerHTML = stellingen[position];
        console.log(antwoorden);
        console.log(argumenten);
    }
    resetInputs();
    document.body.style.backgroundColor = "#fff";
    disableVolgende();
});

connection.on("VraagOverslaan", function (data) {
    antwoorden[position] = "overslaan";
    argumenten[position] = comment.value;

    if (stellingen[position + 1] === undefined){
        volgendeKnop.style.display = "none";
        eindeKnop.style.display = "block";
    } else {
        position++;
        vraag.innerHTML = stellingen[position];
        console.log(antwoorden);
    }
    resetInputs();
    disableVolgende();
});


window.addEventListener('load', function () {
    beginKnop.addEventListener('click', function (event) {
        connection.invoke("LeerlingStart");
        console.log("invoking LeerlingStart");
    });
});

window.addEventListener('load',function () {
    overslaanKnop.addEventListener('click', function (event) {
        connection.invoke("LeerlingOverslaan");
        console.log("invoking LeerlingOverslaan");
    });
});

window.addEventListener('load',function () {
    volgendeKnop.addEventListener('click', function (event) {
        connection.invoke("LeerlingVolgende");
        console.log("invoking LeerlingVolgende");
    });
});

window.addEventListener('load',function () {
    speelMeeKnop.addEventListener('click', function (event) {
        let uniekeCode = document.getElementById('uniekeCode').value;
        console.log(parseInt(uniekeCode));
        connection.invoke("AuthenticateWebUser", parseInt(uniekeCode)).catch(err => console.error(err));
        event.preventDefault();
    });
});

window.addEventListener('load', function () {
    eindeKnop.addEventListener('click', function () {
        connection.invoke("ReceiveAntwoorden", activeSession, antwoorden, argumenten).catch(err => console.log(err));
        eens.disabled = true;
        oneens.disabled = true;
        comment.disabled = true;
        overslaanKnop.disabled = true;
        eindeKnop.disabled = true;
    });
});

window.addEventListener("unload", function () {
    connection.invoke("RemoveFromGroup", activeSession);
});

/*
connection.onclose = function () {
    connection.invoke("RemoveFromGroup", activeSession);
    //connection.invoke("Logout");
};*/

connection.on("ReceiveStellingScherm", function (data) {
    wachtscherm.style.display = "none";
    stellingscherm.style.display = "block";
    stellingen = data;
    position = 0;
    vraag.innerHTML = stellingen[position];
    
    disableVolgende();
});