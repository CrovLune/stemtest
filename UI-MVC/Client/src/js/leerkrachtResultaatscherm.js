google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {

    var data = google.visualization.arrayToDataTable([
        ['Antwoord', 'aantal'],
        ['Eens',     65],
        ['Oneens',      25],
        ['Onbeantwoord',  10]
    ]);

    var options = {
        title: 'Stemmen'
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart'));
    chart.draw(data, options);
}