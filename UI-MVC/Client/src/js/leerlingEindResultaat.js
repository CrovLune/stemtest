

window.addEventListener("load", function () {
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    
    let table = document.getElementById("table");
    let partij1 = [table.rows[0].cells[0].innerText,parseInt(table.rows[0].cells[1].innerText.replace("%",""))];
    let partij2 = [table.rows[1].cells[0].innerText,parseInt(table.rows[1].cells[1].innerText.replace("%",""))];
    let partij3 = [table.rows[2].cells[0].innerText,parseInt(table.rows[2].cells[1].innerText.replace("%",""))];
    let partij4 = [table.rows[3].cells[0].innerText,parseInt(table.rows[3].cells[1].innerText.replace("%",""))];
    let partij5 = [table.rows[4].cells[0].innerText,parseInt(table.rows[4].cells[1].innerText.replace("%",""))];
    let partij6 = [table.rows[5].cells[0].innerText,parseInt(table.rows[5].cells[1].innerText.replace("%",""))];
    console.log(partij1,partij2,partij3,partij4,partij5,partij6);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Partij', 'percentage'],
            [partij1[0], partij1[1]],
            [partij2[0], partij2[1]],
            [partij3[0], partij3[1]],
            [partij4[0], partij4[1]],
            [partij5[0], partij5[1]],
            [partij6[0], partij6[1]],
        ]);

        var options = {
            title: 'Stemmen'
        };

        var chart = new google.visualization.BarChart(document.getElementById('chartPartijen'));
        chart.draw(data, options);
    }
});