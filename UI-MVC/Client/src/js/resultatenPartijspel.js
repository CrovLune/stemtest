﻿"use strict";
import * as signalR from '@microsoft/signalr';

var connection = new signalR.HubConnectionBuilder().withUrl("/resultatenhub").build();

connection.start().then(function () {
    var sessieId = document.getElementById("sessieId").textContent;
    var cookie = document.getElementById("cookie").textContent;
    sessieId = Number(sessieId);
    connection.invoke("Auth", sessieId, cookie)
    console.log(`sessieId ${sessieId}`);
}).catch(function (err) {
    return console.error(err.toString());
});