var newButtonMW, deleteButtonMW, mwcheckbox, i, a, counterMW, counterPA, pacheckbox, deleteButtonPA, newButtonPA;

newButtonMW = document.getElementById("moeilijkWoordButton");
mwcheckbox = document.getElementsByClassName("moeilijkwoordClass");
deleteButtonMW = document.getElementById("moeilijkWoordDeleteButton");

newButtonPA = document.getElementById("partijAntwoordButton");
pacheckbox = document.getElementsByClassName("pacheckboxClass");
deleteButtonPA = document.getElementById("partijAntwoordDeleteButton");

counterMW = 0;
counterPA = 0;

for (i = 0; i < mwcheckbox.length; i++) {
  mwcheckbox[i].addEventListener("change", (event) => {
    if (event.srcElement.checked) {
      counterMW++;
      checkMW();
    } else {
      counterMW--;
      checkMW();
    }
  })
}

function checkMW() {
  if (counterMW > 0) {
    newButtonMW.style.display = 'none'
    deleteButtonMW.style.display = 'block'
  } else {
    newButtonMW.style.display = 'block'
    deleteButtonMW.style.display = 'none'
  }
}

for (i = 0; i < pacheckbox.length; i++) {
  pacheckbox[i].addEventListener("change", (event) => {
    if (event.srcElement.checked) {
      counterPA++;
      checkPA();
    } else {
      counterPA--;
      checkPA();
    }
  })
}

function checkPA() {
  if (counterPA > 0) {
    newButtonPA.style.display = 'none'
    deleteButtonPA.style.display = 'block'
  } else {
    newButtonPA.style.display = 'block'
    deleteButtonPA.style.display = 'none'
  }
}