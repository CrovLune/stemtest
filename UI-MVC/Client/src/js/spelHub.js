const signalR = require("@microsoft/signalr");
let aantal_leerlingen = document.getElementById("aantal_leerlingen");

let vraag = document.getElementById("vraag");

let activeSession = 3;

let connection = new signalR.HubConnectionBuilder()
    .withUrl("/spelHub")
    .configureLogging(signalR.LogLevel.Trace)
    .build();

connection.start()
    .then(() => connection.invoke("Send", "Hello"))
    /*.then(() => connection.invoke("AuthenticateLeerlingWeb", 789))*/
    .then(authenticate)
    .then(leekrachtStartEventListener)
    .catch(function (e) {
        console.error(e);
    });

function authenticate() {
    if (document.location.href === "https://localhost:5001/Speel/Debatspel/Leerkracht/LeerkrachtStartSessie?id=3") {
        console.log("authenticate leerkracht");
        connection.invoke("AuthenticateLeerkracht", 3)
            .catch(function (e) {
                console.error(e);
            })
    } else {
        let spelSessieId = document.getElementById("spelSessieId").value;
        console.log("spelSessieId: "+spelSessieId);
        if (spelSessieId != null) {
            connection.invoke("AuthenticateLeerkracht", spelSessieId)
                .catch(function (e) {
                    console.error(e);
                })
        }
    }

    if (document.location.href === "https://localhost:5001/Speel/Debatspel/Leerling/LeerlingWacht?spelSessieId=3") {
        console.log("authenticate leerling");
        connection.invoke("AuthenticateLeerlingWeb", 789)
            .catch(function (e) {
                console.error(e);
            })
    }
}

connection.on("Send", data => {
    console.log(data);
});

connection.on("LeerkrachtStartTest", () => {
    var leerlingBegin = document.getElementById("leerlingBegin");
    if (leerlingBegin) {
        leerlingBegin.style.display = "block";
    }
});

function leekrachtStartEventListener() {
    var leerkrachtStart = document.getElementById("leerkrachtStart");
    if (leerkrachtStart) {
        leerkrachtStart.addEventListener("click", new function () {
            connection.invoke("LeerkrachtStart", 3);
        });
    }
}

function leekrachtKiestStellingEventListener() {
    var leerkrachtKiestStelling = document.getElementById("leerkrachtKiestStelling");
    if (leerkrachtKiestStelling) {
        leerkrachtKiestStelling.addEventListener("click", new function () {
            connection.invoke("LeerkrachtKiestStelling", 3, 8);
        });
    }
}

connection.on("updateAantal", function (data) {
    aantal_leerlingen.innerHTML = data + " deelnemer(s)";
    console.log(data + " aanwezig");
});

window.addEventListener("unload", function () {
    connection.invoke("RemoveFromGroup", activeSession);
});

/*
connection.onclose = function () {
    connection.invoke("RemoveFromGroup", activeSession);
    //connection.invoke("Logout");
};*/