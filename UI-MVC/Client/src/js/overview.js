﻿import '../css/overview.scss';
function selectAll() {
    var alleChecks = document.getElementsByName('cb');
    for (var i=0; i < alleChecks.length; i++){
        alleChecks[i].checked = true;
    }
}
function deselectAll() {
    var alleChecks = document.getElementsByName('cb');
    for (var i=0; i < alleChecks.length; i++){
        alleChecks[i].checked = false;
    }
}

let checkAlles = document.getElementById('checkAlles');
checkAlles.addEventListener('click', selectAll);
let checkNiets = document.getElementById('checkNiets');
checkNiets.addEventListener('click', deselectAll);
