window.addEventListener('load', function () {
    let table = document.querySelector("#table");
    let buttons = [];
    let antwoorden = [];

    for (let i = 1; i < table.rows.length; i++) {
        if (i % 2){
            buttons.push(table.rows[i].cells[2]);
            antwoorden.push(table.rows[++i]);
        }
    }

    for (let j = 0; j < buttons.length; j++){
        let self = buttons[j];
        let antwoord = antwoorden[j];
        antwoord.style.display = "none";
        self.addEventListener('click', (e) => {
            e.preventDefault();
            if (antwoord.style.display === "block"){
                antwoord.style.display = "none";
            } else {
                antwoord.style.display = "block";
            }
        });
    }
});