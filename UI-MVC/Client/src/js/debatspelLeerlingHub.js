const signalR = require("@microsoft/signalr");


let sessieId = document.getElementById("sessieId");
let aantal_leerlingen = document.getElementById("aantal_leerlingen");

let uniekeCode = document.getElementById("uniekeCode");
let speelMee = document.getElementById("speelMee");

let vraag = document.getElementById("vraag");
let eens = document.getElementById("eens");
let oneens = document.getElementById("oneens");
let comment = document.getElementById("comment");
let overslaan = document.getElementById("overslaan");
let volgende = document.getElementById("volgende");
let antwoord = "";
let stelling;
let leerlingResultatenKnop = document.getElementById("leerlingResultatenKnop");

let leerlingStart = document.getElementById("leerlingStart");
let leerlingWacht = document.getElementById("leerlingWacht");
let leerlingStelling = document.getElementById("leerlingStelling");
let leerlingEindResultaat = document.getElementById("leerlingEindResultaat");
//fix

let connection = new signalR.HubConnectionBuilder()
    .withUrl("/spelHub")
    .configureLogging(signalR.LogLevel.Trace)
    .build();

function speelMeeEventListener() {
    speelMee.addEventListener('click', function (e) {
        e.preventDefault();
        let code = uniekeCode.value;
        console.log("sending invoke Authenticate:" + code);
        connection.invoke("Authenticate", code);
    })
}

function volgendeEventListener() {
    volgende.addEventListener("click", ev => {
        volgende.disabled = true;
        overslaan.disabled = true;
        console.log(antwoord);
        connection.invoke("onLeerlingVolgendeClick", stelling.Id, antwoord);
    })
}

function resultatenEventListener() {
    leerlingResultatenKnop.addEventListener("click", function () {
        leerlingResultatenKnop.disabled = true;
        leerlingWacht.style.display = "none";
        leerlingEindResultaat.style.display = "block";
    });
}

connection.start()
    .then(() => connection.invoke("Send", "Hello"))
    /*.then(authenticate)*/
    /*.then(updateConnection)*/
    .then(speelMeeEventListener)
    .then(volgendeEventListener)
    .then(resultatenEventListener);


connection.on("Send", data => {
    console.log(data);
});

connection.on("updateAantal", function (data) {
    aantal_leerlingen.innerHTML = data + " deelnemer(s)";
    console.log(data + " aanwezig");
});

connection.on("redirect", function (data) {
    console.log(data);
    //leerlingStart = document.getElementById("leerlingStart");
    leerlingStart.style.display = "none";
    //leerlingWacht = document.getElementById("leerlingWacht");
    leerlingWacht.style.display = "block";
});

function updateStelling(data) {
    //vraag = document.getElementById("vraag");
    stelling = JSON.parse(data);
    console.log(stelling);
    vraag.innerHTML = stelling.Vraag;
}

connection.on("receiveEersteStelling", function (data) {
    //let leerlingWacht = document.getElementById("leerlingWacht");
    leerlingWacht.style.display = "none";
    //let leerlingStelling = document.getElementById("leerlingStelling");
    leerlingStelling.style.display = "block";
    let volgende = document.getElementById("volgende");
    volgende.disabled = false;
    //let overslaan = document.getElementById("overslaan");
    overslaan.disabled = false;
    updateStelling(data);
    //eens = document.getElementById("eens");
    eens.addEventListener("click", ev => {
        //ev.preventDefault();
        antwoord = eens.value;
        console.log(antwoord);
    });
    oneens = document.getElementById("oneens");
    oneens.addEventListener("click", ev => {
        antwoord = oneens.value;
        console.log(antwoord);
    });
});

connection.on("leerlingWacht", function () {
    //let leerlingStelling = document.getElementById("leerlingStelling");
    leerlingStelling.style.display = "none";
    //let leerlingWacht = document.getElementById("leerlingWacht");
    leerlingWacht.style.display = "block";
});

connection.on("leerlingEindResultaat", function () {
    console.log("leerlingEindResultaat");
    connection.invoke("OnLeerlingGetResultaat", parseInt(sessieId.value));
    //let leerlingWacht = document.getElementById("leerlingWacht");
    leerlingWacht.style.display = "block";
    //let leerlingResultaat = document.getElementById("leerlingResultaat");
    leerlingResultatenKnop.style.display = "block";
});

window.addEventListener("beforeunload", function (e) {
    console.log("unload: " + parseInt(sessieId.value));
    connection.invoke("RemoveFromGroup", parseInt(sessieId.value));
    (e || window.event).returnValue = null;
    return "Are you sure?";
});

window.onbeforeunload = function (ev) {
    console.log("unload: " + parseInt(sessieId.value));
    connection.invoke("RemoveFromGroup", parseInt(sessieId.value));
    (e || window.event).returnValue = null;
    return "Are you sure?";
};

//--------------------------------------------

function updateConnection() {
    let gebruikerId = document.getElementById("gebruikerId").value;
    let sessieId = document.getElementById("sessieId").value;
    if (gebruikerId !== undefined) {
        console.log("invoking updateConnectionId: " + gebruikerId + sessieId);
        connection.invoke("updateConnectionId", gebruikerId, sessieId);
    }
}

function authenticate() {
    let x = parseInt(sessieId.value);
    console.log("authenticate leerling sessie: " + x);
    connection.invoke("AuthenticateLeerkracht", x)
        .catch(function (e) {
            console.error(e);
        })
}