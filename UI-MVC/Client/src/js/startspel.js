"use strict";
import * as signalR from '@microsoft/signalr';

var connection = new signalR.HubConnectionBuilder().withUrl("/myhub").build();
//*********[BACK] Next Question *********/
connection.on("NextQ", (location) => {
  // console.log(`show me ${location}`);
  window.location.replace(location);
});

//*********[BACK] Prev Question *********/
connection.on("PrevQ", (location) => {
  // console.log(`show me ${location}`);
  window.location.replace(location);
});

// //*********[BACK] Test Afsluiten  *********/
connection.on("Finish", (location) => {
  console.log(`show me ${location}`);
  window.location.replace(location);
});

//*********[FRONT] Save Answer  *********/
var answerButtons = document.getElementsByClassName("answerButton");
var stellingId, value;
for (let index = 0; index < answerButtons.length; index++) {
  const button = answerButtons[index];
  button.addEventListener("click", (event) => {
    stellingId = Number(event.srcElement.getAttribute("id"));
    value = event.srcElement.innerText;
    // console.log(`Button Clicked ${value}`);
  });
}

document.getElementById("argumentButton").addEventListener("click", () => {
  var argumentBoxValue = document.getElementById("answerArgument").value;
  var cookie = document.getElementById("cookie").textContent;

  console.log(`Button Clicked ${value}`);

  connection.invoke("SaveButtonAnswer", cookie, stellingId, value, argumentBoxValue);
});

//*********[FRONT] At first Load *********/
connection.start().then(() => {
  var sessieId = Number(document.getElementById("sessieId").textContent);
  var cookie = document.getElementById("cookie").textContent;

  connection.invoke("Auth", sessieId, cookie)
  console.log("Connection Start + Auth Provoked");
}).catch(function (err) {
  return console.error(err.toString());
});

//*********[FRONT] Test Afsluiten *********/
document.getElementById("testAfsluiten").addEventListener('click', () => {
  console.log("[FRONT] test afsluiten button clicked!")
  var location = document.getElementById("finishGameLocation").textContent;
  var sessieId = Number(document.getElementById("sessieId").textContent);

  connection.invoke("FinishGame", sessieId, location);
});

//*********[FRONT] Next Question Button *********/
document.getElementById("nextQuestion").addEventListener('click', () => {
  console.log("[FRONT] next question button clicked!")
  var location = document.getElementById("location").textContent;
  var sessieId = Number(document.getElementById("sessieId").textContent);

  connection.invoke("NextQ", sessieId, location);
});

//*********[FRONT] Prev Question Button *********/
document.getElementById("prevQuestion").addEventListener('click', () => {
  console.log("[FRONT] prev question button clicked!")
  var location = document.getElementById("location").textContent;
  var sessieId = Number(document.getElementById("sessieId").textContent);

  connection.invoke("PrevQ", sessieId, location);
});