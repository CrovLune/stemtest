"use strict";
import * as signalR from '@microsoft/signalr';

var connection = new signalR.HubConnectionBuilder().withUrl("/spelHub").build();

// const signalR = require("@microsoft/signalr");
let aantal_leerlingen = document.getElementById("aantal_leerlingen");
let sessieId = document.getElementById("sessieId");
let stellingId = document.getElementById("stellingId");
let leerkrachtKiestStelling = document.getElementById("leerkrachtKiestStelling");
let startCard = document.getElementById("startCard");
let currentStellingId = 0;
let stelling;
let ul;
let stellingenDone = 0;
let seId = parseInt(sessieId.value);
let leerlingResultaten = document.getElementById("leerlingResultaten");
let resultatenModal = document.getElementById("resultatenModal");
//fix

// let connection = new signalR.HubConnectionBuilder()
//     .withUrl("/spelHub")
//     .configureLogging(signalR.LogLevel.Trace)
//     .build();

// function authenticate() {
//     let sessieId = document.getElementById("sessieId");
//     let x = parseInt(sessieId.value);
//     console.log("authenticate leerkracht sessie: " + x);
//     connection.invoke("AuthenticateLeerkracht", x)
//         .catch(function (e) {
//             console.error(e);
//         })
// }

// function leekrachtKiestStellingEventListener() {
//     let leerkrachtKiestStelling = document.getElementById("leerkrachtKiestStelling");
//     leerkrachtKiestStelling.addEventListener('click', function () {
//         let sessieId = document.getElementById("sessieId");
//         let seId = parseInt(sessieId.value);
//         let stId = currentStellingId;
//         console.log("sending invoke:" + stId + seId);
//         connection.invoke("LeerkrachtSelecteertEersteStelling", stId, seId);
//         leerkrachtKiestStelling.disabled = true;
//         stellingenDone++;
//     })
// }

// function startCardEventListener() {
//         let sessieId = document.getElementById("sessieId");
//         let seId = parseInt(sessieId.value);
//         connection.invoke("onLeerkrachtStartCardClick", seId)
// }

// function resultatenEventListener() {
//     resultatenModal.addEventListener("click", function () {
//         console.log("invoking onLeerkrachtResultatenClick: "+seId);
//         connection.invoke("onLeerkrachtResultatenClick", seId);
//     });
// }

// connection.start()
//     .then(() => connection.invoke("Send", "Hello"))
//     .then(leekrachtKiestStellingEventListener)
//     .then(authenticate)
//     .then(startCardEventListener)
//     .then(resultatenEventListener);

// connection.on("Send", data => {
//     console.log(data);
// });

// connection.on("updateAantal", function (data) {
//     aantal_leerlingen.innerHTML = data + " deelnemer(s)";
//     console.log(data + " aanwezig");
// });

// connection.on("sessionInProgress", function () {
//     let leerkrachtStartSessie = document.getElementById("leerkrachtStartSessie");
//     leerkrachtStartSessie.style.display = "none";
//     let leerkrachtStellingenCards = document.getElementById("leerkrachtStellingenCards");
//     leerkrachtStellingenCards.style.display = "block";
//     let cards = document.querySelectorAll(".stellingCard").forEach(item => {
//         item.addEventListener("click", evt => {
//             let stellingId = item.firstElementChild;
//             stellingId = parseInt(stellingId.value);
//             console.log(stellingId);
//             connection.invoke("onLeerkrachtStellingCardClick",stellingId);
//             currentStellingId = stellingId;
//         })
//     })
// });

// connection.on("stellingSelected", function (data) {
//     let leerkrachtStellingenCards = document.getElementById("leerkrachtStellingenCards");
//     leerkrachtStellingenCards.style.display = "none";
//     let leerkrachtSelecteerStelling = document.getElementById("leerkrachtSelecteerStelling");
//     leerkrachtSelecteerStelling.style.display = "block";
//     let leerkrachtKiestStelling = document.getElementById("leerkrachtKiestStelling");
//     leerkrachtKiestStelling.disabled = false;

//     stelling = JSON.parse(data);
//     let stellingOnderwerp = document.getElementById("stellingOnderwerp");
//     stellingOnderwerp.innerHTML = stelling.StellingOnderwerp;
//     let stellingVraag = document.getElementById("stellingVraag");
//     stellingVraag.innerHTML = stelling.Vraag;

//     ul = document.getElementById("ul");
//     while (ul.firstChild) {
//         ul.removeChild(ul.lastChild);
//     }

//     console.log(stelling);
//     let terug = document.getElementById("terug");
//     terug.addEventListener("click", ev => {
//         leerkrachtSelecteerStelling.style.display = "none";
//         leerkrachtStellingenCards.style.display = "block";
//     })
// });

// connection.on("nieuwAntwoord", function (data) {
//     console.log("antwoord: "+data);
//     ul = document.getElementById("ul");
//     let span = document.createElement("span");
//     span.innerHTML = data;
//     span.classList.add("badge");
//     span.classList.add("badge-pill");
//     span.classList.add("badge-dark");
//     span.id = "span"+data;
//     let li = document.createElement("li");
//     li.appendChild(span);
//     ul.appendChild(li);
// });

// window.addEventListener("beforeunload", function (e) {
//     console.log("unload: "+parseInt(sessieId.value));
//     connection.invoke("RemoveFromGroup", parseInt(sessieId.value));
//     (e || window.event).returnValue = null;
//     return "Are you sure?";
// });

// window.onbeforeunload = function (ev) {
//     console.log("unload: "+parseInt(sessieId.value));
//     connection.invoke("RemoveFromGroup", parseInt(sessieId.value));
//     (e || window.event).returnValue = null;
//     return "Are you sure?";
// };