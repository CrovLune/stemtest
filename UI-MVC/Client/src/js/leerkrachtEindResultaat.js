google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
    var data = google.visualization.arrayToDataTable([
        ['Partij', 'Percentage', { role: 'style' } ],
        ['NVA', 32, 'color: gray'],
        ['VB', 10, 'color: #76A7FA'],
        ['Groen', 22, 'opacity: 0.2'],
        ['openVld', 16, 'stroke-color: #703593; stroke-width: 4; fill-color: #C5A5CF'],
        ['PVDA', 20, 'stroke-color: #871B47; stroke-opacity: 0.6; stroke-width: 8; fill-color: #BC5679; fill-opacity: 0.2']
    ]);

    var options = {
        title: ''
    };
    
    var chart = new google.visualization.ColumnChart(document.getElementById('grafiekEindResultaat'));
    chart.draw(data, options);
}