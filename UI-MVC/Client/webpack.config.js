const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
//fix
module.exports = {
    entry: {
        site: './src/js/site.js',
        bootstrap_js: './src/js/bootstrap_js.js',
        validation: './src/js/validation.js',
        index: './src/js/index.js',
        leerlingStellingScherm: './src/js/leerlingStellingScherm.js',
        leerlingStellingIndividueelResultaat: './src/js/leerlingStellingIndividueelResultaat',
        leerkrachtResultaatscherm: './src/js/leerkrachtResultaatscherm.js',
        leerlingEindResultaat: './src/js/leerlingEindResultaat.js',
        leerkrachtEindResultaat: './src/js/leerkrachtEindResultaat.js',
        dashboard: './src/js/dashboard.js',
        signalR: './src/js/signalr/dist/browser/signalr.js',
        signalRmin: './src/js/signalr/dist/browser/signalr.min.js',
        tryout: './src/js/tryout.js',
        startspel: './src/js/startspel.js',
        overview: './src/js/overview.js',
        debatspelLeerkrachtHub: './src/js/debatspelLeerkrachtHub.js',
        debatspelLeerlingHub: './src/js/debatspelLeerlingHub.js',
        resultatenDebatspel: './src/js/resultatenDebatspel.js',
        resultatenPartijspel: './src/js/resultatenPartijspel.js',
        resultatenscherm: './src/js/resultatenscherm.js',
        partijKeuzeHub: './src/js/PartijKeuzeHub',
        tableSearch: './src/js/tableSearch.js',
        cleanUp: './src/js/cleanup.js',
        checkboxes: './src/js/checkboxes.js'
    },
    output: {
        filename: '[name].entry.js',
        path: path.resolve(__dirname, '..', 'wwwroot', 'dist')
    },
    devtool: 'source-map',
    mode: 'development',
    module: {
        rules: [{
                test: /\.s?css$/,
                use: [{
                        loader: MiniCssExtractPlugin.loader
                    },
                    "css-loader",
                    "sass-loader"
                ]
            },
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: "file-loader"
            },
            {
                test: /\.(woff|woff2)$/,
                loader: "url-loader?prefix=font/&limit=5000"
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url-loader?limit=10000&mimetype=application/octet-stream"
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url-loader?limit=10000&mimetype=image/svg+xml"
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "[name].css"
        })
    ]
};