﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using ST.BL.Domain;
using ST.BL.Interfaces;

namespace UI_MVC.Pages
{
    [AllowAnonymous]
    public class Index : PageModel
    {
        private readonly ILogger _logger;
        private readonly ISessieManager _sessieManager;
        [BindProperty] public string UniqueCode { get; set; }
        [TempData] public string StatusMessage { get; set; }

        public Index(
            ILogger<Index> logger,
            ISessieManager sessieManager
        )
        {
            _logger = logger;
            _sessieManager = sessieManager;
        }

        public void OnGet()
        {
            
        }

        public IActionResult OnPostEnterSession(string cookie)
        {
            _logger.LogWarning($"Unique Code: {UniqueCode}");
            var code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(UniqueCode));
            var result = _sessieManager
                .GetSessies()
                .Where(s => s.Status == Status.Open)
                .ToList()
                .Find(s => s.UniqueCode.Equals(code));

            if (result != null)
            {
                return RedirectToPage("/StartSessie", new {area = "Sessies", sessieId = result.Id, cookie});
            }

            StatusMessage = "Session with this code does not exist!";
            return Page();
        }
    }
}