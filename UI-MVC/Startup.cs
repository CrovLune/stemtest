using System;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
// using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NETCore.MailKit.Extensions;
using NETCore.MailKit.Infrastructure.Internal;
using ReturnTrue.AspNetCore.Identity.Anonymous;
using ST.BL;
using ST.BL.Interfaces;
using ST.DAL;
using ST.DAL.EF;
using ST.DAL.Interfaces;
using ST.BL.Domain.Identity;
using UI_MVC.Hubs;

// using UI_MVC.Services;

// ReSharper disable StringLiteralTypo


namespace UI_MVC
{
    public class Startup
    {
        //TODO email moet uniek zijn
        //TODO stellingen op basisch van onderwerp in bulk kunnen aanduiden (alle stellingen van onderwerp Brexit bvb)
        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<TreeProjectDbContext>(config =>
            {
                // config.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
                config.UseMySql(Configuration.GetConnectionString("MySQL"));
            });

            services.AddControllers().AddNewtonsoftJson(options =>
            {

                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            });
            //* Set Default Auth policy to require users to be Authenticated.
            services.AddControllersWithViews(config =>
                {
                    var policy = new AuthorizationPolicyBuilder()
                        .RequireAuthenticatedUser()
                        .Build();
                    config.Filters.Add(new AuthorizeFilter(policy));
                })
                .AddRazorRuntimeCompilation()
                .SetCompatibilityVersion(CompatibilityVersion.Latest);
            services.AddRazorPages();

            services.AddScoped<IGebruikerKlassenRepository, GebruikerKlassenRepository>();
            services.AddScoped<IGebruikerKlassenManager, GebruikerKlassenManager>();
            services.AddScoped<IGebruikerManager, GebruikerManager>();
            services.AddScoped<IGebruikerRepository, GebruikerRepository>();
            services.AddScoped<IKlasRepository, KlasRepository>();
            services.AddScoped<IKlasManager, KlasManager>();
            services.AddScoped<IPartijRepository, PartijRepository>();
            services.AddScoped<IPartijManager, PartijManager>();
            services.AddScoped<IPartijStellingenRepository, PartijStellingenRepository>();
            services.AddScoped<IPartijStellingenManager, PartijStellingenManager>();
            services.AddScoped<ISchoolRepository, SchoolRepository>();
            services.AddScoped<ISchoolManager, SchoolManager>();
            services.AddScoped<ISpelRepository, SpelRepository>();
            services.AddScoped<ISpelManager, SpelManager>();
            services.AddScoped<ISessieRepository, SessieRepository>();
            services.AddScoped<ISessieManager, SessieManager>();
            services.AddScoped<IGebruikerStellingenRepository, GebruikerStellingenRepository>();
            services.AddScoped<IGebruikerStellingenManager, GebruikerStellingenManager>();
            services.AddScoped<IStellingRepository, StellingRepository>();
            services.AddScoped<IStellingManager, StellingManager>();
            services.AddScoped<IKlasSpellenRepository, KlasSpellenRepository>();
            services.AddScoped<IKlasSpellenManager, KlasSpellenManager>();
            services.AddScoped<IAntwoordRepository, AntwoordRepository>();
            services.AddScoped<IAntwoordenManager, AntwoordManager>();
            services.AddScoped<ISpelStellingenManager, SpelStellingenManager>();
            services.AddScoped<ISpelStellingenRepository, SpelStellingenRepository>();
            services.AddScoped<IAnoniemeGebruikerRepository, AnoniemeGebruikerRepository>();
            services.AddScoped<IAnoniemeGebruikerManager, AnoniemeGebruikerManager>();
            


            //* ---------------------------------------------------------------------------------------
            //* ----------------------------------> .NET Identity <------------------------------------
            //* ---------------------------------------------------------------------------------------

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    // options.Cookie.ExpireTim = TimeSpan.FromMinutes(30);
                    options.Cookie.Name = "AnonymousCookie";
                });
            
            services.AddIdentity<ApplicationUser, ApplicationRole>(config =>
                {
                    config.SignIn.RequireConfirmedAccount = true;
                    config.SignIn.RequireConfirmedEmail = true;

                    //? Password Settings.
                    config.Password.RequireDigit = true;
                    config.Password.RequireLowercase = true;
                    config.Password.RequireNonAlphanumeric = true;
                    config.Password.RequireUppercase = true;
                    config.Password.RequiredLength = 6;
                    config.Password.RequiredUniqueChars = 1;

                    //? Lockout Settings.
                    config.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                    config.Lockout.MaxFailedAccessAttempts = 5;
                    config.Lockout.AllowedForNewUsers = true;

                    //? User Settings.
                    config.User.AllowedUserNameCharacters =
                        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                    config.User.RequireUniqueEmail = true;
                })
                .AddDefaultTokenProviders()
                .AddEntityFrameworkStores<TreeProjectDbContext>();

            services.ConfigureApplicationCookie(config =>
            {
                //? Cookie Settings.
                config.Cookie.HttpOnly = true;
                config.ExpireTimeSpan = TimeSpan.FromMinutes(20);

                config.Cookie.Name = "Identity.Cookie";
                config.LoginPath = "/Identity/Account/Login";
                config.LogoutPath = "/Identity/Account/Logout";
                config.AccessDeniedPath = "/Identity/Account/Fail";
                config.SlidingExpiration = true;
            });

            // services.AddMailKit(config => config.UseMailKit(Configuration.GetSection("Email").Get<MailKitOptions>()));
            services.AddAuthorization(options =>
            {
                options.AddPolicy("RequireElevatedRights",
                    policy => policy.RequireRole("SuperAdmin", "Admin"));
            });
            
            //* ---------------------------------------------------------------------------------------
            //* -------------------------------------> SignalR <---------------------------------------
            //* ---------------------------------------------------------------------------------------
            services.AddSignalR();
            //* ---------------------------------------------------------------------------------------
            //* ---------------------------------------------------------------------------------------
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            var cookiePolicyOptions = new CookiePolicyOptions
            {
                MinimumSameSitePolicy = SameSiteMode.Strict
            };
            app.UseCookiePolicy(cookiePolicyOptions);

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAnonymousId();
            app.UseAuthentication();
            app.UseAuthorization();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();

                endpoints.MapHub<CleanUp>("/cleanup");
                endpoints.MapHub<MyHub>("/myhub");
                endpoints.MapHub<ResultatenHub>("/resultatenhub");

                endpoints.MapHub<SpelHub>("/spelHub");
                endpoints.MapHub<PartijKeuzeHub>("/spelHub");


                endpoints.MapDefaultControllerRoute();
                // endpoints.MapAreaControllerRoute(
                //     "areas",
                //     "areas",
                //     "{area:exists}/{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
        }
    }
}