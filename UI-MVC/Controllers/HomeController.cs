﻿using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using UI_MVC.Models;

namespace UI_MVC.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        
        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Contact(ContactViewModel contactVM)
        {
            if (ModelState.IsValid)
            {
                // Send an email or save the message in a table...
                // Redirect to a page that says "Thanks for contacting us!"...

                return RedirectToPage("/Index");
            }
            return View();
        }
    }
}