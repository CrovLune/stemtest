﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NuGet.Frameworks;
using Org.BouncyCastle.Utilities.Encoders;
using ST.BL;
using ST.BL.Domain.Antwoorden;
using ST.BL.Domain.Sessies;
using ST.BL.Domain.Spellen;
using ST.BL.Domain.Stellingen;
using ST.BL.Domain.Users;
using ST.DAL;
using ST.DAL.EF;

namespace UI_MVC.Controllers.Api
{
    [Route("api/[controller]")]
    [AllowAnonymous]
    [ApiController]
    public class SessiesController : ControllerBase
    {
        private TreeProjectDbContext context;
        private SessieManager manager;
        private AnoniemeGebruikerManager anonManager;
        private AntwoordManager antwoordManager;
        private PartijManager partijManager;

        public SessiesController()
        {
            context = new TreeProjectDbContext();
            manager = new SessieManager(new SessieRepository(context));
            anonManager = new AnoniemeGebruikerManager(new AnoniemeGebruikerRepository(context));
            antwoordManager = new AntwoordManager(new AntwoordRepository(context));
            partijManager = new PartijManager(new PartijRepository(context));
        }

        private async Task CreateCookie()
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, "AnonCooKie"),
                new Claim(ClaimTypes.Role, "AnonymousUser")
            };
            var claimsIdentity = new ClaimsIdentity(
                claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var authProperties = new AuthenticationProperties
            {
                AllowRefresh = true,
                ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(6000),
                IsPersistent = true
            };

            await HttpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity),
                authProperties);
        }


        [Route("connect/{code}")]
        [HttpGet]
        public async Task<SessionResponse> VerifySession(string code)
        {
            code = Encoding.UTF8.GetString(Base64.Encode(Encoding.UTF8.GetBytes(code)));
            System.Diagnostics.Debug.WriteLine(code);
            var sessie = manager.GetSessies().Where(x => x.UniqueCode == code).FirstOrDefault();
            if (sessie == null || sessie == default(Sessie) || sessie.Status == ST.BL.Domain.Status.Closed) return new SessionResponse();

            await CreateCookie();

            var getCookie = HttpContext.Request.Cookies["AnonymousCookie"];
            if (getCookie == null) getCookie = "";
            var session = new SessionResponse { SessionID = sessie.Id, SessionOptions = sessie.Spel.options, SessionType = sessie.Spel.GetType().ToString(), SessionCookie = getCookie, IsValidSession = true };


            

            return session;
        }

        [Route("result/partij")]
        [HttpGet]
        public SessionPartijResult GetPartijResult([FromQuery] string cookie)
        {
            var user = anonManager.GetAnoniemeGebruikers().Where(x => x.Cookie == cookie).FirstOrDefault();
            if (user == null || user == default(AnoniemeGebruiker) || user.Sessie == null) return null;

            var stellingModels = new List<StellingPartijResultModel>();
            var userAntwoorden = antwoordManager.GetUserAntwoorden().ToList();
            var partijAntwoorden = antwoordManager.GetPartijAntwoorden().ToList();

            var sessie = user.Sessie;

            foreach (var spelStelling in sessie.Spel.Stellingen)
            {
                var teGebruikenUserAntwoord = new UserAntwoordOpStelling();
                foreach (var antwoord in userAntwoorden)
                {
                    var a = (UserAntwoordOpStelling)antwoord;
                    if (a.Stelling.Id.Equals(spelStelling.Id))
                    {
                        teGebruikenUserAntwoord = a;
                    }
                }

                var teGebruikenPartijAntwoord = new PartijAntwoordOpStelling();
                foreach (var antwoord in partijAntwoorden)
                {
                    var a = (PartijAntwoordOpStelling)antwoord;
                    if (a.Partij.Id.Equals(user.Partij.Id) && a.Stelling.Id.Equals(spelStelling.Id))
                    {
                        teGebruikenPartijAntwoord = a;
                    }
                }

                if(teGebruikenUserAntwoord.Stelling != null && teGebruikenPartijAntwoord.Antwoord != null)
                {
                    var stellingModel = new StellingPartijResultModel
                    {
                        Stelling = spelStelling,
                        UserAntwoordOpStelling = teGebruikenUserAntwoord,
                        PartijAntwoordOpStelling = teGebruikenPartijAntwoord
                    };

                    stellingModels.Add(stellingModel);
                }

               

             

              

            }

            var score = 0;

            foreach (var stellingModelScore in stellingModels)
            {
                if ((stellingModelScore.PartijAntwoordOpStelling.Antwoord != null && stellingModelScore.UserAntwoordOpStelling != null) && stellingModelScore.PartijAntwoordOpStelling.Antwoord.Equals(stellingModelScore.UserAntwoordOpStelling.Antwoord))
                {
                    score++;
                }
            }

            var partijResult = new SessionPartijResult { Score = score, StellingModels = stellingModels, Sessie = sessie };

            return partijResult;
        }

        [Route("result/debat")]
        [HttpGet]
        public SessionDebatResult GetDebatResult([FromQuery] string cookie)
        {
            var user = anonManager.GetAnoniemeGebruikers().Where(x => x.Cookie == cookie).FirstOrDefault();
            if (user == null || user == default(AnoniemeGebruiker) || user.Sessie == null) return null;

            var sessie = user.Sessie;

            var stellingModels = new List<StellingDebatResultModel>();
            var userAntwoorden = antwoordManager.GetUserAntwoorden().ToList();
            var partijAntwoorden = antwoordManager.GetPartijAntwoorden().ToList();
            var partijen = partijManager.GetPartijen().ToList();
            var partijScoreModels = new List<PartijScoreModel>();

            foreach (var partij in partijen)
            {
                var modelVoorPartij = new PartijScoreModel()
                {
                    Partij = partij,
                    Score = 0
                };
                partijScoreModels.Add(modelVoorPartij);
            }
            foreach (var spelStelling in sessie.Spel.Stellingen) //voor elke stellingen van een spel
            {
                var teGebruikenUserAntwoord = new UserAntwoordOpStelling(); // creeer een teGebruikenUserantw
                foreach (var antwoord in userAntwoorden) //voor elk antw in userAntw
                {
                    var a = (UserAntwoordOpStelling)antwoord; //cast antw naar userantw
                    if (a.Stelling.Id.Equals(spelStelling.Id)) //als het id van antw Stelling gelijk is aan id van spelstelling
                    {
                        teGebruikenUserAntwoord = a; //vul deze met het ingeladen userantw
                    }
                }

                var teGebruikenPartijAntwoord = new PartijAntwoordOpStelling();
                foreach (var antwoord in partijAntwoorden)
                {
                    var a = (PartijAntwoordOpStelling)antwoord;
                    if (a.Stelling.Id.Equals(spelStelling.Id))
                    {
                        foreach (var partij in partijScoreModels)
                        {
                            if (a.Partij.Id == partij.Partij.Id)
                            {
                                if (a.Antwoord == teGebruikenUserAntwoord.Antwoord)
                                {
                                    partij.Score++;
                                }
                            }
                        }
                    }
                }

                if(teGebruikenUserAntwoord.Stelling != null)
                {
                    var stellingModel = new StellingDebatResultModel()
                    {
                        Stelling = spelStelling,
                        UserAntwoordOpStelling = teGebruikenUserAntwoord,

                    };
                    stellingModels.Add(stellingModel);
                }
               
            }

            var debatResult = new SessionDebatResult { Sessie = sessie, PartijScoreModels = partijScoreModels, StellingModels = stellingModels };

            return debatResult;

        }

    }
}