using Microsoft.AspNetCore.Mvc;
using ST.BL.Interfaces;
using System.Linq;
using ST.BL.Domain.Stellingen;
using ST.BL;
using ST.DAL.EF;
using ST.DAL;
using Microsoft.AspNetCore.Authorization;
using ST.BL.Domain.Users;
using System.Collections.Generic;
using ST.BL.Domain.Antwoorden;
// ReSharper disable StringLiteralTypo

// ReSharper disable IdentifierTypo


namespace UI_MVC.Controllers.Api
{
    [ApiController]
    [AllowAnonymous]
    [Route("api/[controller]")]
    public class StellingenController : ControllerBase
    {
        private TreeProjectDbContext context;
        private AnoniemeGebruikerManager anonManager;
        private AntwoordManager antwoordManager;

        public StellingenController()
        {
            context = new TreeProjectDbContext();
            anonManager = new AnoniemeGebruikerManager(new AnoniemeGebruikerRepository(context));
            antwoordManager = new AntwoordManager(new AntwoordRepository(context));
        }

        [Route("get")]
        [HttpGet]
        public List<Stelling> GetForSession([FromQuery] string cookie)
        {
            var user = anonManager.GetAnoniemeGebruikers().Where(x => x.Cookie == cookie).FirstOrDefault();
            if (user == null || user == default(AnoniemeGebruiker) || user.Sessie == null) return null;

            var stellingen = user.Sessie.Spel.Stellingen.ToList();

            return stellingen;
        }

        [Route("put")]
        [HttpPut]
        public IActionResult PutStelling([FromQuery] string cookie, [FromQuery] int stellingId, [FromQuery] string antwoord, [FromQuery] string argument)
        {
            var user = anonManager.GetAnoniemeGebruikers().Where(x => x.Cookie == cookie).FirstOrDefault();
            if (user == null || user == default(AnoniemeGebruiker) || user.Sessie == null) return BadRequest(400);

            var stelling = user.Sessie.Spel.Stellingen.Where(x => x.Id == stellingId).FirstOrDefault();
            System.Diagnostics.Debug.WriteLine(stellingId);
            System.Diagnostics.Debug.WriteLine(stelling);
            var userAntwoord = new UserAntwoordOpStelling { AnoniemeGebruiker = user, Antwoord = antwoord, Argument = argument == null ? "" : argument, Stelling = stelling, StellingId = stelling.Id };

            antwoordManager.AddAntwoord(userAntwoord);

            return Ok(200);

        }
    }
}