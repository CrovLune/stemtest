﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileSystemGlobbing.Internal.PatternContexts;
using ST.BL;
using ST.BL.Domain.Partijen;
using ST.BL.Domain.Users;
using ST.DAL;
using ST.DAL.EF;

namespace UI_MVC.Controllers.Api
{
    [Route("api/[controller]")]
    [AllowAnonymous]
    [ApiController]
    public class PartijenController : ControllerBase
    {
        private TreeProjectDbContext context;
        private PartijManager manager;
        private AnoniemeGebruikerManager anonManager;

        public PartijenController()
        {
            context = new TreeProjectDbContext();
            manager = new PartijManager(new PartijRepository(context));
            anonManager = new AnoniemeGebruikerManager(new AnoniemeGebruikerRepository(context));
        }

        [Route("get")]
        [HttpGet]
        public List<Partij> GetPartijen([FromQuery] string cookie)
        {
            var user = anonManager.GetAnoniemeGebruikers().Where(x => x.Cookie == cookie).FirstOrDefault();
            if (user == null || user == default(AnoniemeGebruiker) || user.Sessie == null) return null;

            var partijen = manager.GetPartijen().ToList();

            return partijen;
        }

        [Route("put")]
        [HttpPut]
        public IActionResult PutPartij([FromQuery] string cookie, [FromQuery] int partijId)
        {
            var user = anonManager.GetAnoniemeGebruikers().Where(x => x.Cookie == cookie).FirstOrDefault();
            if (user == null || user == default(AnoniemeGebruiker) || user.Sessie == null) return BadRequest(400);

            user.Partij = manager.GetPartijById(partijId);

            anonManager.ChangeAnoniemeGebruiker(user);

            return Ok(200);
        }
    }
}