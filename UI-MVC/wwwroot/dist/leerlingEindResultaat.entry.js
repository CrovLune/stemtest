/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/leerlingEindResultaat.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/leerlingEindResultaat.js":
/*!*****************************************!*\
  !*** ./src/js/leerlingEindResultaat.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {



window.addEventListener("load", function () {
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    
    let table = document.getElementById("table");
    let partij1 = [table.rows[0].cells[0].innerText,parseInt(table.rows[0].cells[1].innerText.replace("%",""))];
    let partij2 = [table.rows[1].cells[0].innerText,parseInt(table.rows[1].cells[1].innerText.replace("%",""))];
    let partij3 = [table.rows[2].cells[0].innerText,parseInt(table.rows[2].cells[1].innerText.replace("%",""))];
    let partij4 = [table.rows[3].cells[0].innerText,parseInt(table.rows[3].cells[1].innerText.replace("%",""))];
    let partij5 = [table.rows[4].cells[0].innerText,parseInt(table.rows[4].cells[1].innerText.replace("%",""))];
    let partij6 = [table.rows[5].cells[0].innerText,parseInt(table.rows[5].cells[1].innerText.replace("%",""))];
    console.log(partij1,partij2,partij3,partij4,partij5,partij6);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Partij', 'percentage'],
            [partij1[0], partij1[1]],
            [partij2[0], partij2[1]],
            [partij3[0], partij3[1]],
            [partij4[0], partij4[1]],
            [partij5[0], partij5[1]],
            [partij6[0], partij6[1]],
        ]);

        var options = {
            title: 'Stemmen'
        };

        var chart = new google.visualization.BarChart(document.getElementById('chartPartijen'));
        chart.draw(data, options);
    }
});

/***/ })

/******/ });
//# sourceMappingURL=leerlingEindResultaat.entry.js.map