﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain.Antwoorden;
using ST.BL.Domain.Sessies;
using ST.BL.Domain.Stellingen;
using ST.BL.Domain.Users;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Resultaten.Pages
{
    [AllowAnonymous]
    public class ResultatenschermPartijspel : PageModel
    {
        private readonly IAnoniemeGebruikerManager _anoniemeGebruikerManager;
        private readonly ISessieManager _sessieManager;
        private readonly IAntwoordenManager _antwoordenManager;
        private readonly IStellingManager _stellingManager;
        
        public AnoniemeGebruiker AnoniemeGebruiker { get; set; }
        public List<StellingModel> StellingModels { get; set; }
        
        public Sessie Sessie { get; set; }
        [BindProperty(SupportsGet = true)] public int IngegevenAntwoordId { get; set; }
        [BindProperty(SupportsGet = true)] public int GebruikersId { get; set; }
        [BindProperty] public int SessieId { get; set; }
        [BindProperty] public string CookieVal { get; set; }
        public int Score { get; set; }

        public ResultatenschermPartijspel(IAnoniemeGebruikerManager anoniemeGebruikerManager, ISessieManager sessieManager, IAntwoordenManager antwoordenManager, IStellingManager stellingManager)
        {
            _anoniemeGebruikerManager = anoniemeGebruikerManager;
            _sessieManager = sessieManager;
            _antwoordenManager = antwoordenManager;
            _stellingManager = stellingManager;
        }

        public class StellingModel
        {
            public Stelling Stelling { get; set; }
            public UserAntwoordOpStelling UserAntwoordOpStelling { get; set; }
            public PartijAntwoordOpStelling PartijAntwoordOpStelling { get; set; }
        }

        public IActionResult OnGet(int sessieId, string cookieVal)
        {
            Sessie = _sessieManager.GetSessieById(sessieId);
            CookieVal = cookieVal;

            //get anon met signalR
            AnoniemeGebruiker = _anoniemeGebruikerManager.GetAnoniemeGebruikers().First(a => a.Cookie.Equals(CookieVal));
            
            StellingModels = new List<StellingModel>();
            var userAntwoorden = _antwoordenManager.GetUserAntwoorden().ToList();
            var partijAntwoorden = _antwoordenManager.GetPartijAntwoorden().ToList();
            
            foreach (var spelStelling in Sessie.Spel.SpelStellingen)
            {
                var teGebruikenUserAntwoord = new UserAntwoordOpStelling();
                foreach (var  antwoord in userAntwoorden)
                {
                    var a = (UserAntwoordOpStelling) antwoord;
                    if (a.Stelling.Id.Equals(spelStelling.Stelling.Id))
                    {
                        teGebruikenUserAntwoord = a;
                    }
                }
                
                var teGebruikenPartijAntwoord = new PartijAntwoordOpStelling();
                foreach (var  antwoord in partijAntwoorden)
                {
                    var a = (PartijAntwoordOpStelling) antwoord;
                    if (a.Partij.Id.Equals(AnoniemeGebruiker.Partij.Id) && a.Stelling.Id.Equals(spelStelling.Stelling.Id))
                    {
                        teGebruikenPartijAntwoord = a;
                    }
                }
                
                var stellingModel = new StellingModel()
                {
                    Stelling = spelStelling.Stelling,
                    UserAntwoordOpStelling = teGebruikenUserAntwoord,
                    PartijAntwoordOpStelling = teGebruikenPartijAntwoord
                };
                StellingModels.Add(stellingModel);
            }
            
            Score = BerekenScore(StellingModels);

            return Page();
        }

        public int BerekenScore(List<StellingModel> stellingModels)
        {
            var score = 0;
            foreach (var stellingModel in stellingModels)
            {
                if (stellingModel.PartijAntwoordOpStelling.Antwoord.Equals(stellingModel.UserAntwoordOpStelling.Antwoord))
                {
                    score++;
                }
            }
            return score;
        }

        public void SeedData(AnoniemeGebruiker gebruiker)
        {
            var ua1 = new UserAntwoordOpStelling() {AnoniemeGebruiker = gebruiker, Antwoord = "Eens", Stelling = _stellingManager.GetStellingById(5)};
            var ua2 = new UserAntwoordOpStelling() {AnoniemeGebruiker = gebruiker, Antwoord = "Oneens", Stelling = _stellingManager.GetStellingById(6)};
            var ua3 = new UserAntwoordOpStelling() {AnoniemeGebruiker = gebruiker, Antwoord = "Eens", Stelling = _stellingManager.GetStellingById(7)};
            var ua4 = new UserAntwoordOpStelling() {AnoniemeGebruiker = gebruiker, Antwoord = "Eens", Stelling = _stellingManager.GetStellingById(8)};
            var ua5 = new UserAntwoordOpStelling() {AnoniemeGebruiker = gebruiker, Antwoord = "Eens", Stelling = _stellingManager.GetStellingById(9)};
            var ua6 = new UserAntwoordOpStelling() {AnoniemeGebruiker = gebruiker, Antwoord = "Oneens", Stelling = _stellingManager.GetStellingById(10)};
            var ua7 = new UserAntwoordOpStelling() {AnoniemeGebruiker = gebruiker, Antwoord = "Eens", Stelling = _stellingManager.GetStellingById(11)};
            var ua8 = new UserAntwoordOpStelling() {AnoniemeGebruiker = gebruiker, Antwoord = "Oneens", Stelling = _stellingManager.GetStellingById(12)};
            var ua9 = new UserAntwoordOpStelling() {AnoniemeGebruiker = gebruiker, Antwoord = "Eens", Stelling = _stellingManager.GetStellingById(13)};
            _antwoordenManager.AddAntwoord(ua1);
            _antwoordenManager.AddAntwoord(ua2);
            _antwoordenManager.AddAntwoord(ua3);
            _antwoordenManager.AddAntwoord(ua4);
            _antwoordenManager.AddAntwoord(ua5);
            _antwoordenManager.AddAntwoord(ua6);
            _antwoordenManager.AddAntwoord(ua7);
            _antwoordenManager.AddAntwoord(ua8);
            _antwoordenManager.AddAntwoord(ua9);
        }
    }
}