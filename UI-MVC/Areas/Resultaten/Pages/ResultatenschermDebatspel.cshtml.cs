﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain.Antwoorden;
using ST.BL.Domain.Partijen;
using ST.BL.Domain.Sessies;
using ST.BL.Domain.Stellingen;
using ST.BL.Domain.Users;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Resultaten.Pages
{
    [AllowAnonymous]
    public class ResultatenschermDebatspel : PageModel
    {
        private readonly IAnoniemeGebruikerManager _anoniemeGebruikerManager;
        private readonly ISessieManager _sessieManager;
        private readonly IAntwoordenManager _antwoordenManager;
        private readonly IStellingManager _stellingManager;
        private readonly IPartijManager _partijManager;

        public List<UserAntwoordOpStelling> UserAntwoorden { get; set; }
        public AnoniemeGebruiker AnoniemeGebruiker { get; set; }
        public Sessie Sessie { get; set; }
        public List<StellingModel> StellingModels { get; set; }
        [BindProperty(SupportsGet = true)] public int IngegevenAntwoordId { get; set; }
        [BindProperty(SupportsGet = true)] public int GebruikersId { get; set; }
        public List<PartijScoreModel> PartijScoreModels { get; set; }
        [BindProperty] public int SessieId { get; set; }
        [BindProperty] public string CookieVal { get; set; }

        public ResultatenschermDebatspel(IAnoniemeGebruikerManager anoniemeGebruikerManager,
            ISessieManager sessieManager, IAntwoordenManager antwoordenManager, IStellingManager stellingManager,
            IPartijManager partijManager)
        {
            _anoniemeGebruikerManager = anoniemeGebruikerManager;
            _sessieManager = sessieManager;
            _antwoordenManager = antwoordenManager;
            _stellingManager = stellingManager;
            _partijManager = partijManager;
        }
        
        public class StellingModel
        {
            public Stelling Stelling { get; set; }
            public UserAntwoordOpStelling UserAntwoordOpStelling { get; set; }
            public List<int> ScoreOpPartij { get; set; }
        }

        public class PartijScoreModel
        {
            public Partij Partij { get; set; }
            public int Score { get; set; }
        }

        // public void SeedData(AnoniemeGebruiker gebruiker)
        // {
        //     var ua1 = new UserAntwoordOpStelling()
        //         {AnoniemeGebruiker = gebruiker, Antwoord = "Eens", Stelling = _stellingManager.GetStellingById(5)};
        //     var ua2 = new UserAntwoordOpStelling()
        //         {AnoniemeGebruiker = gebruiker, Antwoord = "Oneens", Stelling = _stellingManager.GetStellingById(6)};
        //     var ua3 = new UserAntwoordOpStelling()
        //         {AnoniemeGebruiker = gebruiker, Antwoord = "Eens", Stelling = _stellingManager.GetStellingById(7)};
        //     var ua4 = new UserAntwoordOpStelling()
        //         {AnoniemeGebruiker = gebruiker, Antwoord = "Eens", Stelling = _stellingManager.GetStellingById(8)};
        //     var ua5 = new UserAntwoordOpStelling()
        //         {AnoniemeGebruiker = gebruiker, Antwoord = "Eens", Stelling = _stellingManager.GetStellingById(9)};
        //     var ua6 = new UserAntwoordOpStelling()
        //         {AnoniemeGebruiker = gebruiker, Antwoord = "Oneens", Stelling = _stellingManager.GetStellingById(10)};
        //     var ua7 = new UserAntwoordOpStelling()
        //         {AnoniemeGebruiker = gebruiker, Antwoord = "Eens", Stelling = _stellingManager.GetStellingById(11)};
        //     var ua8 = new UserAntwoordOpStelling()
        //         {AnoniemeGebruiker = gebruiker, Antwoord = "Oneens", Stelling = _stellingManager.GetStellingById(12)};
        //     var ua9 = new UserAntwoordOpStelling()
        //         {AnoniemeGebruiker = gebruiker, Antwoord = "Eens", Stelling = _stellingManager.GetStellingById(13)};
        //     _antwoordenManager.AddAntwoord(ua1);
        //     _antwoordenManager.AddAntwoord(ua2);
        //     _antwoordenManager.AddAntwoord(ua3);
        //     _antwoordenManager.AddAntwoord(ua4);
        //     _antwoordenManager.AddAntwoord(ua5);
        //     _antwoordenManager.AddAntwoord(ua6);
        //     _antwoordenManager.AddAntwoord(ua7);
        //     _antwoordenManager.AddAntwoord(ua8);
        //     _antwoordenManager.AddAntwoord(ua9);
        // }

        public IActionResult OnGet(int sessieId)
        {
            Sessie = _sessieManager.GetSessieById(sessieId);
            var getCookie = HttpContext.Request.Cookies["AnonymousCookie"];
            CookieVal = getCookie;

            //get anon met signalR
            AnoniemeGebruiker =
                _anoniemeGebruikerManager.GetAnoniemeGebruikers().Single(x => x.Cookie.Equals(getCookie));

            //SeedData(AnoniemeGebruiker);
            StellingModels = new List<StellingModel>();
            var stellingen = Sessie.Spel.SpelStellingen.ToList();
            UserAntwoorden = new List<UserAntwoordOpStelling>();
            var count = 0;
            var total = 0;
            foreach (var stelling in stellingen)
            {
                var x = stelling.Stelling.UserAntwoordOpStelling.Single(a => a.AnoniemeGebruiker == AnoniemeGebruiker);
                var y = stelling.Stelling.PartijAntwoordOpStelling.ToList();
                foreach (var partijAntwoordOpStelling in y)
                {
                    if (partijAntwoordOpStelling.Antwoord.Equals(x.Antwoord))
                    {
                        count++;
                    }
                }

                total = y.Count;
                UserAntwoorden.Add(x);
            }

            var res = count / total;


            // var userAntwoorden = _antwoordenManager.GetUserAntwoorden().ToList();
            // var partijAntwoorden = _antwoordenManager.GetPartijAntwoorden().ToList();
            // var partijen = _partijManager.GetPartijen().ToList();
            // PartijScoreModels = new List<PartijScoreModel>();
            //
            // foreach (var partij in partijen)
            // {
            //     var modelVoorPartij = new PartijScoreModel()
            //     {
            //         Partij = partij,
            //         Score = 0
            //     };
            //     PartijScoreModels.Add(modelVoorPartij);
            // }
            //
            // foreach (var spelStelling in Sessie.Spel.SpelStellingen) //voor elke stellingen van een spel
            // {
            //     var teGebruikenUserAntwoord = new UserAntwoordOpStelling(); // creeer een teGebruikenUserantw
            //     foreach (var antwoord in userAntwoorden) //voor elk antw in userAntw
            //     {
            //         var a = (UserAntwoordOpStelling) antwoord; //cast antw naar userantw
            //         if (a.Stelling.Id.Equals(spelStelling.Stelling.Id)
            //         ) //als het id van antw Stelling gelijk is aan id van spelstelling
            //         {
            //             teGebruikenUserAntwoord = a; //vul deze met het ingeladen userantw
            //         }
            //     }
            //
            //     var teGebruikenPartijAntwoord = new PartijAntwoordOpStelling();
            //     foreach (var antwoord in partijAntwoorden)
            //     {
            //         var a = (PartijAntwoordOpStelling) antwoord;
            //         if (a.Stelling.Id.Equals(spelStelling.Stelling.Id))
            //         {
            //             foreach (var partij in PartijScoreModels)
            //             {
            //                 if (a.Partij.Id == partij.Partij.Id)
            //                 {
            //                     if (a.Antwoord == teGebruikenUserAntwoord.Antwoord)
            //                     {
            //                         partij.Score++;
            //                     }
            //                 }
            //             }
            //         }
            //     }
            //
            //     var stellingModel = new StellingModel()
            //     {
            //         Stelling = spelStelling.Stelling,
            //         UserAntwoordOpStelling = teGebruikenUserAntwoord,
            //         //PartijAntwoordOpStelling = teGebruikenPartijAntwoord
            //     };
            //     StellingModels.Add(stellingModel);
            // }


            return Page();
        }
    }
}