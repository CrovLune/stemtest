﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using ST.BL.Domain;
using ST.BL.Domain.Antwoorden;
using ST.BL.Domain.Identity;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Domain.Partijen;
using ST.BL.Domain.Spellen;
using ST.BL.Domain.Stellingen;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Stellingen.Pages
{
    public class AddStelling : PageModel
    {
        private readonly IStellingManager _stellingManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IGebruikerManager _gebruikerManager;
        private readonly ISpelStellingenManager _spelStellingenManager;
        private readonly ISpelManager _spelManager;
        private readonly IGebruikerStellingenManager _gebruikerStellingenManager;

        public AddStelling(
            IStellingManager stellingManager,
            UserManager<ApplicationUser> userManager,
            IGebruikerManager gebruikerManager,
            ISpelStellingenManager spelStellingenManager,
            ISpelManager spelManager,
            IGebruikerStellingenManager gebruikerStellingenManager
        )
        {
            _stellingManager = stellingManager;
            _userManager = userManager;
            _gebruikerManager = gebruikerManager;
            _spelStellingenManager = spelStellingenManager;
            _spelManager = spelManager;
            _gebruikerStellingenManager = gebruikerStellingenManager;
        }

        public string ReturnUrl { get; set; }
        public int SpelId { get; set; }
        public List<SpelStellingen> SpelStellingen { get; set; }
        public int KlasId { get; set; }

        [BindProperty] public AddStellingModel Input { get; set; }
        public List<GebruikerStellingen> GebruikerStellingen { get; set; }

        public class AddStellingModel
        {
            [Display(Name = "Onderwerp")] public string StellingOnderwerp { get; set; }
            public string Vraag { get; set; }
            public string Uitleg { get; set; }
            public Status Status { get; set; }
        }


        public async Task OnGet(int klasId, int spelId = 0, string returnUrl = null)
        {
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }

            ReturnUrl = returnUrl;
            KlasId = klasId;

            var user = await _userManager.GetUserAsync(User);
            var gebruiker = _gebruikerManager.GetGebruikerByIdentity(user);

            if (spelId == 0)
            {
                var tempSpel = new Spel {Name = $"tempspel_{gebruiker}"};
                var spelResult = _spelManager.AddSpel(tempSpel);
                SpelId = spelResult.SpelId;
            }
            else
            {
                SpelId = spelId;
            }

            SpelStellingen = _spelStellingenManager
                .GetSpelStellingen()
                .Where(x => x.SpelId == SpelId)
                .ToList();

            GebruikerStellingen = _gebruikerStellingenManager
                .GetGebruikerStellingen()
                .Where(x => x.GebruikerId == gebruiker.Id)
                .ToList();
        }


        public async Task<IActionResult> OnPostAddStellingAsync(int spelId, int klasId)
        {
            var user = await _userManager.GetUserAsync(User);
            var gebruiker = _gebruikerManager.GetGebruikerByIdentity(user);
            
            var stelling = new Stelling
            {
                StellingOnderwerp = Input.StellingOnderwerp,
                Vraag = Input.Vraag,
                Uitleg = Input.Uitleg,
                Status = Input.Status
            };
            var spelStelling = new SpelStellingen {SpelId = spelId, Stelling = stelling};
            var spelstellingResult = _spelStellingenManager.AddSpelStelling(spelStelling);

            var gebstel = new GebruikerStellingen{GebruikerId = gebruiker.Id, StellingId = spelstellingResult.StellingId};
            _gebruikerStellingenManager.AddGebruikerStelling(gebstel);
          

            return RedirectToPage("/AddStelling", new {area = "Stellingen", spelId, klasId});
        }
    }
}