﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using ST.BL.Domain.Antwoorden;
using ST.BL.Domain.Identity;
using ST.BL.Domain.Partijen;
using ST.BL.Domain.Stellingen;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Stellingen.Pages
{
    public class EditStelling : PageModel
    {
        private readonly IStellingManager _stellingManager;
        private readonly IPartijManager _partijManager;


        public EditStelling(
            IStellingManager stellingManager,
            IPartijManager partijManager
        )
        {
            _stellingManager = stellingManager;
            _partijManager = partijManager;
        }

        public string ReturnUrl { get; set; }
        [BindProperty] public MoeilijkWoordModel MWModel { get; set; }
        [BindProperty] public PartijModel PModel { get; set; }
        [BindProperty] public int SelectedPartij { get; set; }
        [BindProperty] public List<int> areCheckedMW { get; set; }
        [BindProperty] public List<int> areCheckedPA { get; set; }

        public int SpelId { get; set; }
        public int KlasId { get; set; }

        public class MoeilijkWoordModel
        {
            public MoeilijkWoordModel()
            {
                MoeilijkWoord ??= new MoeilijkWoord();
            }

            public MoeilijkWoord MoeilijkWoord { get; set; }
            public Stelling Stelling { get; set; }

            // public int SpelId { get; set; }
            // public int KlasId { get; set; }
        }

        public class PartijModel
        {
            public PartijModel()
            {
                PartijenSelectList = new List<SelectListItem>();
            }

            public List<SelectListItem> PartijenSelectList { get; set; }
            public PartijAntwoordOpStelling PartijAntwoordOpStelling { get; set; }
            public Stelling Stelling { get; set; }
            public List<int> areChecked { get; set; }
            public string Antwoord { get; set; }
        }

        public void OnGet(int klasId, int spelId, int stellingId, string returnUrl)
        {
            ReturnUrl = returnUrl;
            KlasId = klasId;
            SpelId = spelId;
            areCheckedMW = new List<int>();
            areCheckedPA = new List<int>();

            MWModel ??= new MoeilijkWoordModel {Stelling = _stellingManager.GetStellingById(stellingId)};
            PModel ??= new PartijModel {Stelling = _stellingManager.GetStellingById(stellingId)};

            var list = new List<SelectListItem>();
            foreach (var partij in _partijManager.GetPartijen())
            {
                var item = new SelectListItem(partij.Naam,
                    partij.Id.ToString());
                list.Add(item);
            }

            PModel.PartijenSelectList = list;
        }


        public IActionResult OnPostSaveMoeilijkWoord(int stellingId, int spelId, int klasId)
        {
            var stelling = _stellingManager.GetStellingById(stellingId);

            stelling.MoeilijkeWoorden.Add(MWModel.MoeilijkWoord);
            _stellingManager.ChangeStelling(stelling);

            return RedirectToPage(new {stellingId, spelId, klasId});
        }

        public IActionResult OnPostSelectMoeilijkWoord(int stellingId, int spelId, int klasId)
        {
            var stelling = _stellingManager.GetStellingById(stellingId);
            foreach (var mwId in areCheckedMW)
            {
                var mw = stelling.MoeilijkeWoorden.Single(x => x.Id == mwId);
                stelling.MoeilijkeWoorden.Remove(mw);
            }

            _stellingManager.ChangeStelling(stelling);

            return RedirectToPage(new {stellingId, spelId, klasId});
        }

        public IActionResult OnPostSavePartij(int stellingId, int spelId, int klasId)
        {
            var stelling = _stellingManager.GetStellingById(stellingId);
            PModel.PartijAntwoordOpStelling.Partij = new Partij();
            PModel.PartijAntwoordOpStelling.Partij = _partijManager.GetPartijById(SelectedPartij);
            if (PModel.Antwoord.Equals("eens"))
            {
                PModel.PartijAntwoordOpStelling.Antwoord = "eens";
            }

            if (PModel.Antwoord.Equals("oneens"))
            {
                PModel.PartijAntwoordOpStelling.Antwoord = "oneens";
            }

            stelling.PartijAntwoordOpStelling.Add(PModel.PartijAntwoordOpStelling);
            _stellingManager.ChangeStelling(stelling);
            
            return RedirectToPage(new {stellingId, spelId, klasId});
        }

        public IActionResult OnPostSelectPartij(int stellingId, int spelId, int klasId)
        {
            var stelling = _stellingManager.GetStellingById(stellingId);
            foreach (var paId in areCheckedPA)
            {
                var pa = stelling.PartijAntwoordOpStelling.Single(x => x.Id == paId);
                stelling.PartijAntwoordOpStelling.Remove(pa);
            }

            _stellingManager.ChangeStelling(stelling);
            return RedirectToPage(new {stellingId, spelId, klasId});
        }
    }
}