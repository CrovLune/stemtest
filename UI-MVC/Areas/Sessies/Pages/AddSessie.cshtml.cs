﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using ST.BL.Domain;
using ST.BL.Domain.Identity;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Domain.Sessies;
using ST.BL.Domain.Spellen;
using ST.BL.Domain.Stellingen;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Sessies.Pages
{
    public class AddSessie : PageModel
    {
        private readonly IKlasManager _klasManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ISessieManager _sessieManager;
        private readonly ISpelManager _spelManager;
        private readonly ILogger<AddSessie> _logger;
        private readonly IGebruikerManager _gebruikerManager;
        private readonly IKlasSpellenManager _klasSpellenManager;

        public AddSessie(
            IKlasManager klasManager,
            UserManager<ApplicationUser> userManager,
            ISessieManager sessieManager,
            ISpelManager spelManager,
            ILogger<AddSessie> logger,
            IGebruikerManager gebruikerManager,
            IKlasSpellenManager klasSpellenManager
        )
        {
            _klasManager = klasManager;
            _userManager = userManager;
            _sessieManager = sessieManager;
            _spelManager = spelManager;
            _logger = logger;
            _gebruikerManager = gebruikerManager;
            _klasSpellenManager = klasSpellenManager;
        }

        public int KlasId { get; set; }
        [BindProperty] public Sessie Sessie { get; set; }
        [BindProperty] public int SelectedSoortSpel { get; set; }
        [BindProperty] public List<KlasSpellen> KSList { get; set; }
        [BindProperty] public int Selected { get; set; }
        public string ReturnUrl { get; set; }
        public int SpelId { get; set; }

        public async Task<IActionResult> OnGetAsync(int klasId, string returnUrl = null)
        {
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }

            ReturnUrl = returnUrl;

            KlasId = klasId;

            var user = await _userManager.GetUserAsync(User);
            var gebruiker = _gebruikerManager.GetGebruikerByIdentity(user);

            KSList = new List<KlasSpellen>();
            KSList = _klasSpellenManager
                .GetKlasSpellen()
                .Where(k => k.KlasId == klasId)
                .Where(k => k.Spel.Creator.Equals(gebruiker))
                .ToList();

            return Page();
        }

        // public IActionResult OnPostCreateSpel(int klasId, string returnUrl = null)
        // {
        //     return RedirectToPage("/AddSpel", new {area = "Spellen", klasId, returnUrl});
        // }

        private static string RandomString(int length)
        {
            var random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public IActionResult OnPostSelectedSpel(int klasId, int spelId, string returnUrl = null)
        {
            var klas = _klasManager.GetKlasById(klasId);

            var items = _klasSpellenManager.GetKlasSpellen().ToList();
            foreach (var item in items)
            {
                item.Selected = false;
            }

            _klasSpellenManager.ChangeKlasSpelRange(items);

            var ks = _klasSpellenManager.GetKlasSpellen()
                .Where(k => k.KlasId.Equals(klasId))
                .SingleOrDefault(k => k.SpelId == spelId);

            ks.Selected = true;
            _klasSpellenManager.ChangeKlasSpel(ks);

            return RedirectToPage();
        }

        public IActionResult OnPostDeleteSessie(int sessieId, string returnUrl = null)
        {
            var sessie = _sessieManager.GetSessieById(sessieId);
            _sessieManager.RemoveSessie(sessie);

            return LocalRedirect(returnUrl);
        }

        public async Task<IActionResult> OnPostStartSessie(int spelId,int klasId, string returnUrl = null)
        {
            var user = await _userManager.GetUserAsync(User);
            var gebruiker = _gebruikerManager.GetGebruikerByIdentity(user);

            
            var klas = _klasManager.GetKlasById(klasId);
            var date = $"{DateTime.Today.Day}/{DateTime.Today.Month}/{DateTime.Today.Year}";
            var spel = _spelManager.GetSpelById(spelId);
            
            var sessie = new Sessie
            {
                Name = $"[{date} - {spel.Name}] - {klas.Sessies.Count} - {klas.Naam}",
                Klas = klas,
                Creator = gebruiker,
                AantalDeelnemers = klas.AantalLeerlingen,
                Status = Status.Open,
                Spel = spel,
                UniqueCode = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(RandomString(7)))
            };
            var sessieResult = _sessieManager.AddSessie(sessie);

            return RedirectToPage("/StartSessie",new {sessieId = sessieResult.Id});
        }
    }
}