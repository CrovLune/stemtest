﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain.Antwoorden;
using ST.BL.Domain.Sessies;
using ST.BL.Domain.Stellingen;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Sessies.Pages
{
    public class SessieInfo : PageModel
    {
        [BindProperty] public int SessieId { get; set; }
        public int AantalDeelnemers { get; set; }
        public Sessie Sessie { get; set; }
        public List<UserAntwoordOpStelling> UserAntwoorden { get; set; }
        public List<StellingModel> StellingModels { get; set; }
        
        private readonly ISessieManager _sessieManager;
        private readonly IAntwoordenManager _antwoordenManager;
        private readonly IStellingManager _stellingManager;
        private readonly IAnoniemeGebruikerManager _anoniemeGebruikerManager;

        public SessieInfo(ISessieManager sessieManager, IAntwoordenManager antwoordenManager, IStellingManager stellingManager, IAnoniemeGebruikerManager anoniemeGebruikerManager)
        {
            _sessieManager = sessieManager;
            _antwoordenManager = antwoordenManager;
            _stellingManager = stellingManager;
            _anoniemeGebruikerManager = anoniemeGebruikerManager;
        }
        
        public class StellingModel
        {
            public Stelling Stelling { get; set; }
            public int AantalEens { get; set; }
            public int AantalOneens { get; set; }
        }
        
        public void OnGet(int sessieId)
        {
            SessieId = sessieId;
            Sessie = _sessieManager.GetSessieById(sessieId);
            UserAntwoorden = new List<UserAntwoordOpStelling>();
            StellingModels = new List<StellingModel>();
            
            //voor alle anons in de sessie alle antwoorden ophalen en in de UserAntwoorden steken
            var anons = _anoniemeGebruikerManager.GetAnoniemeGebruikers().Where(a => a.Sessie.Id.Equals(Sessie.Id)).ToList();
            AantalDeelnemers = anons.Count();
            
            var antwoorden = _antwoordenManager.GetUserAntwoorden();
            
            //voor elke antwoord checken of de anon in deze sessie voorkwam, zo ja toevoegen aan lijst
            foreach (var antwoord in antwoorden)
            {
                var a = (UserAntwoordOpStelling) antwoord;
                if (anons.Contains(a.AnoniemeGebruiker))
                {
                    UserAntwoorden.Add(a);
                }
            }

            foreach (var spelStelling in Sessie.Spel.SpelStellingen)
            {
                //van alle userantwoorden checken of ze eens of oneens hebben geantwoord
                var eens = 0;
                var oneens = 0;
                foreach (var antwoordOpStelling in UserAntwoorden.Where(u => u.StellingId.Equals(spelStelling.StellingId)))
                {
                    if (antwoordOpStelling.Antwoord.Equals("Eens"))
                    {
                        eens++;
                    } else if (antwoordOpStelling.Antwoord.Equals("Oneens"))
                    {
                        oneens++;
                    }
                }
                
                var stelling = new StellingModel()
                {
                    Stelling = spelStelling.Stelling,
                    AantalEens = eens,
                    AantalOneens = oneens
                };
                StellingModels.Add(stelling);
            }
        }
    }
}