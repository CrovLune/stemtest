using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain.Identity;
using ST.BL.Domain.Sessies;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Sessies.Pages
{
    public class SchoolSessies : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ISessieManager _sessieManager;
        private readonly IGebruikerManager _gebruikerManager;

        public SchoolSessies(
            UserManager<ApplicationUser> userManager,
            ISessieManager sessieManager,
            IGebruikerManager gebruikerManager
        )
        {
            _userManager = userManager;
            _sessieManager = sessieManager;
            _gebruikerManager = gebruikerManager;
        }

        public List<Sessie> Sessions { get; set; }
        public string ReturnUrl { get; set; }


        public async Task OnGet(string returnUrl = null)
        {
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }

            ReturnUrl = returnUrl;

            var user = await _userManager.GetUserAsync(User);
            var gebruiker = _gebruikerManager.GetGebruikerByIdentity(user);
            Sessions = new List<Sessie>();

            Sessions = _sessieManager
                .GetSessies()
                .Where(s => s.Creator.School.Id == gebruiker.School.Id)
                .ToList();
        }


        public IActionResult OnPostDeleteSessie(int sessieId, string returnUrl)
        {
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }

            ReturnUrl = returnUrl;
            var sessie = _sessieManager.GetSessieById(sessieId);
            _sessieManager.RemoveSessie(sessie);

            return LocalRedirect(returnUrl);
        }
    }
}