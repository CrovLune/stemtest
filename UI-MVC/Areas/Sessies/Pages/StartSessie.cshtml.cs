﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using QRCoder;
using ST.BL.Domain.Sessies;
using ST.BL.Domain.Spellen;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Sessies.Pages
{
    [AllowAnonymous]
    public class StartSessie : PageModel
    {
        private readonly ISessieManager _sessieManager;
        public Sessie Sessie { get; set; } = new Sessie();
        public string QrImage { get; set; }
        public string Location { get; set; }
        public string LocationTeacher { get; set; }

        public StartSessie(
            ISessieManager sessieManager
        )
        {
            _sessieManager = sessieManager;
        }

        [BindProperty] public int SessieId { get; set; }
        [BindProperty] public string CookieVal { get; set; }

        public async Task OnGet(int sessieId)
        {
            var getCookie = HttpContext.Request.Cookies["AnonymousCookie"];
            CookieVal = getCookie;
            if (CookieVal == null)
            {
                await CreateCookie();
                CookieVal = getCookie;
            }

            SessieId = sessieId;
            Sessie = _sessieManager.GetSessieById(sessieId);

            QrImage = GetQRCode(sessieId);

            // if (Sessie.Spel is PartijSpel)
            // {
            //     Location = Url.Page(
            //         "/PartijKeuze",
            //         pageHandler: null,
            //         values: new
            //         {
            //             area = "Speel",
            //             sessieId
            //         },
            //         protocol: Request.Scheme);
            // }
            // else
            // if (Sessie.Spel is DebatSpel)
            // {
                Location = Url.Page(
                    "/StartSpel",
                    pageHandler: null,
                    values: new
                    {
                        area = "Spellen",
                        sessieId
                    },
                    protocol: Request.Scheme);
            // }

            LocationTeacher = Url.Page(
                "/StartSpel",
                pageHandler: null,
                values: new
                {
                    area = "Spellen",
                    sessieId
                },
                protocol: Request.Scheme);
        }

        private string GetQRCode(int sessieId)
        {
            var callbackUrl = Url.Page(
                "/StartSessie",
                pageHandler: null,
                values: new
                {
                    area = "Sessies",
                    sessieId
                },
                protocol: Request.Scheme);
            var ms = new MemoryStream();
            var qrGenerator = new QRCodeGenerator();
            var qrCodeData = qrGenerator.CreateQrCode(callbackUrl, QRCodeGenerator.ECCLevel.Q);
            var qrCode = new QRCode(qrCodeData);
            var qrCodeImage = qrCode.GetGraphic(20);

            qrCodeImage.Save(ms, ImageFormat.Png);
            var result = "data:image/png;base64," + Convert.ToBase64String(ms.ToArray());

            return result;
        }

        private async Task CreateCookie()
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, "AnonCooKie"),
                new Claim(ClaimTypes.Role, "AnonymousUser")
            };
            var claimsIdentity = new ClaimsIdentity(
                claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var authProperties = new AuthenticationProperties
            {
                AllowRefresh = true,
                ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(6000),
                IsPersistent = true
            };

            await HttpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity),
                authProperties);
        }
    }
}