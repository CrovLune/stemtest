﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore.Internal;
using ST.BL.Domain.Antwoorden;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Domain.Partijen;
using ST.BL.Domain.Sessies;
using ST.BL.Domain.Stellingen;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Sessies.Pages
{
    public class SelectedSessie : PageModel
    {
        private readonly ISessieManager _sessieManager;
        private readonly IPartijManager _partijManager;
        private readonly ISpelManager _spelManager;
        private readonly IAntwoordenManager _antwoordenManager;
        private readonly ISpelStellingenManager _spelStellingenManager;

        public SelectedSessie(
            ISessieManager sessieManager,
            IPartijManager partijManager,
            ISpelManager spelManager,
            IAntwoordenManager antwoordenManager,
            ISpelStellingenManager spelStellingenManager
        )
        {
            _sessieManager = sessieManager;
            _partijManager = partijManager;
            _spelManager = spelManager;
            _antwoordenManager = antwoordenManager;
            _spelStellingenManager = spelStellingenManager;
            Sessie = new Sessie();
            PartijenList = new List<Partij>();
            Stellingen = new List<Stelling>();
        }

        public Sessie Sessie { get; set; }
        public List<Partij> PartijenList { get; set; }
        public List<Stelling> Stellingen { get; set; }

        public void OnGet(int sessieId, string returnUrl = null)
        {
            var random = new Random();
            Sessie = _sessieManager.GetSessieById(sessieId);
            PartijenList = _partijManager.GetPartijen().ToList();
            var spel = _spelManager.GetSpelById(Sessie.Spel.SpelId);
            foreach (var spelStelling in spel.SpelStellingen)
            {
                if (spelStelling.Stelling.UserAntwoordOpStelling.Any()) continue;
                
                for (var i = 0; i < 18; i++)
                {
                    var ant = RandomAntwoord(random.Next(100));
                    _antwoordenManager.AddAntwoord(ant);
                    
                    spelStelling.Stelling.UserAntwoordOpStelling.Add(ant);

                }
                _spelStellingenManager.ChangeSpelStelling(spelStelling);
            }

            foreach (var spelStelling in spel.SpelStellingen)
            {
                Stellingen.Add(spelStelling.Stelling);
            }
        }

        public UserAntwoordOpStelling RandomAntwoord(int kans)
        {
            if (kans > 53)
            {
                return new UserAntwoordOpStelling
                {
                    Antwoord = "eens"
                };
            }

            return new UserAntwoordOpStelling
            {
                Antwoord = "oneens"
            };
        }
    }
}