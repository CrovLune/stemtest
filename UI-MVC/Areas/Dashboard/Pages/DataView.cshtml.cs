﻿using System.Linq;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Dashboard.Pages
{
    public class DataView : PageModel
    {
        private readonly IGebruikerManager _gebruikerManager;
        private readonly ISchoolManager _schoolManager;

        public DataView(IGebruikerManager gebruikerManager, ISchoolManager schoolManager)
        {
            _gebruikerManager = gebruikerManager;
            _schoolManager = schoolManager;
        }


        public class Totaal
        {
            public int AlleGebruikers { get; set; }
            public int AlleScholen { get; set; }
            public int AlleTesten { get; set; }
            public int AlleSessies { get; set; }
        }

        public Totaal UserTotaal { get; set; }

        public void OnGet()
        {
            UserTotaal = new Totaal();
            UserTotaal.AlleGebruikers = _gebruikerManager.GetGebruikers().Count();
            UserTotaal.AlleScholen = _schoolManager.GetScholen().Count();
        }
    }
}