using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Domain.Sessies;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Speel.Pages.Debatspel.Leerkracht
{
    public class LeerkrachtSpa : PageModel
    {
        [BindProperty(SupportsGet = true)] public Sessie Sessie { get; set; }

        public string UniqueCodeDecode { get; set; }
        public List<SpelStellingen> SpelStellingen { get; set; }

        private readonly ISessieManager _sessieManager;
        private readonly ISpelStellingenManager _spelStellingenManager;


        public LeerkrachtSpa(ISessieManager sessieManager,
            ISpelStellingenManager spelStellingenManager)
        {
            _sessieManager = sessieManager;
            _spelStellingenManager = spelStellingenManager;
        }

        public void OnGet(int sessieId)
        {
            Sessie = _sessieManager.GetSessieById(sessieId);
            SpelStellingen = _spelStellingenManager.GetSpelStellingen().ToList();

            //UniqueCodeDecode = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(Sessie.UniqueCode));
            //     UniqueCodeDecode = Encoding.UTF8.GetString(WebEncoders.Base64UrlDecode(Sessie.UniqueCode));
            UniqueCodeDecode = Encoding.UTF8.GetString(Convert.FromBase64String(Sessie.UniqueCode));
        }
    }
}