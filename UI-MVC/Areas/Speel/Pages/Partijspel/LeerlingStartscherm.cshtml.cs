﻿﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Speel.Pages.Partijspel
{
    [AllowAnonymous]
    public class LeerlingStartscherm : PageModel
    {
        private readonly ISessieManager _sessieManager;
        [BindProperty(SupportsGet = true)] public string Foutmelding { get; set; }
        [BindProperty(SupportsGet = true)] public string UniekeCode { get; set; }

        public LeerlingStartscherm(ISessieManager spelSessieManager)
        {
            _sessieManager = spelSessieManager;
        }

        public void OnGet()
        {
            
        }

        public IActionResult OnPostAsync()
        {
            if (ModelState.IsValid == false)
                return Page();
            

            var spelSessie = _sessieManager.GetSessies().SingleOrDefault(s => s.UniqueCode.Equals(UniekeCode));

            if (spelSessie == null)
            {
                return RedirectToPage("LeerlingStartscherm", new {Foutmelding = "Geen spelsessie gevonden"});
            }
            return RedirectToPage("LeerlingWachtscherm", new {spelSessieId = spelSessie.Id});
        }
    }
}