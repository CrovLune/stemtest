﻿﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain.Sessies;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Speel.Pages
{
    public class LeerkrachtEindscherm : PageModel
    {
        private readonly ISessieManager _sessieManager;
        public Sessie Sessie { get; set; }

        public LeerkrachtEindscherm(ISessieManager sessieManager)
        {
            _sessieManager = sessieManager;
        }

        public void OnGet(int sessieId)
        {
            Sessie = _sessieManager.GetSessieById(sessieId);
        }
    }
}