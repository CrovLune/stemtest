﻿﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain;
using ST.BL.Domain.Antwoorden;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Domain.Stellingen;
using ST.BL.Domain.Users;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Speel.Pages.Partijspel
{
    [AllowAnonymous]
    public class StellingInvullen : PageModel
    {
        public Stelling CurrentStelling { get; set; }
        public AnoniemeGebruiker AnoniemeGebruiker { get; set; }
        public IEnumerable<SpelStellingen> SpelStellingen { get; set; }
        
        [BindProperty(SupportsGet = true)] public string Antwoord { get; set; }
        [BindProperty(SupportsGet = true)] public int UserAntwoordId { get; set; }
        [BindProperty(SupportsGet = true)] public int GebruikersId { get; set; }
        [BindProperty(SupportsGet = true)] public int CurrentStellingId { get; set; }

        public UserAntwoordOpStelling UserAntwoordOpStelling { get; set; }
        private readonly IAnoniemeGebruikerManager _anoniemeGebruikerManager;
      //  private readonly IUserAntwoordManager _userAntwoordManager;
        private readonly IStellingManager _stellingManager;
        private readonly ISpelStellingenManager _spelStellingenManager;

        public StellingInvullen( IStellingManager stellingManager, IAnoniemeGebruikerManager anoniemeGebruikerManager, ISpelStellingenManager spelStellingenManager)
        {
          //  _userAntwoordManager = userAntwoordManager;
            _stellingManager = stellingManager;
            _anoniemeGebruikerManager = anoniemeGebruikerManager;
            _spelStellingenManager = spelStellingenManager;
        }

        public IActionResult OnGetAsync(int gebruikersId)
        {
            AnoniemeGebruiker = _anoniemeGebruikerManager.GetAnoniemeGebruikerById(gebruikersId);

            SpelStellingen = _spelStellingenManager.GetSpelStellingen()
                .Where(s => s.Spel.SpelId.Equals(AnoniemeGebruiker.Sessie.Spel.SpelId));
            
            // UserAntwoordOpStelling = _userAntwoordManager.GetUserAntwoorden()
            //      .Where(u => u.AnoniemeGebruiker.Id.Equals(AnoniemeGebruiker.Id))
            //      .FirstOrDefault(u => u.Antwoord == null);

            if (UserAntwoordOpStelling == null)
            {
                return RedirectToPage("StellingenBekijken", new { gebruikersId = AnoniemeGebruiker.Id });
            }

            CurrentStelling = UserAntwoordOpStelling?.Stelling;
            
            return Page();
        }
        
        public IActionResult OnPostAsync()
        {
            // var userAntwoord = _userAntwoordManager.GetUserAntwoordById(UserAntwoordId);
            // userAntwoord.Antwoord = Antwoord;
            // _userAntwoordManager.ChangeUserAntwoord(userAntwoord);
            //
            return RedirectToPage("StellingInvullen", new {gebruikersId = GebruikersId});
        }

        public IActionResult OnPostResultAsync()
        {
            return RedirectToPage("StellingenBekijken", new { gebruikersId = GebruikersId });
        }
    }
}