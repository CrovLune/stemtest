﻿﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain;
using ST.BL.Domain.Identity;
using ST.BL.Domain.Partijen;
using ST.BL.Domain.Sessies;
using ST.BL.Domain.Stellingen;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Speel.Pages
{
    public class LeerkrachtWachtscherm : PageModel
    {
        private readonly ISessieManager _sessieManager;
        private readonly IStellingManager _stellingManager;
        private readonly ISpelStellingenManager _spelStellingenManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IGebruikerManager _gebruikerManager;
        public Sessie Sessie { get; set; }
        public Stelling Stelling { get; set; }
        public List<Partij> Partijen { get; set; }
        [BindProperty(SupportsGet = true)] public int SessieId { get; set; }

        public LeerkrachtWachtscherm(ISessieManager sessieManager, IStellingManager stellingManager, UserManager<ApplicationUser> userManager, IGebruikerManager gebruikerManager, ISpelStellingenManager spelStellingenManager)
        {
            _sessieManager = sessieManager;
            _stellingManager = stellingManager;
            _userManager = userManager;
            _gebruikerManager = gebruikerManager;
            _spelStellingenManager = spelStellingenManager;
        }

        public async Task<IActionResult> OnGetAsync(int sessieId)
        {
            var user = await _userManager.GetUserAsync(User);
            var gebruiker = _gebruikerManager.GetGebruikerByIdentity(user);
            Sessie = _sessieManager.GetSessieById(sessieId);
            return Page();
        }

        public IActionResult OnPostAsync()
        {
            //de eerste stelling van het spel die als status closed heeft
            Sessie = _sessieManager.GetSessieById(SessieId);
            var spelStelling = _spelStellingenManager.GetSpelStellingen()
                .Where(s => s.SpelId.Equals(Sessie.Spel.SpelId))
                .FirstOrDefault(s => s.Status.Equals(Status.Closed));

            //als er geen spelstellingen zijn met status open, eindscherm tonen
            if (spelStelling == null)
            {
                return RedirectToPage("LeerkrachtEindscherm", new { sessieId = Sessie.Id });
            }
            else
            {
                spelStelling.Status = Status.Open;
                _spelStellingenManager.ChangeSpelStelling(spelStelling);
                return RedirectToPage("StellingTonen", new {stellingId = spelStelling.StellingId, sessieId = SessieId});
            }
            //return stelling == null ? RedirectToPage("LeerkrachtEindscherm") : RedirectToPage("StellingTonen", new {stellingId = stelling.Id, sessieId = SessieId});
        }
    }
}