﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain.Partijen;
using ST.BL.Domain.Users;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Speel.Pages.Partijspel
{
    [AllowAnonymous]
    public class PartijKeuze : PageModel
    {
        // [BindProperty(SupportsGet = true)] public int PartijId { get; set; }
        // [BindProperty(SupportsGet = true)] public int GebruikersId { get; set; }

        [BindProperty] public InputModel Input { get; set; }

        public AnoniemeGebruiker AnoniemeGebruiker { get; set; }
        public string Location { get; set; }


        public class InputModel
        {
            public List<DetailsPartij> Partijen { get; set; }
            public string Cookie { get; set; }
            public int SessieId { get; set; }
        }

        public class DetailsPartij
        {
            public bool IsChecked { get; set; }
            public Partij Partij { get; set; }
        }

        private readonly IPartijManager _partijManager;
        private readonly IAnoniemeGebruikerManager _anoniemeGebruikerManager;

        public PartijKeuze(IPartijManager partijManager, IAnoniemeGebruikerManager anoniemeGebruikerManager)
        {
            _partijManager = partijManager;
            _anoniemeGebruikerManager = anoniemeGebruikerManager;
        }

        public void OnGet(int sessieId)
        {
            Input = new InputModel();
            Input.Partijen = new List<DetailsPartij>();

            var allePartijen = _partijManager.GetPartijen();
            foreach (var partij in allePartijen)
            {
                DetailsPartij detailsPartij = new DetailsPartij();
                detailsPartij.Partij = partij;
            }

            Input.Cookie = HttpContext.Request.Cookies["AnonymousCookie"];
            Input.SessieId = sessieId;
            
            Location = Url.Page(
                "/StartSpel",
                pageHandler: null,
                values: new
                {
                    area = "Spellen",
                    sessieId
                },
                protocol: Request.Scheme);

        }
    }
}