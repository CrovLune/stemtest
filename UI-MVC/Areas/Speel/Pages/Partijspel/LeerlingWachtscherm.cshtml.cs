﻿﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain.Antwoorden;
using ST.BL.Domain.Sessies;
using ST.BL.Domain.Users;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Speel.Pages.Partijspel
{
    [AllowAnonymous]
    public class LeerlingWachtscherm : PageModel
    {
        private readonly ISessieManager _spelSessieManager;
        private readonly IAnoniemeGebruikerManager _anoniemeGebruikerManager;
     //   private readonly IUserAntwoordManager _userAntwoordManager;
        private readonly IStellingManager _stellingManager;
        public Sessie Sessie { get; set; }
        public AnoniemeGebruiker AnoniemeGebruiker { get; set; }
        [BindProperty(SupportsGet = true)] public int GebruikersId { get; set; }

        public LeerlingWachtscherm(
            ISessieManager spelSessieManager,
           // IUserAntwoordManager userAntwoordManager, 
            IAnoniemeGebruikerManager anoniemeGebruikerManager, IStellingManager stellingManager)
        {
            _spelSessieManager = spelSessieManager;
         //   _userAntwoordManager = userAntwoordManager;
            _anoniemeGebruikerManager = anoniemeGebruikerManager;
            _stellingManager = stellingManager;
        }

        public Sessie GetSpelSessie(int id)
        {
            return _spelSessieManager.GetSessies().Single(s => s.Id.Equals(id));
        }

        public IActionResult OnGetAsync(int spelSessieId)
        {
            Sessie = GetSpelSessie(spelSessieId);
            
            if (AnoniemeGebruiker == null)
            {
                AnoniemeGebruiker = MaakAnoniemeGebruiker();
                return Page();
            }
            return Page();
        }

        private AnoniemeGebruiker MaakAnoniemeGebruiker()
        {
            //gebruiker aanmaken
            var anon = new AnoniemeGebruiker()
            {
                Sessie = _spelSessieManager.GetSessieById(Sessie.Id)
            };
            anon = _anoniemeGebruikerManager.AddAnoniemeGebruiker(anon);
            
            //voor elke stelling van het spel een UserAntwoord aanmaken
            foreach (var spelStelling in Sessie.Spel.SpelStellingen)
            {
                var userAntwoord = new UserAntwoordOpStelling()
                {
                    AnoniemeGebruiker = _anoniemeGebruikerManager.GetAnoniemeGebruikerById(anon.Id),
                    Stelling = _stellingManager.GetStellingById(spelStelling.StellingId)
                };
              // _userAntwoordManager.AddUserAntwoord(userAntwoord);
            }
            return anon;
        }
        
        public IActionResult OnPostAsync()
        {
            return RedirectToPage("PartijKeuze", new { gebruikersId = GebruikersId });
        }
    }
}