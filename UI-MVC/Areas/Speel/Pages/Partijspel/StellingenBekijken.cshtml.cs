﻿﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain.Users;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Speel.Pages.Partijspel
{
    [AllowAnonymous]
    public class StellingenBekijken : PageModel
    {
        private readonly IAnoniemeGebruikerManager _anoniemeGebruikerManager;
       // private readonly IUserAntwoordManager _userAntwoordManager;

        public AnoniemeGebruiker AnoniemeGebruiker { get; set; }
        [BindProperty(SupportsGet = true)] public int UserAntwoordId { get; set; }
        [BindProperty(SupportsGet = true)] public int GebruikersId { get; set; }

        public StellingenBekijken(IAnoniemeGebruikerManager anoniemeGebruikerManager) //IUserAntwoordManager userAntwoordManager)
        {
            _anoniemeGebruikerManager = anoniemeGebruikerManager;
         //   _userAntwoordManager = userAntwoordManager;
        }

        public IActionResult OnGetAsync(int gebruikersId)
        {
            AnoniemeGebruiker = _anoniemeGebruikerManager.GetAnoniemeGebruikerById(gebruikersId);

            return Page();
        }
        
        public IActionResult OnPostAsync()
        {
            //Antwoord leeg maken en opslaan
            // var userAntwoord = _userAntwoordManager.GetUserAntwoordById(UserAntwoordId);
            // userAntwoord.Antwoord = null;
            // _userAntwoordManager.ChangeUserAntwoord(userAntwoord);
            
            return RedirectToPage("StellingInvullen", new { gebruikersId = GebruikersId });
        }
        
        public IActionResult OnPostResultAsync()
        {
            return RedirectToPage("Resultatenscherm", new { gebruikersId = GebruikersId });
        }
    }
}