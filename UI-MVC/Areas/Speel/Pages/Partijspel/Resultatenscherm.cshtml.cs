﻿﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain.Sessies;
using ST.BL.Domain.Users;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Speel.Pages.Partijspel
{
    [AllowAnonymous]
    public class Resultatenscherm : PageModel
    {
        private readonly IAnoniemeGebruikerManager _anoniemeGebruikerManager;
        private readonly ISessieManager _sessieManager;

        public AnoniemeGebruiker AnoniemeGebruiker { get; set; }
        public Sessie Sessie { get; set; }
        [BindProperty(SupportsGet = true)] public int IngegevenAntwoordId { get; set; }
        [BindProperty(SupportsGet = true)] public int GebruikersId { get; set; }

        public Resultatenscherm(IAnoniemeGebruikerManager anoniemeGebruikerManager, ISessieManager sessieManager)
        {
            _anoniemeGebruikerManager = anoniemeGebruikerManager;
            _sessieManager = sessieManager;
        }

        public IActionResult OnGetAsync(int gebruikersId)
        {
            AnoniemeGebruiker = _anoniemeGebruikerManager.GetAnoniemeGebruikerById(gebruikersId);
            Sessie = _sessieManager.GetSessieById(AnoniemeGebruiker.Sessie.Id);
            
            
            return Page();
        }
    }
}