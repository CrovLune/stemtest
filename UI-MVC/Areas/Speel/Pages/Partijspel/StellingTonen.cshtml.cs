﻿﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain;
using ST.BL.Domain.Sessies;
using ST.BL.Domain.Stellingen;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Speel.Pages
{
    public class StellingTonen : PageModel
    {
        private readonly IStellingManager _stellingManager;
        private readonly ISessieManager _sessieManager;
        private readonly ISpelStellingenManager _spelStellingenManager;
        public Stelling Stelling { get; set; }
        public Sessie Sessie { get; set; }
        [BindProperty(SupportsGet = true)] public int SessieId { get; set; }

        public StellingTonen(IStellingManager stellingManager, ISessieManager sessieManager, ISpelStellingenManager spelStellingenManager)
        {
            _stellingManager = stellingManager;
            _sessieManager = sessieManager;
            _spelStellingenManager = spelStellingenManager;
        }

        public void OnGet(int stellingId, int sessieId)
        {
            Sessie = _sessieManager.GetSessieById(sessieId);
            Stelling = _stellingManager.GetStellingById(stellingId);
        }

        public IActionResult OnPostAsync()
        {
            //de eerste stelling van het spel die als status closed heeft
            Sessie = _sessieManager.GetSessieById(SessieId);
            var spelStelling = _spelStellingenManager.GetSpelStellingen()
                .Where(s => s.SpelId.Equals(Sessie.Spel.SpelId))
                .FirstOrDefault(s => s.Status.Equals(Status.Closed));

            //als er geen spelstellingen zijn met status closed, eindscherm tonen
            if (spelStelling == null)
            {
                return RedirectToPage("LeerkrachtEindscherm", new { sessieId = Sessie.Id });
            }
            else
            {
                spelStelling.Status = Status.Open;
                _spelStellingenManager.ChangeSpelStelling(spelStelling);
                return RedirectToPage("StellingTonen", new {stellingId = spelStelling.StellingId, sessieId = SessieId});
            }
        }
    }
}