﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain.Identity;
using ST.BL.Domain.Users;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Gebruikers.Pages
{
    public class Gebruikers : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IGebruikerManager _gebruikerManager;

        public Gebruikers(
            UserManager<ApplicationUser> userManager,
            IGebruikerManager gebruikerManager
        )
        {
            _userManager = userManager;
            _gebruikerManager = gebruikerManager;
        }

        [BindProperty] public InputModel Input { get; set; }

        public class InputModel
        {
            public List<DetailsLeerkracht> DetailsLeerkrachten { get; set; }
        }

        public string ReturnUrl { get; set; }
        public DetailsLeerkracht Leerkracht { get; set; }

        public class DetailsLeerkracht
        {
            public Gebruiker Leerkracht { get; set; }
            public bool IsChecked { get; set; }
        }

        public async Task OnGet(string returnUrl = null)
        {
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }

            ReturnUrl = returnUrl;

            var user = await _userManager.GetUserAsync(User);
            var gebruiker = _gebruikerManager.GetGebruikerByIdentity(user);
            var Leerkrachten = new List<Gebruiker>();

            Leerkrachten = _gebruikerManager
                .GetGebruikers()
                .ToList();
            foreach (var L in Leerkrachten)
            {
                DetailsLeerkracht detailsLeerkracht = new DetailsLeerkracht();
                detailsLeerkracht.Leerkracht = L;
                detailsLeerkracht.IsChecked = false;
                Input.DetailsLeerkrachten.Add(detailsLeerkracht);
            }
        }

        public IActionResult OnPostDeleteAsync(int[] cb, string returnUrl = null)
        {
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }

            ReturnUrl = returnUrl;

            var ids = cb;
            var verwijderenGebruiker = _gebruikerManager.GetGebruikers().Where(g => ids.Contains(g.Id)).ToList();
            foreach (var g in verwijderenGebruiker)
            {
                var opgehaaldeGebruiker = _gebruikerManager.GetGebruikerById(g.Id);
                _gebruikerManager.RemoveGebruiker(opgehaaldeGebruiker);
            }

            return LocalRedirect(ReturnUrl);
        }
    }
}