using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain.Identity;
using ST.BL.Interfaces;
using ST.DAL.Interfaces;

namespace UI_MVC.Areas.Gebruikers.Pages
{
    public class EditGebruiker : PageModel
    {
        private readonly ISchoolManager _schoolManager;

        private readonly IGebruikerManager _gebruikerManager;
        [BindProperty] public InputModel Input { get; set; }
        public string ReturnUrl { get; set; }

        public EditGebruiker(IGebruikerManager gebruikerManager,
            ISchoolManager schoolManager)
        {
            _gebruikerManager = gebruikerManager;
            _schoolManager = schoolManager;
        }

        public class InputModel
        {
            public int Id { get; set; }

            [Required]
            [Display(Name = "Username")]
            public string Username { get; set; }

            [Required]
            [Display(Name = "Voornaam")]
            public string Voornaam { get; set; }

            [Required]
            [Display(Name = "Achternaam")]
            public string Achternaam { get; set; }

            [Required] [Display(Name = "Email")] public string Email { get; set; }

            [Required] [Display(Name = "School")] public string School { get; set; }
            public int SchoolId { get; set; }
        }

        public void OnGet(int id, string returnUrl = null)
        {
            var gekozenGebruiker = _gebruikerManager.GetGebruikerById(id);
            Input = new InputModel
            {
                Id = id,
                Username = gekozenGebruiker.Username,
                Voornaam = gekozenGebruiker.Voornaam,
                Achternaam = gekozenGebruiker.Achternaam,
                Email = gekozenGebruiker.Email,
                School = gekozenGebruiker.School.Naam,
                SchoolId = gekozenGebruiker.School.Id
            };
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }

            ReturnUrl = returnUrl;
        }

        public IActionResult OnPostEditGebruiker(int id, string returnUrl = null)
        {
            var opgehaaldeGebruiker = _gebruikerManager.GetGebruikerById(id);
            opgehaaldeGebruiker.Username = Input.Username;
            opgehaaldeGebruiker.Voornaam = Input.Voornaam;
            opgehaaldeGebruiker.Achternaam = Input.Achternaam;
            opgehaaldeGebruiker.Email = Input.Email;
            opgehaaldeGebruiker.School = _schoolManager.GetScholen().Single(s => s.Naam.Equals(Input.School));
            _gebruikerManager.ChangeGebruiker(opgehaaldeGebruiker);

            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }

            ReturnUrl = returnUrl;
            return LocalRedirect(ReturnUrl);
        }
    }
}