using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using NETCore.MailKit.Core;
using ST.BL.Domain.Users;
using ST.BL.Interfaces;
using ST.BL.Domain.Identity;
using ST.BL.Domain.Scholen;

// ReSharper disable StringLiteralTypo
// ReSharper disable CommentTypo
// ReSharper disable ClassNeverInstantiated.Global

namespace UI_MVC.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    [SuppressMessage("ReSharper", "IdentifierTypo")]
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly ISchoolManager _schoolManager;
        private readonly IGebruikerManager _gebruikerManager;

        public RegisterModel(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            RoleManager<ApplicationRole> roleManager,
            ILogger<RegisterModel> logger,
            ISchoolManager schoolManager,
            IGebruikerManager gebruikerManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _logger = logger;
            _schoolManager = schoolManager;
            _gebruikerManager = gebruikerManager;
        }

        [BindProperty] public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }


        public class InputModel
        {
            [Required]
            [Display(Name = "FirstName")]
            public string FirstName { get; set; }

            [Required]
            [Display(Name = "LastName")]
            public string LastName { get; set; }

            [Required] [Display(Name = "School")] public string School { get; set; }

            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.",
                MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }
        }

        public void OnGet(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl ??= Url.Content("~/");

            if (!ModelState.IsValid) return Page();

            //* Create/Look-up School
            //*************************************************************************//
            School resolvedSchool = null;
            try
            {
                //* Search school in database
                var schoolFound = _schoolManager
                    .GetScholen()
                    .Any(s => s.Naam.Equals(Input.School));

                if (!schoolFound)
                {
                    resolvedSchool = new School
                    {
                        Naam = Input.School,
                        Adres = new Adres(),
                        Gebruikers = new List<Gebruiker>(),
                        Klassen = new List<Klas>(),
                        MailContactpersoon = "Onbekend"
                    };
                    var resultSchool = _schoolManager.AddSchool(resolvedSchool);
                }
                else
                {
                    resolvedSchool = _schoolManager
                        .GetScholen()
                        .First(s => s.Naam.Equals(Input.School));
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine("School not found or Created" + exception);
            }
            //*************************************************************************//

            //* Register Functionality
            //? Create User<GeregistreerdeGebruiker>
            var gebruiker = new Gebruiker()
            {
                Username = Input.Email,
                Achternaam = Input.LastName,
                Voornaam = Input.FirstName,
                Email = Input.Email,
                School = resolvedSchool
            };
            //* Add User<GeregistreerdeGebruiker> to gebruikerRepo
            var resultGebruiker = _gebruikerManager.AddGebruiker(gebruiker);
            //? Create User<Identity>
            var user = new ApplicationUser
            {
                UserName = Input.Email,
                Email = Input.Email,
                FirstName = Input.FirstName,
                Gebruiker = gebruiker
            };
            var resultUserAdded = await _userManager.CreateAsync(user, Input.Password);
            if (!resultUserAdded.Succeeded)
            {
                _gebruikerManager.RemoveGebruiker(resultGebruiker);
                _schoolManager.RemoveSchool(resolvedSchool);
            }

            //? Add Role to User<Identity>
            const string roleName = "RegisteredUser";
            var roleAvailable = await _roleManager.RoleExistsAsync(roleName);
            if (!roleAvailable)
            {
                await _roleManager.CreateAsync(new ApplicationRole {Name = roleName});
            }

            var role = await _roleManager.FindByNameAsync(roleName);


          
            if (resultUserAdded.Succeeded)
            {
                var resultUserAddRole = await _userManager.AddToRoleAsync(user, role.Name);

                if (resultUserAddRole.Succeeded)
                {
                    _logger.LogInformation($"User created a new account with password and role [{role.Name}]");

                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                    var callbackUrl = Url.Page(
                        "/Account/ConfirmEmail",
                        pageHandler: null,
                        values: new
                        {
                            area = "Identity",
                            userId = user.Id,
                            code
                        },
                        protocol: Request.Scheme);

                 

                    if (_userManager.Options.SignIn.RequireConfirmedAccount)
                    {
                        return RedirectToPage("RegisterConfirmation", new
                        {
                            email = Input.Email
                        });
                    }

                    await _signInManager.SignInAsync(user, false);
                    return LocalRedirect(returnUrl);
                }
            }

            foreach (var error in resultUserAdded.Errors)
            {
                ModelState.AddModelError(
                    string.Empty,
                    error.Description
                );
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}