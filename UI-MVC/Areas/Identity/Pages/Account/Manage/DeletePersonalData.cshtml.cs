﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using ST.BL.Interfaces;
using ST.BL.Domain.Identity;
using ST.BL.Domain.Users;

namespace UI_MVC.Areas.Identity.Pages.Account.Manage
{
    public class DeletePersonalData : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger<DeletePersonalData> _logger;
        private readonly IGebruikerManager _gebruikerManager;

        public DeletePersonalData(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<DeletePersonalData> logger,
            IGebruikerManager gebruikerManager
        )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _gebruikerManager = gebruikerManager;
        }

        [BindProperty] public DeletePersonalDataModel Input { get; set; }
        public bool RequirePassword { get; set; }
        
        //* Model
        public class DeletePersonalDataModel
        {
            [Required]
            [DataType(DataType.Password)]
            public string Password { get; set; }
        }


        public async Task<IActionResult> OnGet()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            RequirePassword = await _userManager.HasPasswordAsync(user);
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            RequirePassword = await _userManager.HasPasswordAsync(user);
            if (RequirePassword)
            {
                if (!await _userManager.CheckPasswordAsync(user, Input.Password))
                {
                    ModelState.AddModelError(string.Empty, "Incorrect password.");
                    return Page();
                }
            }

            var gebruiker = _gebruikerManager.GetGebruikerByIdentity(user);
            _logger.LogInformation(gebruiker == null
                ? $"User {user.FirstName} NOT FOUND in Database"
                : $"User FOUND: {user.FirstName} with UserName {gebruiker.Username} in Database"
            );

            //! Delete User from Registered User Database
            if (gebruiker != null)
            {
                _gebruikerManager.RemoveGebruiker(gebruiker);
            }

            //! Delete Identity User
            var userId = await _userManager.GetUserIdAsync(user);
            var result = await _userManager.DeleteAsync(user);
            if (!result.Succeeded)
            {
                throw new InvalidOperationException($"Unexpected error occurred deleting user with ID '{userId}'.");
            }

            await _signInManager.SignOutAsync();

            _logger.LogInformation("User with ID '{UserId}' deleted themselves.", userId);

            return Redirect("~/");
        }
    }
}