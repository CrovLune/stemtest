﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using ST.BL.Domain;
using ST.BL.Domain.Antwoorden;
using ST.BL.Domain.Identity;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Domain.Partijen;
using ST.BL.Domain.Scholen;
using ST.BL.Domain.Sessies;
using ST.BL.Domain.Spellen;
using ST.BL.Domain.Stellingen;
using ST.BL.Domain.Users;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Identity.Pages.Account.Manage
{
    [SuppressMessage("ReSharper", "IdentifierTypo")]
    public class DevTools : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<DevTools> _logger;
        private readonly IGebruikerKlassenManager _gebruikerKlassenManager;
        private readonly ISpelManager _spelManager;
        private readonly IGebruikerStellingenManager _gebruikerStellingenManager;
        private readonly IStellingManager _stellingManager;
        private readonly ISessieManager _sessieManager;
        private readonly IGebruikerManager _gebruikerManager;
        private readonly IPartijManager _partijManager;

        public string StatusMessage { get; set; }

        public DevTools(
            UserManager<ApplicationUser> userManager,
            ILogger<DevTools> logger,
            IGebruikerKlassenManager gebruikerKlassenManager,
            ISpelManager spelManager,
            IGebruikerStellingenManager gebruikerStellingenManager,
            IStellingManager stellingManager,
            ISessieManager sessieManager,
            IGebruikerManager gebruikerManager,
            IPartijManager partijManager
        )
        {
            _userManager = userManager;
            _logger = logger;
            _gebruikerKlassenManager = gebruikerKlassenManager;
            _spelManager = spelManager;
            _gebruikerStellingenManager = gebruikerStellingenManager;
            _stellingManager = stellingManager;
            _sessieManager = sessieManager;
            _gebruikerManager = gebruikerManager;
            _partijManager = partijManager;
        }

        [BindProperty] public Gebruiker Gebruiker { get; set; }
        [BindProperty] public List<GebruikerKlassen> Classes { get; set; }
        [BindProperty] public List<Stelling> Stellingen { get; set; }
        [BindProperty] public List<Sessie> Sessies { get; set; }
        [BindProperty] public List<Partij> Partijen { get; set; }

        public async Task OnGet()
        {
            var user = await _userManager.GetUserAsync(User);
            Gebruiker = new Gebruiker();
            Gebruiker = _gebruikerManager.GetGebruikerByIdentity(user);
            Classes = new List<GebruikerKlassen>();
            Classes = _gebruikerKlassenManager
                .GetGebruikerKlassen()
                .Where(g => g.GebruikerId == Gebruiker.Id)
                .ToList();
            Stellingen = new List<Stelling>();
            Stellingen = _stellingManager.GetStellingen().ToList();
            Sessies = new List<Sessie>();
            Sessies = _sessieManager
                .GetSessies()
                .Where(s => s.Creator.Equals(Gebruiker))
                .AsEnumerable()
                .ToList();

            Partijen = new List<Partij>();
            Partijen = _partijManager
                .GetPartijen()
                .ToList();
        }

        public IActionResult OnPostAddPartijAsync()
        {
            var parts = new List<Partij>
            {
                new Partij
                {
                    Naam = "CD&V",
                    Beschrijving = "",
                    PartijInfo = new PartijInfo{Info = "CD&V - de Vlaamse christendemocraten - is meer nog dan een politieke partij, een brede beweging van geëngageerde mensen. CD&V profileert zich als enthousiaste partner van gezinnen, ondernemers en verenigingen. Met een hecht team van ervaren en bekwame politici versterken en ondersteunen we uw verhaal. Want wij liggen wakker van datgene waar u van droomt. En… we maken er graag samen werk van!"},
                    VisieOmschrijving = "",
                    VolledigeNaam = "Christen-Democratisch en Vlaams"
                },
                new Partij
                {
                    Naam = "sp.a",
                    Beschrijving = "",
                    PartijInfo = new PartijInfo{Info = "Om het samen beter te maken voor iedereen - zonder onderscheid - is een sterke socialistische partij nodig. Dat heeft de geschiedenis al aangetoond. Dit jaar bestaat de sociale zekerheid 75 jaar. Destijds was het onze verdienste dat die zekerheid voor de mensen er kwam.Vandaag leven we opnieuw in tijden waarin niets nog zeker lijkt. Of het nu een deftig pensioen is, een job die loont om de facturen te betalen of de beste en betaalbare zorg wanneer die nodig is. Wij vinden die onzekerheid niet normaal.Daarom is het aan ons om opnieuw voor die zekerheid te zorgen. Voor iedereen. En de reden waarom sp.a dat moet doen, is heel simpel: omdat niemand anders het zal doen.Zekerheid. Voor iedereen. #NieuweStrijd"},
                    VisieOmschrijving = "",
                    VolledigeNaam = "Socialistische Partij Anders"
                },
                new Partij
                {
                    Naam = "openVLD",
                    Beschrijving = "",
                    PartijInfo = new PartijInfo{Info = "OpenVld is een liberale democratische partij die resoluut kiest voor een open samenleving. Een samenleving waarin elke persoon zoveel mogelijk kansen krijgt om zijn of haar leven uit te bouwen. Niet om zichzelf ten koste van anderen te realiseren, maar om op een vrije en verantwoordelijke manier bij te dragen tot die open samenleving."},
                    VisieOmschrijving = "",
                    VolledigeNaam = "Open Vlaams Liberalen en Democraten"
                },
                new Partij
                {
                    Naam = "Groen",
                    Beschrijving = "",
                    PartijInfo = new PartijInfo{Info = "Met het verhaal dat het allemaal slecht gaat, nemen we geen genoegen. Duizenden doeners tonen dat het anders kan. Samen bouwen we aan een menselijkere, eerlijkere en gezondere samenleving. Overheid en economie bestaan niet voor zichzelf. Ze staan ten dienste van mensen. Wie een job wil, verdient werkbaar werk. Wie nood heeft aan hulp, krijgt gepaste en betaalbare zorg op het juiste moment. Wie kinderen heeft, moet kunnen vertrouwen op kwaliteitsvolle opvang en onderwijs. Wie de ouderdom voelt wegen, heeft recht op een omgeving om waardig te leven. En wie z'n thuisland moest ontvluchten, kan hier rekenen op een menselijke behandeling. Ongelijkheid geeft in het beste geval een handvol winnaars. Groen plaatst eerlijkheid en gelijke kansen voorop. Dat begint bij de belastingen. Vervuilers en grote vermogens moeten meer bijdragen dan mensen die werken en ondernemen. Grote bedrijven mogen de dans niet ontspringen. Bovendien heeft iedereen recht op betaalbaar wonen en een menswaardig inkomen. Armoede aanvaarden we niet. En onze samenleving mag niemand uitsluiten op basis van leeftijd, geloof, etnisch-culturele achtergrond, seksualiteit of gender. De mens heeft nog nooit zo'n grote impact gehad op het milieu. Hoog tijd voor actie. Voor een klimaat dat niet op hol slaat, doordat we met hernieuwbare energie de CO2-uitstoot verminderen in plaats van fossiele brandstoffen te blijven stoken. Ruimte voor natuur en bossen: behouden wat er is en extra planten waar te weinig is, ook dichtbij woonkernen. Respectvol omgaan met dieren. Een evenwicht waarbij levenskwaliteit voor mens en dier hand in hand gaat met propere lucht, zuiver water en vruchtbare grond. Als we onze omgeving met zorg behandelen, staan we ook zelf gezonder in het leven."},
                    VisieOmschrijving = "",
                    VolledigeNaam = "Groen"
                },
                new Partij
                {
                    Naam = "N-VA",
                    Beschrijving = "",
                    PartijInfo = new PartijInfo{Info = "De N-VA is een relatief jonge politieke partij die een moderne, vooruitziende en democratische vorm van het Vlaamse Nationalisme promoot. De N-VA verdedigt de politieke, sociale, economische en culturele interesses van 6.5miljoen mensen in Vlaanderen door het"},
                    VisieOmschrijving = "",
                    VolledigeNaam = "Nieuwe-Vlaamse-Alliantie"
                },
                new Partij
                {
                    Naam = "VB",
                    Beschrijving = "",
                    PartijInfo = new PartijInfo{Info = "De partij is de partijpolitieke tolk van de Vlaamse Beweging, zoals die historisch is gegroeid, en ze verdedigt op het politieke forum de eisen van die Vlaamse Beweging, zoals ondermeer het zelfbestuur, de afschaffing van de faciliteiten, de terugkeer van de ons ontstolen gebieden, het nooit-meer-oorlog en de amnestie-eis.De partij is een Vlaams-nationalistische partij, een instrument voor een politiek van nationale en culturele identiteit in Vlaanderen. Dit betekent dat de partij met haar politieke actie ervoor wil zorgen dat culturele identiteit en volksgemeenschap (mede) bepalend worden voor de inrichting en het bestuur van de staat. De staat is slechts een structuur en is op grond van het zelfbeschikkingsrecht ondergeschikt aan de volksgemeenschap. De staat dient de belangen van het volk en niet omgekeerd.De partij is tevens een rechts-nationalistische partij, omdat zij de vrije mens erkent zoals hij is en dus de ideologieën verwerpt die van de maakbaarheid van de mens uitgaan. Tradities, waarden en normen, zoals die zijn gegroeid, moeten gerespecteerd worden en mee deel uitmaken van de manier waarop de toekomst vorm wordt gegeven."},
                    VisieOmschrijving = "",
                    VolledigeNaam = "Vlaams Belang"
                },
                new Partij
                {
                    Naam = "PVDA",
                    Beschrijving = "",
                    PartijInfo = new PartijInfo{Info = "Eerst de mensen, niet de winst. Niets typeert de PVDA (Partij van de Arbeid) beter dan dit uitgangspunt. Mensen zijn geen nummer. Een mens is meer. Een samenleving is geen jackpot. Een samenleving is meer.De commercie wint steeds meer veld. Gezondheid, werk, onderwijs, milieu, inspraak, internationale solidariteit en vrede worden opgeofferd aan het koude geldgewin. Wij willen de prioriteiten opnieuw juist stellen. De samenleving weer op haar voeten zetten. Vandaar ‘eerst de mensen, niet de winst’."},
                    VisieOmschrijving = "",
                    VolledigeNaam = "Partij van de Arbeid"
                }
            };
            foreach (var partij in parts)
            {
                _partijManager.AddPartij(partij);
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAddClassAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var gebruiker = _gebruikerManager.GetGebruikerByIdentity(user);

            var klas = ClassChecker(gebruiker);

            if (klas == null) return Page();

            var gebKlas = new GebruikerKlassen {klas = klas, Gebruiker = gebruiker};
            _gebruikerKlassenManager.AddGebruikerKlassen(gebKlas);

            _logger.LogInformation($"Klas {klas.Naam} added.");

            return Page();
        }

        private Klas RandomClassGenerator(Gebruiker gebruiker)
        {
            var randomInt = new Random();
            return new Klas
            {
                AantalLeerlingen = randomInt.Next(1, 20),
                Naam = $"INF{randomInt.Next(200, 210)}{Convert.ToChar(randomInt.Next(65, 67))}",
                School = gebruiker.School,
                StudieJaar = randomInt.Next(2019, 2020)
            };
        }

        private Klas ClassChecker(Gebruiker gebruiker)
        {
            bool classExists;
            int classCount;
            Klas klas;
            var classes = _gebruikerKlassenManager
                .GetGebruikerKlassen()
                .Where(g => g.GebruikerId == gebruiker.Id)
                .ToList();
            do
            {
                klas = RandomClassGenerator(gebruiker);
                classExists = classes.Any(k => k.klas.Naam.Equals(klas.Naam));
                classCount = classes.Count;
            } while (classExists && classCount < 20);

            if (!classExists) return klas;
            StatusMessage = $"You have already added all available classes";
            return null;
        }

        private Sessie RandomSessieGenerator(Gebruiker gebruiker, Spel spel)
        {
            var random = new Random();
            var spelSessie = new Sessie
            {
                AantalDeelnemers = random.Next(20),
                Name = $"randomName{random.Next(100)}",
                UniqueCode = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(RandomString(7))),
                Creator = gebruiker,
                Klas = RandomClassGenerator(gebruiker),
                Status = Status.Pending,
                Spel = spel
            };
            _gebruikerKlassenManager.AddGebruikerKlassen(new GebruikerKlassen
                {Gebruiker = gebruiker, klas = spelSessie.Klas});
            return spelSessie;
        }

        private string RandomString(int length)
        {
            var random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private Spel RandomSpelGenerator(Gebruiker gebruiker)
        {
            Spel spel;
            var random = new Random();
            var list = new List<Stelling>
            {
                RandomStellingGenerator(),
                RandomStellingGenerator(),
                RandomStellingGenerator(),
                RandomStellingGenerator()
            };
            foreach (var stelling in list)
            {
                var gebStel = new GebruikerStellingen {Gebruiker = gebruiker, Stelling = stelling};
                _gebruikerStellingenManager.AddGebruikerStelling(gebStel);
            }

            if (random.Next(100) > 50)
            {
                spel = new PartijSpel
                    {
                        Creator = gebruiker,
                        options = new Options(),
                        Status = Status.Pending,
                        Name = $"PartijSpel - Brexit",
                        Stellingen = list
                    }
                    ;
            }
            else
            {
                spel = new DebatSpel
                {
                    Creator = gebruiker,
                    options = new Options(),
                    Status = Status.Pending,
                    UserArgument = "user argument",
                    Name = $"DebatSpel - Brexit",
                    Stellingen = list
                };
            }

            return spel;
        }

        public async Task<IActionResult> OnPostAddStellingAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var gebruiker = _gebruikerManager.GetGebruikerByIdentity(user);
            var spel = RandomSpelGenerator(gebruiker);

            _spelManager.AddSpel(spel);
            
            return Page();
        }

        private Stelling RandomStellingGenerator()
        {
            var random = new Random();
            var stelling = new Stelling
            {
                MoeilijkeWoorden = new List<MoeilijkWoord>
                {
                    new MoeilijkWoord
                    {
                        Uitleg = "Moeilijkwoord Uitleg",
                        Woord = "Moeilijkwoord"
                    }
                },
                PartijAntwoordOpStelling = new List<PartijAntwoordOpStelling>
                {
                    new PartijAntwoordOpStelling
                    {
                        Antwoord = "Eens",
                        Uitleg = "partij uitleg bij antwoord",
                        Partij = new Partij
                        {
                            Beschrijving = "bescrhijving partij",
                            Logo = "partij logo Bitmap",
                            Naam = "Partij naam",
                            PartijInfo = new PartijInfo {Info = "partij info", Video = "partij Video bitmap"},
                            VisieOmschrijving = "Partij Visie Omschrijving",
                            VolledigeNaam = "Volledige Naam van het Partij"
                        }
                    },
                    new PartijAntwoordOpStelling
                    {
                        Antwoord = "eens",
                        Uitleg = "partij uitleg bij antwoord2",
                        Partij = new Partij
                        {
                            Beschrijving = "bescrhijving partij",
                            Logo = "partij logo Bitmap",
                            Naam = "Partij naam2",
                            PartijInfo = new PartijInfo {Info = "partij info", Video = "partij Video bitmap"},
                            VisieOmschrijving = "Partij Visie Omschrijving",
                            VolledigeNaam = "Volledige Naam van het Partij"
                        }
                    }
                },
                UserAntwoordOpStelling = new List<UserAntwoordOpStelling>(),
                Vraag = "Dit is de vraag?",
                StellingOnderwerp = $"Dit is het onderwerp {random.Next(100)}",
                Uitleg = "Dit is het Uitleg van de Stelling",
                Status = Status.Pending
            };
            return stelling;
        }
    }
}