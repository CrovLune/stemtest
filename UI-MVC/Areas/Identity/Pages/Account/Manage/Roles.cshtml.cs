﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using ST.BL.Domain.Identity;

namespace UI_MVC.Areas.Identity.Pages.Account.Manage
{
    public class RolesModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly ILogger<RolesModel> _logger;

        public RolesModel(
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            ILogger<RolesModel> logger
        )
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _logger = logger;
        }

        public ERoles Roles { get; set; }
        [BindProperty] public string SelectedRole { get; set; }
        [BindProperty] public string SelectedForDelete { get; set; }


        public enum ERoles
        {
            SuperAdmin = 0,
            Admin = 1,
            RegisteredUser = 2,
            AnonymousUser = 3
        }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var user = _userManager.GetUserAsync(User).Result;
            int.TryParse(SelectedRole, out var roleId);
            var roleName = ((ERoles) roleId).ToString();

            var roleFound = await _roleManager.FindByNameAsync(roleName);
            if (roleFound == null)
            {
                var role = new ApplicationRole
                {
                    Name = roleName
                };
                await _roleManager.CreateAsync(role);
            }

            var userRoles = await _userManager.GetRolesAsync(user);

            //* Delete Role from User
            if (SelectedForDelete != null)
            {
                if (userRoles.Contains(SelectedForDelete))
                {
                    var deleteRole = await _userManager.RemoveFromRoleAsync(user, SelectedForDelete);
                    if (deleteRole.Succeeded)
                    {
                        _logger.LogInformation($"{user.FirstName} removed from role {SelectedForDelete}");
                        return RedirectToPage();
                    }
                }

                _logger.LogInformation($"{user.FirstName} doesnt have the role {SelectedForDelete}");
                return RedirectToPage();
            }

            if (SelectedRole != null)
            {
                if (!userRoles.Contains(SelectedRole))
                {
                    var addRole = await _userManager.AddToRoleAsync(user, roleName);
                    if (addRole.Succeeded)
                    {
                        _logger.LogInformation($"{user.FirstName} added to role {roleName}");
                        return RedirectToPage();
                    }
                }

                _logger.LogInformation($"{user.FirstName} already has role {roleName}");
                return RedirectToPage();
            }

            return RedirectToAction("Fail", "Home");
        }
    }
}