﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.RazorPages;


namespace UI_MVC.Areas.Identity.Pages.Account
{
    [Authorize(Policy = "RequireElevatedRights")]
    public class SecretPage : PageModel
    {
        public void OnGet()
        {
            
        }
    }
}