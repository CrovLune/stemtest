﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain.Partijen;
using ST.BL.Interfaces;
using UI_MVC.Areas.Scholen.Pages;

namespace UI_MVC.Areas.Partijen.Pages
{
    public class EditPartij : PageModel
    {
        private readonly IPartijManager _partijManager;
        public Partij Partij { get; set; }
        [BindProperty] public InputModel Input { get; set; }
        public string ReturnUrl { get; set; }

        public EditPartij(IPartijManager partijManager)
        {
            _partijManager = partijManager;
        }

        public class InputModel
        {
            public int Id { get; set; }

            [Required]
            [Display(Name = "Name")]
            public string Name { get; set; }
            
            [Required]
            [Display(Name = "VolledigeNaam")]
            public string VolledigeNaam { get; set; }
            
            [Required]
            [Display(Name = "Beschrijving")]
            public string Beschrijving { get; set; }
            
            [Required]
            [Display(Name = "VisieOmschrijving")]
            public string VisieOmschrijving { get; set; }
            
            [Required]
            [Display(Name = "Info")]
            public string Info { get; set; }

        }
        
        public void OnGet(int id, string returnUrl = null)
        {
            Partij = _partijManager.GetPartijById(id);
            Input = new InputModel()
            {
                Id = Partij.Id,
                Name = Partij.Naam,
                VolledigeNaam = Partij.VolledigeNaam,
                Beschrijving = Partij.Beschrijving,
                VisieOmschrijving = Partij.VisieOmschrijving,
                Info = Partij.PartijInfo.Info
            };
            
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }
            ReturnUrl = returnUrl;
        }

        public IActionResult OnPostEditPartij(int id, string returnUrl = null)
        {
            var opgehaaldePartij = _partijManager.GetPartijById(id);
            opgehaaldePartij.Naam = Input.Name;
            opgehaaldePartij.VolledigeNaam = Input.VolledigeNaam;
            opgehaaldePartij.Beschrijving = Input.Beschrijving;
            opgehaaldePartij.VisieOmschrijving = Input.VisieOmschrijving;
            opgehaaldePartij.PartijInfo.Info = Input.Info;
            _partijManager.ChangePartij(opgehaaldePartij);
            
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }

            ReturnUrl = returnUrl;
            return LocalRedirect(ReturnUrl);
        }
        
        public IActionResult OnPostReturn(string returnUrl = null)
        {
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }

            ReturnUrl = returnUrl;
            return LocalRedirect(ReturnUrl);
        }
    }
}