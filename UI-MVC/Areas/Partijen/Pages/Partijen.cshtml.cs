﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain.Partijen;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Partijen.Pages
{
    public class Partijen : PageModel
    {
        private readonly IPartijManager _partijManager;
        public IEnumerable<Partij> PartijenList { get; set; }

        public Partijen(IPartijManager partijManager)
        {
            _partijManager = partijManager;
        }

        public void OnGet()
        {
            PartijenList = _partijManager.GetPartijen();
        }
    }
}