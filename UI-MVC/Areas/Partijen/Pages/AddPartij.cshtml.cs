﻿using System.ComponentModel.DataAnnotations;
using System.Web.Helpers;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain.Partijen;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Partijen.Pages
{
    public class AddPartij : PageModel
    {
        private readonly IPartijManager _partijManager;
        [BindProperty] public InputModel Input { get; set; }
        public string ReturnUrl { get; set; }

        public AddPartij(IPartijManager partijManager)
        {
            _partijManager = partijManager;
        }

        public class InputModel
        {
            public int Id { get; set; }

            [Required]
            [Display(Name = "Name")]
            public string Name { get; set; }
            
            [Required]
            [Display(Name = "VolledigeNaam")]
            public string VolledigeNaam { get; set; }
            
            [Required]
            [Display(Name = "Beschrijving")]
            public string Beschrijving { get; set; }
            
            [Required]
            [Display(Name = "VisieOmschrijving")]
            public string VisieOmschrijving { get; set; }
            
            [Required]
            [Display(Name = "Info")]
            public string Info { get; set; }
        }
        
        public void OnGet(string returnUrl = null)
        {
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }
            ReturnUrl = returnUrl;
        }

        public IActionResult OnPostAddPartij(string returnUrl = null)
        {
            var partij = new Partij()
            {
                Naam = Input.Name,
                VolledigeNaam = Input.VolledigeNaam,
                Beschrijving = Input.Beschrijving,
                VisieOmschrijving = Input.VisieOmschrijving,
                Logo = Input.Name+".png",
                PartijInfo = new PartijInfo()
                {
                    Info = Input.Info
                }
            };
            _partijManager.AddPartij(partij);
            
            
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }

            ReturnUrl = returnUrl;
            return LocalRedirect(ReturnUrl);
        }
        
        public IActionResult OnPostReturn(string returnUrl = null)
        {
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }

            ReturnUrl = returnUrl;
            return LocalRedirect(ReturnUrl);
        }
    }
}