﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using ST.BL.Domain.Identity;
using ST.BL.Domain.Scholen;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Scholen.Pages
{
    public class Scholen : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IGebruikerManager _gebruikerManager;
        private readonly ISchoolManager _schoolManager;

        [BindProperty] public InputModel Input { get; set; }
        public string ReturnUrl { get; set; }


        public class InputModel
        {
            public List<DetailsSchool> DetailsSchool { get; set; }
        }

        public class DetailsSchool
        {
            public School School { get; set; }
            public int AantalGebruikers { get; set; }
            public int AantalTesten { get; set; }
            public bool IsChecked { get; set; }
        }


        public Scholen(
            UserManager<ApplicationUser> userManager,
            IGebruikerManager gebruikerManager, ISchoolManager schoolManager
        )
        {
            _userManager = userManager;
            _gebruikerManager = gebruikerManager;
            _schoolManager = schoolManager;
        }


        public async Task OnGet(string returnUrl = null)
        {
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }
            ReturnUrl = returnUrl;
            Input = new InputModel();
            Input.DetailsSchool = new List<DetailsSchool>();

            var scholen = _schoolManager.GetScholen();
            var alleGebruikers = _gebruikerManager.GetGebruikers().ToList();
            var usersInRole = await _userManager.GetUsersInRoleAsync("RegisteredUser");
            foreach (var school1 in scholen)
            {
                DetailsSchool detailsSchool = new DetailsSchool();
                // je zet alle identity leerkrachten om naar gebruiker leerkrachten
                var gebruikers = new List<ST.BL.Domain.Users.Gebruiker>();
                foreach (var geb in usersInRole.ToList())
                {
                    var a = alleGebruikers.Find(g => g.Username.ToLower().Equals(geb.UserName.ToLower()));
                    gebruikers.Add(a);
                }

                // je zoekt hoeveel van die gebruiker leerkrachten in de school aanwezig zijn
                var chosen = new List<ST.BL.Domain.Users.Gebruiker>();
                foreach (var geb in gebruikers)
                {
                    if (school1.Gebruikers.Contains(geb))
                    {
                        chosen.Add(geb);
                    }
                }

                detailsSchool.School = school1;
                detailsSchool.AantalGebruikers = chosen.Count;
                Input.DetailsSchool.Add(detailsSchool);
            }
        }

        public IActionResult OnPostDeleteSchool(int[] cb)
        {
        
            var ids = cb;
            var verwijderenScholen = _schoolManager.GetScholen().Where(s => ids.Contains(s.Id)).ToList();
            foreach (var school in verwijderenScholen)
            {
                var opgehaaldeSchool = _schoolManager.GetSchoolById(school.Id);
                _schoolManager.RemoveSchool(opgehaaldeSchool);
            }

            return LocalRedirect("/Scholen/Scholen");
        }
    }
}