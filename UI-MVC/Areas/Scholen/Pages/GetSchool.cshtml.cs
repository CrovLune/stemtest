using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain.Identity;
using ST.BL.Domain.Scholen;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Scholen.Pages
{
    public class GetSchool : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IGebruikerManager _gebruikerManager;
        private readonly ISchoolManager _schoolManager;
        private readonly IKlasManager _klasManager;

        [BindProperty] public InputModel Input { get; set; }
        public string ReturnUrl { get; set; }


        public class InputModel
        {
            public School School { get; set; }
            public int AantalKlassen { get; set; }
            public int AantalGebruikers { get; set; }
        }

        public GetSchool(
            UserManager<ApplicationUser> userManager,
            IGebruikerManager gebruikerManager, 
            ISchoolManager schoolManager, 
            IKlasManager klasManager)
        {
            _userManager = userManager;
            _gebruikerManager = gebruikerManager;
            _schoolManager = schoolManager;
            _klasManager = klasManager;
        }

        public async Task OnGet(string returnUrl = null)
        {
            Input = new InputModel();
            var user = await _userManager.GetUserAsync(User);
            var gebruiker = _gebruikerManager.GetGebruikerByIdentity(user);
            var school = _schoolManager.GetScholen().Single(s => s.Id == gebruiker.School.Id);
            Input.School = school;
            Input.AantalGebruikers = school.Gebruikers.Count;
            var klassen = _klasManager.GetKlasses().Where(k => k.School.Id.Equals(school.Id)).ToList();
            Input.AantalKlassen = klassen.Count;

            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }

            ReturnUrl = returnUrl;
        }
    }
}