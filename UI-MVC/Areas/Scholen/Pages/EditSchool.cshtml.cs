﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Scholen.Pages
{
    public class EditSchool : PageModel
    {
        private readonly ISchoolManager _schoolManager;
        private readonly IGebruikerManager _gebruikerManager;
        [BindProperty] public InputModel Input { get; set; }
        public string ReturnUrl { get; set; }


        public EditSchool(ISchoolManager schoolManager, IGebruikerManager gebruikerManager)
        {
            _schoolManager = schoolManager;
            _gebruikerManager = gebruikerManager;
        }

        public class InputModel
        {
            public int Id { get; set; }

            [Required]
            [Display(Name = "SchoolNaam")]
            public string Name { get; set; }

            [Required]
            [Display(Name = "Mail Contactpersoon")]
            public string Mailcontactpersoon { get; set; }

            [Required]
            [Display(Name = "Gemeente")]
            public int Postcode { get; set; }

            [Required]
            [Display(Name = "Straatnaam")]
            public string Straatnaam { get; set; }

            [Required]
            [Display(Name = "Huisnummer")]
            public int Huisnr { get; set; }

            [Required] [Display(Name = "Land")] public string Land { get; set; }
        }

        public void OnGet(int id, string returnUrl = null)
        {
            var gekozenSchool = _schoolManager.GetSchoolById(id);
            Input = new InputModel
            {
                Id = id,
                Name = gekozenSchool.Naam,
                Mailcontactpersoon = gekozenSchool.MailContactpersoon,
                Postcode = gekozenSchool.Adres.Postcode,
                Straatnaam = gekozenSchool.Adres.StraatNaam,
                Huisnr = gekozenSchool.Adres.HuisNr,
                Land = gekozenSchool.Adres.Land
            };
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }

            ReturnUrl = returnUrl;
        }

        public IActionResult OnPostEditSchool(int id, string returnUrl = null)
        {
            var opgehaaldeSchool = _schoolManager.GetSchoolById(id);
            opgehaaldeSchool.Naam = Input.Name;
            opgehaaldeSchool.MailContactpersoon = Input.Mailcontactpersoon;
            opgehaaldeSchool.Adres.StraatNaam = Input.Straatnaam;
            opgehaaldeSchool.Adres.Postcode = Input.Postcode;
            opgehaaldeSchool.Adres.HuisNr = Input.Huisnr;
            opgehaaldeSchool.Adres.Land = Input.Land;
            _schoolManager.ChangeSchool(opgehaaldeSchool);

            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }

            ReturnUrl = returnUrl;
            return LocalRedirect(ReturnUrl);
        }
    }
}