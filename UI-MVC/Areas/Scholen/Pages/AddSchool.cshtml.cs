using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain.Scholen;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Scholen.Pages
{
    public class AddSchool : PageModel
    {
        private readonly ISchoolManager _schoolManager;

        [BindProperty] public InputModel Input { get; set; }
        public string ReturnUrl { get; set; }


        public class InputModel
        {
            [Required]
            [Display(Name = "Schoolnaam")]
            public string Name { get; set; }

            [Required] [Display(Name = "Straat")] public string Straatnaam { get; set; }

            [Required]
            [Display(Name = "Huisnummer")]
            public int Huisnr { get; set; }

            [Required]
            [Display(Name = "Postcode")]
            public int Postcode { get; set; }

            [Required] [Display(Name = "Land")] public string Land { get; set; }
        }

        public AddSchool(ISchoolManager schoolManager)
        {
            _schoolManager = schoolManager;
        }

        public void OnGet(string returnUrl = null)
        {
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }

            ReturnUrl = returnUrl;
        }

        public IActionResult OnPostAddSchool(string returnUrl = null)
        {
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }

            ReturnUrl = returnUrl;
            var school = new School()
            {
                Naam = Input.Name,
                Adres = new Adres()
                {
                    StraatNaam = Input.Straatnaam, HuisNr = Input.Huisnr,
                    Postcode = Input.Postcode, Land = Input.Land
                }
            };
            _schoolManager.AddSchool(school);
            return LocalRedirect(ReturnUrl);
        }

        public IActionResult OnPostReturn(string returnUrl = null)
        {
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }

            ReturnUrl = returnUrl;
            return LocalRedirect(ReturnUrl);
        }
    }
}