﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using ST.BL.Domain.Identity;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Domain.Scholen;
using ST.BL.Domain.Users;
using ST.BL.Interfaces;


namespace UI_MVC.Areas.Klassen.Pages
{
    public class AddKlass : PageModel
    {
        private readonly IKlasManager _klasManager;
        private readonly IGebruikerManager _gebruikerManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<AddKlass> _logger;
        private readonly IGebruikerKlassenManager _gebruikerKlassenManager;

        public AddKlass(
            IKlasManager klasManager,
            IGebruikerManager gebruikerManager,
            UserManager<ApplicationUser> userManager,
            ILogger<AddKlass> logger,
            IGebruikerKlassenManager gebruikerKlassenManager
        )
        {
            _klasManager = klasManager;
            _gebruikerManager = gebruikerManager;
            _userManager = userManager;
            _logger = logger;
            _gebruikerKlassenManager = gebruikerKlassenManager;
        }

        [BindProperty] public AddKlassModel Input { get; set; }

        public class AddKlassModel
        {
            [Required] [Display(Name = "Name")] public string Name { get; set; }

            [Required]
            [Display(Name = "Amount of students")]
            public int StudentsCount { get; set; }

            [Required]
            [Display(Name = "School Year")]
            public int SchoolYear { get; set; }
        }

        public string StatusMessage { get; set; }

        [BindProperty] public Gebruiker Gebruiker { get; set; }
        public string ReturnUrl { get; set; }

        public async Task OnGet(string returnUrl = null) 
        {
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }

            ReturnUrl = returnUrl;

            var user = await _userManager.GetUserAsync(User);
            Gebruiker = new Gebruiker();
            Gebruiker = _gebruikerManager.GetGebruikerByIdentity(user);
        }

        public async Task<IActionResult> OnPostAddKlassAsync(string returnUrl)
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var gebruiker = _gebruikerManager.GetGebruikerByIdentity(user);

            var klas = ClassChecker(gebruiker);

            if (klas == null) return Page();

            var gebKlas = new GebruikerKlassen
                {klas = klas, Gebruiker = gebruiker, KlasId = klas.Id, GebruikerId = gebruiker.Id};
            _gebruikerKlassenManager.AddGebruikerKlassen(gebKlas);

            _logger.LogInformation($"Klas {klas.Naam} added.");

            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }

            ReturnUrl = returnUrl;

            return LocalRedirect(ReturnUrl);
        }

        private Klas ClassGenerator(Gebruiker gebruiker)
        {
            return new Klas
            {
                AantalLeerlingen = Input.StudentsCount,
                Naam = Input.Name,
                School = gebruiker.School,
                StudieJaar = Input.SchoolYear
            };
        }

        private Klas ClassChecker(Gebruiker gebruiker)
        {
            var existingClasses = _gebruikerKlassenManager
                .GetGebruikerKlassen()
                .Where(g => g.GebruikerId == gebruiker.Id)
                .ToList();

            var klas = ClassGenerator(gebruiker);
            var classExists = existingClasses.Any(k => k.klas.Equals(klas));

            if (!classExists) return klas;
            StatusMessage = $"You already have added this class";
            return null;
        }
    }
}