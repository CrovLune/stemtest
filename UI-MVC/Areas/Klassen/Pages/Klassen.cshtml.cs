﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain.Identity;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Klassen.Pages
{
    public class Klassen : PageModel
    {
        private readonly IGebruikerKlassenManager _gebruikerKlassenManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IGebruikerManager _gebruikerManager;

        public Klassen(
            IGebruikerKlassenManager gebruikerKlassenManager,
            UserManager<ApplicationUser> userManager,
            IGebruikerManager gebruikerManager
        )
        {
            _gebruikerKlassenManager = gebruikerKlassenManager;
            _userManager = userManager;
            _gebruikerManager = gebruikerManager;
        }

        public List<GebruikerKlassen> Classes { get; set; }
        public string ReturnUrl { get; set; }
        public async Task OnGet(string returnUrl = null)
        {
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }
            ReturnUrl = returnUrl;
            
            var user = await _userManager.GetUserAsync(User);
            var gebruiker = _gebruikerManager.GetGebruikerByIdentity(user);
            Classes = new List<GebruikerKlassen>();
            
            Classes = _gebruikerKlassenManager
                .GetGebruikerKlassen()
                .Where(g => g.GebruikerId == gebruiker.Id)
                .ToList();
        }

        public async Task<IActionResult> OnPostDeleteAsync(int klasId, string returnUrl = null)
        {
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }
            ReturnUrl = returnUrl; //* /Klassen/Klassen
            
            var user = await _userManager.GetUserAsync(User);
            var gebruiker = _gebruikerManager.GetGebruikerByIdentity(user);

            var klas = _gebruikerKlassenManager
                .GetGebruikerKlassen()
                .Where(g => g.GebruikerId == gebruiker.Id)
                .Single(k => k.KlasId == klasId);

            if (klas != null) _gebruikerKlassenManager.RemoveGebruikerKlassen(klas);

            return LocalRedirect(returnUrl);
        }
    }
}