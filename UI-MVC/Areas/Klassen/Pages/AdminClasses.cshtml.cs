using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain.Identity;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Domain.Scholen;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Klassen.Pages
{
    public class AdminClasses : PageModel
    {
        private readonly IGebruikerKlassenManager _gebruikerKlassenManager;
        private readonly IGebruikerManager _gebruikerManager;
        private readonly ISchoolManager _schoolManager;
        private readonly UserManager<ApplicationUser> _userManager;
        public School School { get; set; }

        public AdminClasses(
            IGebruikerKlassenManager gebruikerKlassenManager,
            UserManager<ApplicationUser> userManager,
            IGebruikerManager gebruikerManager, 
            ISchoolManager schoolManager
        )
        {
            _gebruikerManager = gebruikerManager;
            _gebruikerKlassenManager = gebruikerKlassenManager;
            _userManager = userManager;
            _schoolManager = schoolManager;
        }

        public List<GebruikerKlassen> Classes { get; set; }
        public string ReturnUrl { get; set; }

        public async Task OnGet(string returnUrl = null)
        {
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }

            ReturnUrl = returnUrl;
            var user = await _userManager.GetUserAsync(User);
            var gebruiker = _gebruikerManager.GetGebruikerByIdentity(user);
            School = gebruiker.School;
            
            //adhv de school van de admin, alle klassen tonen die bij die bepaalde school horen
            Classes = new List<GebruikerKlassen>();
            var leerkrachten = _gebruikerManager.GetGebruikers().ToList();

            foreach (var leerkracht in leerkrachten)
            {
                foreach (var gebruikerKlas in leerkracht.GebruikerKlassen)
                {
                    if (gebruikerKlas.klas.School.Id.Equals(gebruiker.School.Id))
                    {
                        Classes.Add(gebruikerKlas);
                    }
                }
            }
        }

        public IActionResult OnPostDeleteAsync(int klasId, string returnUrl = null)
        {
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }

            ReturnUrl = returnUrl;

            var klas = _gebruikerKlassenManager
                .GetGebruikerKlassen()
                .Single(k => k.KlasId == klasId);

            if (klas != null) _gebruikerKlassenManager.RemoveGebruikerKlassen(klas);

            return LocalRedirect(ReturnUrl);
        }
    }
}