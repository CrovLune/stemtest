﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using ST.BL.Domain;
using ST.BL.Domain.Identity;
using ST.BL.Domain.Scholen;
using ST.BL.Domain.Sessies;
using ST.BL.Domain.Spellen;
using ST.BL.Domain.Users;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Klassen.Pages
{
    public class SelectedClass : PageModel
    {
        private readonly IKlasManager _klasManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IGebruikerManager _gebruikerManager;
        private readonly ISessieManager _sessieManager;

        public SelectedClass(
            IKlasManager klasManager,
            UserManager<ApplicationUser> userManager,
            IGebruikerManager gebruikerManager,
            ISessieManager sessieManager
        )
        {
            _klasManager = klasManager;
            _userManager = userManager;
            _gebruikerManager = gebruikerManager;
            _sessieManager = sessieManager;
        }

        public Klas Klas { get; set; }
        [BindProperty] public List<Sessie> Sessies { get; set; }
        public string ReturnUrl { get; set; }

        public async Task OnGet(int klasId)
        {
            var user = await _userManager.GetUserAsync(User);
            var gebruiker = _gebruikerManager.GetGebruikerByIdentity(user);
            Klas = new Klas();
            Klas = _klasManager.GetKlasById(klasId);

            Sessies = new List<Sessie>();
            Sessies = _sessieManager
                .GetSessies()
                .Where(s => s.Klas.Id == klasId)
                .Where(s => s.Creator.Id == gebruiker.Id)
                .ToList();

            foreach (var sessie in Sessies)
            {
                sessie.UniqueCode = Encoding.UTF8.GetString(WebEncoders.Base64UrlDecode(sessie.UniqueCode));
            }
        }
    }
}