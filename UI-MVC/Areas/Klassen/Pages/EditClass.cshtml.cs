using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Klassen.Pages
{
    public class EditClass : PageModel
    {
        private readonly IKlasManager _klasManager;
        private readonly ISchoolManager _schoolManager;
        [BindProperty] public InputModel Input { get; set; }
        public string ReturnUrl { get; set; }


        public EditClass(IKlasManager klasManager, ISchoolManager schoolManager)
        {
            _klasManager = klasManager;
            _schoolManager = schoolManager;
        }

        public class InputModel
        {
            public int Id { get; set; }

            [Required]
            [Display(Name = "KlasName")]
            public string Name { get; set; }

            [Required]
            [Display(Name = "Aantal Leerlingen")]
            public int AantalLeerlingen { get; set; }

            [Required]
            [Display(Name = "StudieJaar")]
            public int StudieJaar { get; set; }

            [Required] [Display(Name = "School")] public string School { get; set; }
            public int SchoolId { get; set; }
        }

        public void OnGet(int id, string returnUrl = null)
        {
            var gekozenKlas = _klasManager.GetKlasById(id);
            Input = new InputModel
            {
                Id = id,
                Name = gekozenKlas.Naam,
                AantalLeerlingen = gekozenKlas.AantalLeerlingen,
                StudieJaar = gekozenKlas.StudieJaar,
                School = gekozenKlas.School.Naam,
                SchoolId = gekozenKlas.School.Id
            };
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }

            ReturnUrl = returnUrl;
        }

        public IActionResult OnPostEditClass(int id, string returnUrl = null)
        {
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                returnUrl = $"~/{returnUrl.Substring(23)}";
            }

            ReturnUrl = returnUrl;

            var opgehaaldeKlas = _klasManager.GetKlasById(id);
            opgehaaldeKlas.Naam = Input.Name;
            opgehaaldeKlas.AantalLeerlingen = Input.AantalLeerlingen;
            opgehaaldeKlas.StudieJaar = Input.StudieJaar;
            opgehaaldeKlas.School = _schoolManager.GetScholen().Single(s => s.Naam.Equals(Input.School));
            _klasManager.ChangeKlas(opgehaaldeKlas);

            return LocalRedirect(ReturnUrl);
        }
    }
}