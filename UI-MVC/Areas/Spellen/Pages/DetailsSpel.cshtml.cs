﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain.Spellen;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Spellen.Pages
{
    public class DetailsSpel : PageModel
    {
        private readonly ISpelManager _spelManager;
        public Spel Spel { get; set; }

        public DetailsSpel(ISpelManager spelManager)
        {
            _spelManager = spelManager;
        }
        
        public void OnGet(int spelId)
        {
            Spel = _spelManager.GetSpelById(spelId);
        }
    }
}