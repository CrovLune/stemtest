﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain.Antwoorden;
using ST.BL.Domain.Identity;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Domain.Partijen;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Spellen.Pages
{
    public class ExportCsv : PageModel
    {
        private readonly IGebruikerStellingenManager _gebruikerStellingenManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IGebruikerManager _gebruikerManager;
        
        public IEnumerable<GebruikerStellingen> GebruikerStellingen { get; set; }
        [BindProperty(SupportsGet = true)] public string StellingOnderwerp { get; set; }
        public List<string> Onderwerpen { get; set; }

        public ExportCsv(IGebruikerStellingenManager stellingManager, UserManager<ApplicationUser> userManager, IGebruikerManager gebruikerManager)
        {
            _gebruikerStellingenManager = stellingManager;
            _userManager = userManager;
            _gebruikerManager = gebruikerManager;
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var user =  await _userManager.GetUserAsync(User);
            var gebruiker = _gebruikerManager.GetGebruikerByIdentity(user);
            
            //Alle stellingonderwerpen van een bepaalde gebruiker tonen
            GebruikerStellingen = _gebruikerStellingenManager.GetGebruikerStellingen()
                .Where(g => g.GebruikerId.Equals(gebruiker.Id));

            Onderwerpen = new List<string>();
            
            foreach (var gebruikerStelling in GebruikerStellingen)
            {
                Onderwerpen.Add(gebruikerStelling.Stelling.StellingOnderwerp);
            }
            
            Onderwerpen = Onderwerpen.Distinct().ToList();
            
            return Page();
        }
        
        public async Task<IActionResult> OnPostAsync ()
        {
            var address = "wwwroot/temp.csv";
            System.IO.File.Delete(address);
            
            var user =  await _userManager.GetUserAsync(User);
            var gebruiker = _gebruikerManager.GetGebruikerByIdentity(user);
            var partijen = new List<Partij>();

            var gebruikerStellingen = _gebruikerStellingenManager.GetGebruikerStellingen()
                .Where(g => g.GebruikerId.Equals(gebruiker.Id))
                .Where(g => g.Stelling.StellingOnderwerp.Equals(StellingOnderwerp));

            var sb = new StringBuilder();
            var header = new StringBuilder();
            var prefix = "";
            header.Append("Vraag;");
            header.Append("Uitleg;");
            
            foreach (var gebruikerStelling in gebruikerStellingen)
            {
                foreach (var partijAntwoordOpStelling in gebruikerStelling.Stelling.PartijAntwoordOpStelling)
                {
                    partijen.Add(partijAntwoordOpStelling.Partij);
                }
            }

            partijen = partijen.Distinct().OrderBy(p => p.Id).ToList();

            foreach (var partij in partijen)
            {
                header.Append(prefix);
                prefix = ";";
                header.Append("Antwoord" + partij.Naam);
                header.Append(prefix);
                header.Append("Uitleg" + partij.Naam);
            }

            sb.AppendLine(header.ToString());

            foreach (var gebruikerStelling in gebruikerStellingen)
            {
                var line = new StringBuilder();
                var prefix2 = "";
                line.Append(gebruikerStelling.Stelling.Vraag+";");
                line.Append(gebruikerStelling.Stelling.Uitleg+";");
                
                var partijAntwoorden = new List<PartijAntwoordOpStelling>();
                foreach (var partijAntwoordOpStelling in gebruikerStelling.Stelling.PartijAntwoordOpStelling)
                {
                    partijAntwoorden.Add(partijAntwoordOpStelling);
                }
                partijAntwoorden = partijAntwoorden.OrderBy(p => p.Partij.Id).ToList();

                foreach (var partijAntwoordOpStelling in partijAntwoorden)
                {
                    line.Append(prefix2);
                    prefix2 = ";";
                    line.Append(partijAntwoordOpStelling.Antwoord);
                    line.Append(prefix2);
                    line.Append(partijAntwoordOpStelling.Uitleg);
                }
                sb.AppendLine(line.ToString());
            }
            
            TextWriter sw = new StreamWriter(address, true);
            sw.Write(sb.ToString());
            sw.Close(); 
            
            var net = new System.Net.WebClient();
            var data = net.DownloadData(address);
            var content = new MemoryStream(data);
            var contentType = "APPLICATION/octet-stream";
            return File(content, contentType, $"{StellingOnderwerp}.csv");
        }
    }
}