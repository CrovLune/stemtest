﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LumenWorks.Framework.IO.Csv;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain.Antwoorden;
using ST.BL.Domain.Identity;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Domain.Stellingen;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Spellen.Pages
{
    public class ImportCsv : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IGebruikerManager _gebruikerManager;
        private readonly IPartijManager _partijManager;
        private readonly IStellingManager _stellingManager;
        private readonly IGebruikerStellingenManager _gebruikerStellingenManager;
        private readonly IAntwoordenManager _antwoordenManager;

        [BindProperty(SupportsGet = true)] public string Onderwerp { get; set; }
        [BindProperty(SupportsGet = true)] public IFormFile Filee { get; set; }
        
        public ImportCsv(
            UserManager<ApplicationUser> userManager, 
            IGebruikerManager gebruikerManager, 
            IPartijManager partijManager, 
            IStellingManager stellingManager, 
            IGebruikerStellingenManager gebruikerStellingenManager,
            IAntwoordenManager antwoordenManager
            )
        {
            _userManager = userManager;
            _gebruikerManager = gebruikerManager;
            _partijManager = partijManager;
            _stellingManager = stellingManager;
            _gebruikerStellingenManager = gebruikerStellingenManager;
            _antwoordenManager = antwoordenManager;
        }

        public void OnGet()
        {
            
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var user =  await _userManager.GetUserAsync(User);
            var gebruiker = _gebruikerManager.GetGebruikerByIdentity(user);
            
            using (var csv = new CsvReader(new StreamReader(Filee.OpenReadStream()), true, ';'))
            {
                var headers = csv.GetFieldHeaders();
                var aantalpartijen = (headers.Length-2)/2;
                
                while (csv.ReadNextRecord())
                {
                    var stelling = _stellingManager.AddStelling(new Stelling()
                    {
                        StellingOnderwerp = Onderwerp,
                        Vraag = csv[0],
                        Uitleg = csv[1]
                    });
                    
                    _gebruikerStellingenManager.AddGebruikerStelling(new GebruikerStellingen()
                    {
                        Gebruiker = _gebruikerManager.GetGebruikerById(gebruiker.Id),
                        Stelling = _stellingManager.GetStellingById(stelling.Id)
                    });
                    
                    for (int i = 0; i < aantalpartijen; i++)
                    {
                        var partijNaam = headers[3+(2*i)];
                        partijNaam = partijNaam.Replace("Uitleg", "");
                        var partij = _partijManager.GetPartijen().FirstOrDefault(p => p.Naam.Equals(partijNaam));
                        if (partij != null)
                        {
                            var pa = new PartijAntwoordOpStelling()
                            {
                                Antwoord = csv[2+(2*i)],
                                Uitleg = csv[3+(2*i)],
                                Partij = _partijManager.GetPartijById(partij.Id),
                                Stelling = _stellingManager.GetStellingById(stelling.Id)
                            };
                            _antwoordenManager.AddAntwoord(pa);
                        }
                    }
                }
            }
            return RedirectToPage();
        }
    }
}