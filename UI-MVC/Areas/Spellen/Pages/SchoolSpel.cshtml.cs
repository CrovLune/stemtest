using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain.Spellen;
using ST.BL.Domain.Stellingen;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Spellen.Pages
{
    public class SchoolSpel : PageModel
    {
        private readonly ISchoolManager _schoolManager;
        private readonly ISpelManager _spelManager;
        private readonly IStellingManager _stellingManager;


        public SchoolSpel(
            ISpelManager spelManager, ISchoolManager schoolManager, IStellingManager stellingManager
        )
        {
            _stellingManager = stellingManager;
            _spelManager = spelManager;
            _schoolManager = schoolManager;
        }

        public class InputModel
        {
            public List<DetailsSpel> DetailsSpel { get; set; }
        }

        public class DetailsSpel
        {
            public Spel Spel { get; set; }
            public string School { get; set; }
            public string Eigenaar { get; set; }
            public int AantalStellingen { get; set; }
            public bool IsChecked { get; set; }
        }

        [BindProperty] public InputModel Input { get; set; }
        public string ReturnUrl { get; set; }

        public void OnGet(int schoolId, string returnUrl = null)
        {
            Input = new InputModel();
            Input.DetailsSpel = new List<DetailsSpel>();

            var spellen = _spelManager.GetSpellen().Where(s => s.Creator.School.Id == schoolId).ToList();
            var alleStellingen = _stellingManager.GetStellingen().ToList();
            foreach (var spel in spellen)
            {
                DetailsSpel detailsSpel = new DetailsSpel();
                var stellingen = new List<Stelling>();
                foreach (var stel in alleStellingen)
                {
                    if (spel.Stellingen.Contains(stel))
                    {
                        stellingen.Add(stel);
                    }
                }

                detailsSpel.Spel = spel;
                detailsSpel.AantalStellingen = stellingen.Count;
                detailsSpel.Eigenaar = spel.Creator.Username;
                detailsSpel.School = spel.Creator.School.Naam;
                Input.DetailsSpel.Add(detailsSpel);
            }
        }
        public IActionResult OnPostDeleteSpel(int[] cb)
        {
            var ids = cb;
            var verwijderenSpellen = _spelManager.GetSpellen().Where(s => ids.Contains(s.SpelId)).ToList();
            foreach (var spel in verwijderenSpellen)
            {
                var opgehaaldSpel = _spelManager.GetSpelById(spel.SpelId);
                _spelManager.RemoveSpel(opgehaaldSpel);
            }

            return LocalRedirect("/Spellen/GetSpel");
        }
    }
}