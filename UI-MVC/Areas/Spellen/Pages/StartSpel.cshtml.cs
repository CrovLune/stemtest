﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Domain.Sessies;
using ST.BL.Domain.Spellen;
using ST.BL.Domain.Stellingen;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Spellen.Pages
{
    [AllowAnonymous]
    public class StartSpel : PageModel
    {
        private readonly ISpelStellingenManager _spelStellingenManager;
        private readonly ISessieManager _sessieManager;
        private readonly ISpelManager _spelManager;

        [BindProperty] public List<SpelStellingen> SpelStellingen { get; set; } = new List<SpelStellingen>();
        [BindProperty] public int SessieId { get; set; }
        [BindProperty] public string Cookie { get; set; }
        [BindProperty] public Spel Spel { get; set; } = new Spel();

        public string Location { get; set; }
        public string finishGameLocation { get; set; }

        public StartSpel(
            ISpelStellingenManager spelStellingenManager,
            ISessieManager sessieManager,
            ISpelManager spelManager
        )
        {
            _spelStellingenManager = spelStellingenManager;
            _sessieManager = sessieManager;
            _spelManager = spelManager;
        }

        public void OnGet(int sessieId)
        {
            var sessie = _sessieManager.GetSessieById(sessieId);
            
            SpelStellingen = _spelStellingenManager
                .GetSpelStellingen()
                .Where(x => x.SpelId == sessie.Spel.SpelId)
                .ToList();
            SessieId = sessieId;

            var getCookie = HttpContext.Request.Cookies["AnonymousCookie"];
            Cookie = getCookie;

            Spel = _sessieManager.GetSessieById(sessieId).Spel;
            
            
            finishGameLocation = Url.Page(
                "/ResultatenschermDebatspel",
                pageHandler: null,
                values: new
                {
                    area = "Resultaten",
                    sessieId
                },
                protocol: Request.Scheme);
            
            Location = Url.Page(
                "/StartSpel",
                pageHandler: null,
                values: new
                {
                    area = "Spellen",
                    sessieId
                },
                protocol: Request.Scheme);
        }
    }
}