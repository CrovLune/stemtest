﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ST.BL.Domain;
using ST.BL.Domain.Antwoorden;
using ST.BL.Domain.Identity;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Domain.Spellen;
using ST.BL.Domain.Stellingen;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Spellen.Pages
{
    public class AddSpel : PageModel
    {
        private readonly IGebruikerStellingenManager _gebruikerStellingenManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IGebruikerManager _gebruikerManager;
        private readonly IStellingManager _stellingManager;
        private readonly ISpelManager _spelManager;
        private readonly IKlasSpellenManager _klasSpellenManager;
        private readonly ISpelStellingenManager _spelStellingenManager;

        public AddSpel(
            IGebruikerStellingenManager gebruikerStellingenManager,
            UserManager<ApplicationUser> userManager,
            IGebruikerManager gebruikerManager,
            IStellingManager stellingManager,
            ISpelManager spelManager,
            IKlasSpellenManager klasSpellenManager,
            ISpelStellingenManager spelStellingenManager
        )
        {
            _gebruikerStellingenManager = gebruikerStellingenManager;
            _userManager = userManager;
            _gebruikerManager = gebruikerManager;
            _stellingManager = stellingManager;
            _spelManager = spelManager;
            _klasSpellenManager = klasSpellenManager;
            _spelStellingenManager = spelStellingenManager;
        }

        [BindProperty] public int KlasId { get; set; }
        [BindProperty] public int SpelId { get; set; }
        [BindProperty] public AddSpelModel Input { get; set; }
        [BindProperty] public SettingsModel SettingsInput { get; set; }
        [BindProperty] public Spel Spel { get; set; }

        [BindProperty] public bool IsPartijSpel { get; set; }

        // public List<List<GebruikerStellingen>> SortedByThemaStellingen { get; set; }
        // [BindProperty] public bool Result { get; set; }

        public class SettingsModel
        {
            public string Name { get; set; }

            [Display(Name = "Color The Screen Post Answer")]
            public bool ColorScreenPostAnswer { get; set; }

            [Display(Name = "Allow Argument Input")]
            public bool AllowArgumentInput { get; set; }

            [Display(Name = "Explain Difficult Words")]
            public bool ExplainDifficultWord { get; set; }

            [Display(Name = "Skip Questions")] public bool Skip { get; set; }
            public Status Status { get; set; }
        }


        public class AddSpelModel
        {
            public AddSpelModel()
            {
                GebruikerStellingen = new List<GebruikerStellingen>();
            }

            public List<GebruikerStellingen> GebruikerStellingen { get; set; }
            public List<int> areChecked { get; set; }
        }

        public string ReturnUrl { get; set; }

        public async Task OnGet(int klasId, int spelId = 0, string returnUrl = null)
        {
            if (returnUrl == null)
            {
                returnUrl = Url.Content(HttpContext.Request.GetEncodedUrl());
                var index = returnUrl.Split('1');
                returnUrl = $"~/{index[1].Substring(1)}";
            }

            ReturnUrl = returnUrl;

            KlasId = klasId;
            SpelId = spelId;

            var user = await _userManager.GetUserAsync(User);
            var gebruiker = _gebruikerManager.GetGebruikerByIdentity(user);

            Input = new AddSpelModel
            {
                GebruikerStellingen = _gebruikerStellingenManager
                    .GetGebruikerStellingen()
                    .Where(g => g.GebruikerId == gebruiker.Id)
                    .ToList()
            };
            
            
            
        }

        // public int SessieId { get; set; }

        public IActionResult OnPostSelectStelling(int spelId, string returnUrl = null)
        {
            ReturnUrl = returnUrl;
        
            var x = _spelStellingenManager.GetSpelStellingen().Where(s => s.SpelId == spelId).ToList();
            foreach (var spelStellingen in x)
            {
                spelStellingen.Selected = false;
            }
        
            foreach (var stelling in Input.areChecked.Select(i => _stellingManager.GetStellingById(i)))
            {
                var found = _spelStellingenManager.GetSpelStellingen().Where(s => s.StellingId == stelling.Id)
                    .SingleOrDefault(s => s.SpelId == spelId);
                if (found == null)
                {
                    _spelStellingenManager.AddSpelStelling(new SpelStellingen
                        {SpelId = spelId, StellingId = stelling.Id, Selected = true});
                }
                else
                {
                    found.Selected = true;
                    _spelStellingenManager.ChangeSpelStelling(found);
                }
            }
            return LocalRedirect(returnUrl);
        }

        public IActionResult OnPostCreateStelling(int klasId, int spelId, string returnUrl = null)
        {
            var stelling = new Stelling
            {
                MoeilijkeWoorden = new List<MoeilijkWoord>(),
                PartijAntwoordOpStelling = new List<PartijAntwoordOpStelling>(),
                Status = Status.Pending,
                StellingOnderwerp = "",
                Uitleg = "",
                Vraag = "",
                UserAntwoordOpStelling = new List<UserAntwoordOpStelling>()
            };
            var result = _stellingManager.AddStelling(stelling);

            return RedirectToPage("/AddStelling",
                new {area = "Stellingen", klasId, spelId, stellingId = result.Id, returnUrl});
        }

        public async Task<IActionResult> OnPostCreateSpel(int klasId, string returnUrl = null)
        {
            var user = await _userManager.GetUserAsync(User);
            var gebruiker = _gebruikerManager.GetGebruikerByIdentity(user);

            var options = new Options()
            {
                ColorScreenPostAnswer = SettingsInput.ColorScreenPostAnswer,
                AllowArgumentInput = SettingsInput.AllowArgumentInput,
                ExplainDifficultWord = SettingsInput.ExplainDifficultWord,
                Skip = SettingsInput.Skip,
            };

            if (IsPartijSpel)
            {
                Spel = new PartijSpel()
                {
                    Name = SettingsInput.Name,
                    Status = SettingsInput.Status,
                    Creator = gebruiker,
                    options = options
                };
                Spel = (PartijSpel) _spelManager.AddSpel(Spel);
            }
            else
            {
                Spel = new DebatSpel()
                {
                    Name = SettingsInput.Name,
                    Status = SettingsInput.Status,
                    Creator = gebruiker,
                    options = options
                };
                Spel = (DebatSpel) _spelManager.AddSpel(Spel);
            }

            var ks = new KlasSpellen {KlasId = klasId, SpelId = Spel.SpelId};
            _klasSpellenManager.AddKlasSpel(ks);

            foreach (var stelling in Input.areChecked.Select(i => _stellingManager.GetStellingById(i)))
            {
                _spelStellingenManager.AddSpelStelling(new SpelStellingen
                {
                    SpelId = Spel.SpelId,
                    StellingId = stelling.Id,
                    Selected = true
                });
            }

            return RedirectToPage("/AddSessie", new {area = "Sessies", klasId = klasId});
        }
    }
}