using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using ST.BL.Domain.Identity;
using ST.BL.Domain.Scholen;
using ST.BL.Domain.Spellen;
using ST.BL.Domain.Stellingen;
using ST.BL.Interfaces;

namespace UI_MVC.Areas.Spellen.Pages
{
    public class Spellen : PageModel
    {
        private readonly ISpelManager _spelManager;
        private readonly IStellingManager _stellingManager;

        [BindProperty] public InputModel Input { get; set; }


        public class InputModel
        {
            public List<DetailsSpel> DetailsSpel { get; set; }
        }

        public class DetailsSpel
        {
            public Spel Spel { get; set; }
            public string School { get; set; }
            public string Eigenaar { get; set; }
            public int AantalStellingen { get; set; }
            public bool IsChecked { get; set; }
        }


        public Spellen(
            ISpelManager spelManager,
            IStellingManager stellingManager
        )
        {
            _spelManager = spelManager;
            _stellingManager = stellingManager;
        }


        public void OnGet()
        {
            Input = new InputModel();
            Input.DetailsSpel = new List<DetailsSpel>();

            var spellen = _spelManager.GetSpellen().ToList();
            var alleStellingen = _stellingManager.GetStellingen().ToList();
            foreach (var spel in spellen)
            {
                DetailsSpel detailsSpel = new DetailsSpel();
                var stellingen = new List<Stelling>();
                foreach (var stel in alleStellingen)
                {
                    if (spel.Stellingen.Contains(stel))
                    {
                        stellingen.Add(stel);
                    }
                }

                detailsSpel.Spel = spel;
                detailsSpel.AantalStellingen = stellingen.Count;
                detailsSpel.Eigenaar = spel.Creator.Username;
                detailsSpel.School = spel.Creator.School.Naam;
                Input.DetailsSpel.Add(detailsSpel);
            }
        }
    }
}