using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;
using ST.BL.Domain;
using ST.BL.Domain.Antwoorden;
using ST.BL.Domain.Sessies;
using ST.BL.Domain.Users;
using ST.BL.Interfaces;

namespace UI_MVC.Hubs
{
    public class SpelHub : Hub
    {
        private readonly ISessieManager _sessieManager;
        private readonly ISpelStellingenManager _spelStellingenManager;
        private readonly IAnoniemeGebruikerManager _anoniemeGebruikerManager;
        private readonly IStellingManager _stellingManager;
        private readonly IAntwoordenManager _antwoordenManager;
        
        private static Dictionary<AnoniemeGebruiker, string> users = new Dictionary<AnoniemeGebruiker, string>();
        private static List<SpelSessie> _sessies = new List<SpelSessie>();

        public SpelHub(
            ISessieManager sessieManager,
            ISpelStellingenManager spelStellingenManager,
            IAnoniemeGebruikerManager anoniemeGebruikerManager,
            IStellingManager stellingManager,
            IAntwoordenManager antwoordenManager
        )
        {
            _sessieManager = sessieManager;
            _spelStellingenManager = spelStellingenManager;
            _anoniemeGebruikerManager = anoniemeGebruikerManager;
            _stellingManager = stellingManager;
            _antwoordenManager = antwoordenManager;
        }

        public class SpelSessie
        {
            public int SessieId { get; set; }
            //Creator.name
            public string Leerkracht { get; set; }
            public Dictionary<int, string> Leerlingen { get; set; }
        }

        public Sessie FindSessionByCode(string code)
        {
            Console.WriteLine(Convert.ToBase64String(Encoding.UTF8.GetBytes(code)));
           
            var foundSession = _sessieManager.GetSessies().SingleOrDefault(x =>
                x.UniqueCode == Convert.ToBase64String(Encoding.UTF8.GetBytes(code)));
            if (foundSession == null || foundSession.Status == Status.Closed) return null;
            
            Console.WriteLine($"{foundSession.Creator.Id} {foundSession.Id} {foundSession.Klas.Id} {foundSession.Name} {foundSession.Spel.Name} {foundSession.Status} {foundSession.UniqueCode}");

            //* fout afhandeling
            return foundSession;
        }

        public  void OpenNextStelling(string code)
        {
            var foundSession = FindSessionByCode(code);
            

            //Get The first locked stelling

            var spelStelling = _spelStellingenManager.GetSpelStellingen()
                .SingleOrDefault(x => x.SpelId == foundSession.Spel.SpelId && x.Status == Status.Closed);
            var stelling = spelStelling.Stelling;

            //Open the stelling
            spelStelling.Status = Status.Open;

            _spelStellingenManager.ChangeSpelStelling(spelStelling);

              }

        public async Task Authenticate(string code)
        {
            var foundSession = FindSessionByCode(code);

            if (foundSession != null && foundSession.Status != Status.Closed)
            {
                var anon = new AnoniemeGebruiker
                {
                    Sessie = foundSession
                };
                anon = _anoniemeGebruikerManager.AddAnoniemeGebruiker(anon);
                users.Add(anon, Context.ConnectionId);
                await AddToGroup(foundSession.Id);
                await Clients.Caller.SendAsync("Redirect", foundSession.Id);

                var sessie = _sessies.Find(x => x.SessieId.Equals(foundSession.Id));

                sessie.Leerlingen.Add(anon.Id, Context.ConnectionId);
                Console.WriteLine("gebruikerId: " + anon.Id);
                Console.WriteLine(JsonConvert.SerializeObject(sessie, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    }));
            }
            else
            {
                await Clients.Caller.SendAsync("Send", "foutieve code");
            }
        }

        public async Task PushUpdateDeelnemers(int sessieId)
        {
            var usersList = users.Where(x => x.Key.Sessie.Id == sessieId).ToList();
            var aantal = usersList.Count;
            foreach (var (key, value) in usersList)
            {
                Console.WriteLine("sessieId: " + key.Sessie.Id + " gebruikerId: " + key.Id);
                Console.WriteLine("connectionId: " + value);
            }

            Console.WriteLine("trigger updateAantal " + aantal);

            foreach (var (anoniemeGebruiker, connectionId) in usersList)
            {
                await Clients.Client(connectionId).SendAsync("updateAantal", aantal);
            }
        }

        public async Task AddToGroup(int sessieId)
        {
            Console.WriteLine($"adding to group {sessieId} {Context.ConnectionId}");

            await PushUpdateDeelnemers(sessieId);
        }

        public async Task RemoveFromGroup(int sessieId)
        {
            Console.WriteLine("removing from group " + Context.ConnectionId);
            var sessie = _sessies.Find(x => x.SessieId.Equals(sessieId));
            var leerling = sessie.Leerlingen.SingleOrDefault(x => x.Value.Equals(Context.ConnectionId)).Key;
            Console.WriteLine("removing: " + leerling);
            sessie.Leerlingen.Remove(leerling);
              await PushUpdateDeelnemers(sessieId);
        }

        //--------------------------Leerkracht---------------------------

        public async Task LeerkrachtSelecteertEersteStelling(int stellingId, int sessieId)
        {
            Console.WriteLine("LeerkrachtSelecteertEersteStelling: " + stellingId + " " + sessieId);
            var sessie = _sessieManager.GetSessieById(sessieId);
            var spelStelling = _spelStellingenManager.GetSpelStellingen()
                .SingleOrDefault(x => x.StellingId == stellingId);
            var stellingJson = "";
            if (spelStelling != null)
            {
                stellingJson = JsonConvert.SerializeObject(spelStelling.Stelling, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });
            }
            //TODO: Error handling

            var clients = users.Where(x => x.Key.Sessie.Id == sessieId).ToList();
            foreach (var (client, connectionId) in clients)
            {
                await Clients.Client(connectionId).SendAsync("receiveEersteStelling", stellingJson);
            }
        }

        public async Task OnLeerkrachtStartCardClick(int sessieId)
        {
            var sessie = _sessieManager.GetSessieById(sessieId);
            await Clients.Caller.SendAsync("sessionInProgress");
        }

        public async Task OnLeerkrachtStellingCardClick(int stellingId)
        {
            var stelling = _stellingManager.GetStellingById(stellingId);
            var stellingJson = JsonConvert.SerializeObject(stelling, Formatting.Indented,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
            await Clients.Caller.SendAsync("stellingSelected", stellingJson);
        }

        public async Task OnLeerkrachtResultatenClick(int sessieId)
        {
            Console.WriteLine("invoking resultaten sessie: " + sessieId);
            var sessie = _sessies.SingleOrDefault(x => x.SessieId == sessieId);
            var sessieLeerlingen = new Dictionary<int, string>();
            if (sessie != null)
            {
                sessieLeerlingen = sessie.Leerlingen;
            }

            foreach (var (anonId, connectionid) in sessieLeerlingen)
            {
                Console.WriteLine("invoking leerlingEindResultaat on: " + connectionid);
                await Clients.Client(connectionid).SendAsync("leerlingEindResultaat");
            }
        }

        //--------------------------Leerling---------------------------

        public async Task OnLeerlingVolgendeClick(int stellingId, string antwoord)
        {
            var stelling = _stellingManager.GetStellingById(stellingId);
            var gebruikerId = users.SingleOrDefault(x => x.Value == Context.ConnectionId).Key.Id;
            var gebruiker = _anoniemeGebruikerManager.GetAnoniemeGebruikerById(gebruikerId);
            var uaos = new UserAntwoordOpStelling
            {
                Antwoord = antwoord,
                AnoniemeGebruiker = gebruiker,
                Stelling = stelling
            };
            Console.WriteLine("gebruiker: " + uaos.AnoniemeGebruiker.Id + " submitting answer: " + uaos.Antwoord +
                              " on stelling: " + uaos.Stelling.Id);
            
            var sessieId = gebruiker.Sessie.Id;
            var partijantwoorden = _sessieManager.GetSessieById(sessieId)
                .Spel
                .SpelStellingen
                .Single(x => x.StellingId == stellingId)
                .Stelling
                .PartijAntwoordOpStelling;

            foreach (var partijAntwoordOpStelling in partijantwoorden)
            {
                Console.WriteLine($"stelling: {partijAntwoordOpStelling.Stelling.Id} {partijAntwoordOpStelling.Stelling.Vraag} - partijantwoord: {partijAntwoordOpStelling.Antwoord} {partijAntwoordOpStelling.Partij.Naam}");
            }
            
            _sessieManager
                .GetSessieById(sessieId)
                .Spel
                .SpelStellingen
                .Single(x => x.StellingId == stellingId)
                .Stelling
                .UserAntwoordOpStelling
                .Add(uaos);
            
            Console.WriteLine(_sessieManager.GetSessieById(sessieId).Spel.SpelStellingen.Single(x => x.StellingId == stellingId).Stelling.Vraag);

            await UpdateLeerkrachtResultaten(sessieId, uaos.Id, stellingId);
            await Clients.Caller.SendAsync("leerlingWacht");
        }

        public async Task UpdateLeerkrachtResultaten(int sessieId, int uaosId, int stellingId)
        {
            var sessie = _sessies.SingleOrDefault(spelSessie => spelSessie.SessieId == sessieId);
            var uaos = _sessieManager
                .GetSessieById(sessieId)
                .Spel
                .SpelStellingen
                .Single(x => x.StellingId == stellingId)
                .Stelling.UserAntwoordOpStelling
                .Single(x => x.Id == uaosId);

            if (sessie != null)
            {
                await Clients.Client(sessie.Leerkracht).SendAsync("nieuwAntwoord", uaos.Antwoord);
            }
        }

        public  void LeerlingGetResultaat(int sessieId)
        {
            var sessie = _sessies.SingleOrDefault(spelSessie => spelSessie.SessieId.Equals(sessieId));

            var gebruikerId = 0;
            if (sessie != null)
            {
                gebruikerId = sessie.Leerlingen.SingleOrDefault(x => x.Value.Equals(Context.ConnectionId)).Key;
            }
            
            var antwoorden = _sessieManager.GetSessieById(sessieId);
           
        }
        //-------------------------------------------------------

        public async Task AuthenticateLeerkracht(int sessieId)
        {
            try
            {
                var sessie = _sessieManager.GetSessieById(sessieId);
              
                    users.Add(new AnoniemeGebruiker {Sessie = sessie}, Context.ConnectionId);
                    _sessies.Add(new SpelSessie
                    {
                        SessieId = sessie.Id,
                        Leerkracht = Context.ConnectionId,
                        Leerlingen = new Dictionary<int, string>()
                    });
                    Console.WriteLine(JsonConvert.SerializeObject(_sessies.Single().SessieId));
                    await PushUpdateDeelnemers(sessieId);
                // }
            }
            catch (InvalidOperationException)
            {
                await Clients.Caller.SendAsync("Send", "foutieve code");
            }
        }

        public async Task Send(string message)
        {
            await Clients.All.SendAsync("Send", message);
        }

        
    }
}