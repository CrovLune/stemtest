﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using ST.BL.Domain;
using ST.BL.Domain.Antwoorden;
using ST.BL.Domain.Identity;
using ST.BL.Domain.Users;
using ST.BL.Interfaces;

namespace UI_MVC.Hubs
{
    public class MyHub : Hub
    {
        private readonly ISessieManager _sessieManager;
        private readonly IAnoniemeGebruikerManager _anoniemeGebruikerManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ISpelStellingenManager _spelStellingenManager;
        private readonly IAntwoordenManager _antwoordenManager;

        public MyHub(
            ISessieManager sessieManager,
            IAnoniemeGebruikerManager anoniemeGebruikerManager,
            UserManager<ApplicationUser> userManager,
            ISpelStellingenManager spelStellingenManager,
            IAntwoordenManager antwoordenManager
        )
        {
            _sessieManager = sessieManager;
            _anoniemeGebruikerManager = anoniemeGebruikerManager;
            _userManager = userManager;
            _spelStellingenManager = spelStellingenManager;
            _antwoordenManager = antwoordenManager;
        }

        public async Task Auth(int sessieId, string cookie)
        {
            var identifier = Context.UserIdentifier;
            int.TryParse(identifier, out var identityId);

            var anon = _anoniemeGebruikerManager
                .GetAnoniemeGebruikers()
                .ToList()
                .SingleOrDefault(x => x.Cookie.Equals(cookie));

            if (identityId > 0)
            {
                var user = await _userManager.GetUserAsync(Context.User);
                await _userManager.SetPhoneNumberAsync(user, sessieId.ToString());

                var list = _anoniemeGebruikerManager.GetAnoniemeGebruikers().ToList();

                var connectedUsers = list.Count(x => x.Sessie.Id == sessieId);

                //* Send separate message to Teacher
                await Clients.User(identifier).SendAsync("UpdateDeelnemers", connectedUsers);

                // //* Send separate message to Students in Session
                // var groups = (from item in list where item.Sessie.Id == sessieId select item.GroupName).ToList();
                // await Clients.Groups(groups).SendAsync("updateDeelnemers", connectedUsers);
            }
            else if (anon != null)
            {
                var teacher = _userManager.Users.Single(x => x.PhoneNumber.Equals(sessieId.ToString()));

                await AddToGroup(anon.GroupName);
                anon.Sessie = _sessieManager.GetSessieById(sessieId);

                var list = _anoniemeGebruikerManager.GetAnoniemeGebruikers().ToList();
                var connectedUsers = list.Count(x => x.Sessie.Id == sessieId);

                var groups =
                    (from anoniem in list where anoniem.Sessie.Id == sessieId select anoniem.GroupName).ToList();

                await Clients.Groups(groups).SendAsync("UpdateDeelnemers", connectedUsers);
                await Clients.User(teacher.Id.ToString()).SendAsync("UpdateDeelnemers", connectedUsers);
            }
            else
            {
                var teacher = _userManager.Users.Single(x => x.PhoneNumber.Equals(sessieId.ToString()));

                var user = new AnoniemeGebruiker
                {
                    Sessie = _sessieManager.GetSessieById(sessieId),
                    AntwoordenOpStellingen = new List<UserAntwoordOpStelling>(),
                    Cookie = cookie,
                    GroupName = RandomString(10)
                };

                _anoniemeGebruikerManager.AddAnoniemeGebruiker(user);
                await AddToGroup(user.GroupName);

                var list = _anoniemeGebruikerManager.GetAnoniemeGebruikers().ToList();
                var connectedUsers = list.Count(x => x.Sessie.Id == sessieId);

                var groups =
                    (from anoniem in list where anoniem.Sessie.Id == sessieId select anoniem.GroupName).ToList();

                await Clients.Groups(groups).SendAsync("UpdateDeelnemers", connectedUsers);
                await Clients.User(teacher.Id.ToString()).SendAsync("UpdateDeelnemers", connectedUsers);
            }

            var clear = _anoniemeGebruikerManager.GetAnoniemeGebruikers().Where(x => x.Cookie.Equals("")).ToList();
            _anoniemeGebruikerManager.RemoveAnoniemeGebruikesRange(clear);
        }

        public async Task AddToGroup(string groupName)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, groupName);
        }

        public async Task RemoveFromGroup(string groupName)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, groupName);
        }

        private static string RandomString(int length)
        {
            var random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public async Task StartSpel(int sessieId, string location)
        {
            var teacher = _userManager.Users.Single(x => x.PhoneNumber.Equals(sessieId.ToString()));

            var list = _anoniemeGebruikerManager.GetAnoniemeGebruikers().ToList();
            var connectedUsers = list.Count(x => x.Sessie.Id == sessieId);

            var groups =
                (from anoniem in list where anoniem.Sessie.Id == sessieId select anoniem.GroupName).ToList();

            await Clients.Groups(groups).SendAsync("Game", location);
            await Clients.User(teacher.Id.ToString()).SendAsync("Game", location);


            Console.WriteLine("Start Spel Done");
        }

        public void OpenFirstQuestion(int sessieId, string cookie)
        {
            var sessie = _sessieManager.GetSessieById(sessieId);
            var spelStellingen = sessie.Spel.SpelStellingen.ToList();
            // var teacher = _userManager.Users.Single(x => x.PhoneNumber.Equals(sessieId.ToString()));

            foreach (var spelStelling in spelStellingen)
            {
                spelStelling.Status = Status.Closed;
            }

            spelStellingen[0].Status = Status.Open;

            foreach (var spelStelling in spelStellingen)
            {
                _spelStellingenManager.ChangeSpelStelling(spelStelling);
            }

            Console.WriteLine("First Question Opened");
        }

        public async Task NextQ(int sessieId, string location)
        {
            var teacher = _userManager.Users.Single(x => x.PhoneNumber.Equals(sessieId.ToString()));

            var list = _anoniemeGebruikerManager.GetAnoniemeGebruikers().ToList();

            var groups =
                (from anoniem in list where anoniem.Sessie.Id == sessieId select anoniem.GroupName).ToList();

            var sessie = _sessieManager.GetSessieById(sessieId);
            var spelStellingen = sessie.Spel.SpelStellingen.ToList();

            for (int i = 0; i < spelStellingen.Count; i++)
            {
                if (spelStellingen[i].Status != Status.Open) continue;

                spelStellingen[i].Status = Status.Closed;
                spelStellingen[i + 1].Status = Status.Open;

                _spelStellingenManager.ChangeSpelStelling(spelStellingen[i]);
                _spelStellingenManager.ChangeSpelStelling(spelStellingen[i + 1]);
                break;
            }

            await Clients.Groups(groups).SendAsync("NextQ", location);
            await Clients.User(teacher.Id.ToString()).SendAsync("NextQ", location);


            Console.WriteLine("NextQ Done");
        }

        public async Task PrevQ(int sessieId, string location)
        {
            var teacher = _userManager.Users.Single(x => x.PhoneNumber.Equals(sessieId.ToString()));

            var list = _anoniemeGebruikerManager.GetAnoniemeGebruikers().ToList();

            var groups =
                (from anoniem in list where anoniem.Sessie.Id == sessieId select anoniem.GroupName).ToList();

            var sessie = _sessieManager.GetSessieById(sessieId);
            var spelStellingen = sessie.Spel.SpelStellingen.ToList();

            for (int i = 0; i < spelStellingen.Count; i++)
            {
                if (spelStellingen[i].Status != Status.Open) continue;

                spelStellingen[i].Status = Status.Closed;
                spelStellingen[i - 1].Status = Status.Open;

                _spelStellingenManager.ChangeSpelStelling(spelStellingen[i]);
                _spelStellingenManager.ChangeSpelStelling(spelStellingen[i - 1]);
                break;
            }

            await Clients.Groups(groups).SendAsync("PrevQ", location);
            await Clients.User(teacher.Id.ToString()).SendAsync("PrevQ", location);


            Console.WriteLine("PrevQ Done");
        }

        public void SaveButtonAnswer(string cookie, int stellingId, string antwoord, string argument)
        {
            var anon = _anoniemeGebruikerManager
                .GetAnoniemeGebruikers()
                .ToList()
                .Single(x => x.Cookie.Equals(cookie));

            var answer = new UserAntwoordOpStelling
            {
                AnoniemeGebruiker = anon,
                Antwoord = antwoord,
                StellingId = stellingId,
                Argument = argument
            };
            var found = false;

            foreach (var x in anon.AntwoordenOpStellingen)
            {
                if (x.StellingId == answer.StellingId)
                {
                    x.Antwoord = answer.Antwoord;
                    x.Argument = answer.Argument;
                    _antwoordenManager.ChangeAntwoord(x);
                    _anoniemeGebruikerManager.ChangeAnoniemeGebruiker(anon);
                    found = true;
                    break;
                }
            }

            if (found == false)
            {
                var result = _antwoordenManager.AddAntwoord(answer);
                anon.AntwoordenOpStellingen.Add((UserAntwoordOpStelling) result);
            }

            Console.WriteLine("Button Answer Saved Done");
        }
        
        public async Task FinishGame(int sessieId, string location)
        {
            var teacher = _userManager.Users.Single(x => x.PhoneNumber.Equals(sessieId.ToString()));

            var list = _anoniemeGebruikerManager.GetAnoniemeGebruikers().ToList();

            var groups =
                (from anoniem in list where anoniem.Sessie.Id == sessieId select anoniem.GroupName).ToList();
            
            await Clients.Groups(groups).SendAsync("Finish", location);
            await Clients.User(teacher.Id.ToString()).SendAsync("Finish", location);
        }
    }
}