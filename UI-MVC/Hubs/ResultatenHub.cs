﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using ST.BL.Domain.Antwoorden;
using ST.BL.Domain.Identity;
using ST.BL.Domain.Users;
using ST.BL.Interfaces;

namespace UI_MVC.Hubs
{
    public class ResultatenHub : Hub
    {
        private readonly ISessieManager _sessieManager;
        private readonly IAnoniemeGebruikerManager _anoniemeGebruikerManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IGebruikerManager _gebruikerManager;
        private readonly ISpelStellingenManager _spelStellingenManager;

        public ResultatenHub(ISessieManager sessieManager, IAnoniemeGebruikerManager anoniemeGebruikerManager, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IHttpContextAccessor httpContextAccessor, IGebruikerManager gebruikerManager, ISpelStellingenManager spelStellingenManager)
        {
            _sessieManager = sessieManager;
            _anoniemeGebruikerManager = anoniemeGebruikerManager;
            _userManager = userManager;
            _signInManager = signInManager;
            _httpContextAccessor = httpContextAccessor;
            _gebruikerManager = gebruikerManager;
            _spelStellingenManager = spelStellingenManager;
        }

        public async Task Auth(int sessieId, string cookie)
        {
            var identifier = Context.UserIdentifier;
            int.TryParse(identifier, out var identityId);
            var anon = _anoniemeGebruikerManager.GetAnoniemeGebruikers().ToList().Find(x => x.Cookie.Equals(cookie));

            if (identityId > 0)
            {
                var x = await _userManager.GetUserAsync(Context.User);
                await _userManager.SetPhoneNumberAsync(x, sessieId.ToString());

                var list = _anoniemeGebruikerManager.GetAnoniemeGebruikers().ToList();
                var connectedUsers = list.Count(x => x.Sessie.Id == sessieId);
                //* Send separate message to Teacher
                await Clients.User(identifier).SendAsync("updateDeelnemers", connectedUsers);
                //* Send separate message to Students in Session
                foreach (var item in list)
                {
                    if (item.Sessie.Id == sessieId)
                    {
                        await Clients.Group(item.GroupName).SendAsync("updateDeelnemers", connectedUsers);
                    }
                }
            }
            else if (anon != null)
            {
                var teacher = _userManager.Users.Single(x => x.PhoneNumber.Equals(sessieId.ToString()));

                await AddToGroup(anon.GroupName);

                var list = _anoniemeGebruikerManager.GetAnoniemeGebruikers().ToList();
                var connectedUsers = list.Count(x => x.Sessie.Id == sessieId);
                foreach (var item in list)
                {
                    if (item.Sessie.Id == sessieId)
                    {
                        await Clients.Group(item.GroupName).SendAsync("updateDeelnemers", connectedUsers);
                        await Clients.User(teacher.Id.ToString())
                            .SendAsync("updateDeelnemers", connectedUsers);
                    }
                }
            }
            else
            {
                var teacher = _userManager.Users.Single(x => x.PhoneNumber.Equals(sessieId.ToString()));
                var user = new AnoniemeGebruiker
                {
                    Sessie = _sessieManager.GetSessieById(sessieId),
                    AntwoordenOpStellingen = new List<UserAntwoordOpStelling>(),
                    Cookie = cookie,
                    GroupName = RandomString(10)
                };
                _anoniemeGebruikerManager.AddAnoniemeGebruiker(user);
                await AddToGroup(user.GroupName);
                var list = _anoniemeGebruikerManager.GetAnoniemeGebruikers().ToList();
                var connectedUsers = list.Count(x => x.Sessie.Id == sessieId);

                foreach (var item in list)
                {
                    if (item.Sessie.Id == sessieId)
                    {
                        await Clients.Group(item.GroupName).SendAsync("updateDeelnemers", connectedUsers);
                        await Clients.User(teacher.Id.ToString())
                            .SendAsync("updateDeelnemers", connectedUsers);
                    }
                }
            }
        }
        
        public async Task AddToGroup(string groupName)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, groupName);
        }
        
        private static string RandomString(int length)
        {
            var random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}