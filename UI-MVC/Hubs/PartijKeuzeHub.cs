using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using ST.BL.Domain;
using ST.BL.Domain.Antwoorden;
using ST.BL.Domain.Identity;
using ST.BL.Domain.Spellen;
using ST.BL.Domain.Users;
using ST.BL.Interfaces;

namespace UI_MVC.Hubs
{
    public class PartijKeuzeHub : Hub
    {
        private readonly ISessieManager _sessieManager;
        private readonly IAnoniemeGebruikerManager _anoniemeGebruikerManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IGebruikerManager _gebruikerManager;
        private readonly ISpelStellingenManager _spelStellingenManager;
        private readonly IPartijManager _partijManager;

        public PartijKeuzeHub(
            ISessieManager sessieManager,
            IAnoniemeGebruikerManager anoniemeGebruikerManager,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IHttpContextAccessor httpContextAccessor,
            IGebruikerManager gebruikerManager,
            ISpelStellingenManager spelStellingenManager,
            IPartijManager partijManager
        )
        {
            _sessieManager = sessieManager;
            _anoniemeGebruikerManager = anoniemeGebruikerManager;
            _userManager = userManager;
            _signInManager = signInManager;
            _httpContextAccessor = httpContextAccessor;
            _gebruikerManager = gebruikerManager;
            _spelStellingenManager = spelStellingenManager;
            _partijManager = partijManager;
        }

        public async Task PartijKeuze(int sessieId, int partijKeuze, string cookie, string location)
        {
            var anon = _anoniemeGebruikerManager.GetAnoniemeGebruikers().ToList().Find(x => x.Cookie.Equals(cookie));

            if (anon != null)
            {
                anon.Partij = _partijManager.GetPartijById(partijKeuze);
                await Clients.Group(anon.GroupName).SendAsync("startGame", location);
            }
        }

        public async Task StartSpel(int sessieId, string location, string locationTeacher)
        {
            var teacher = _userManager.Users.Single(x => x.PhoneNumber.Equals(sessieId.ToString()));

            var list = _anoniemeGebruikerManager.GetAnoniemeGebruikers().ToList();

            var sessie = _sessieManager.GetSessieById(sessieId);


            foreach (var item in list)
            {
                if (item.Sessie.Id == sessieId && sessie.Spel is PartijSpel)
                {
                    await Clients.Group(item.GroupName).SendAsync("PartijKeuze", location);
                    await Clients.User(teacher.Id.ToString())
                        .SendAsync("startGame", locationTeacher);
                }
            }
        }
    }
}