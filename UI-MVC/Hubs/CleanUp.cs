﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using ST.BL.Domain;
using ST.BL.Domain.Identity;
using ST.BL.Interfaces;

namespace UI_MVC.Hubs
{
    public class CleanUp : Hub
    {
        private readonly IGebruikerStellingenManager _gebruikerStellingenManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IGebruikerManager _gebruikerManager;

        public CleanUp(
            IGebruikerStellingenManager gebruikerStellingenManager,
            UserManager<ApplicationUser> userManager,
            IGebruikerManager gebruikerManager
        )
        {
            _gebruikerStellingenManager = gebruikerStellingenManager;
            _userManager = userManager;
            _gebruikerManager = gebruikerManager;
        }

        public async Task StartCleanUp()
        {
            var user = await _userManager.GetUserAsync(Context.User);
            var gebruiker = _gebruikerManager.GetGebruikerByIdentity(user);
            var gebruikerStellingen = _gebruikerStellingenManager
                .GetGebruikerStellingen()
                .Where(x => x.Gebruiker == gebruiker)
                .Where(x => x.Stelling.Status == Status.Pending)
                .ToList();

            var result = _gebruikerStellingenManager.RemoveGebruikerStellingRange(gebruikerStellingen);
            if (result > 0) Console.Write($"Deleted Temp files [{result}]");
        }
    }
}