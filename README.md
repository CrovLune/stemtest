# Integratieproject 1 - 2019-2020
Team Tree

### Teamleden
  - Goedgezelschap Lawrence
  - Hellebaut Bart
  - Noels Quinten
  - Reynders Matthias
  - Van Tiggel Noah
  - Zelenskiy Aleksey
  
### Project
.NET backend

### Langere beschrijving

1)  SQLServer database updaten via terminal in Rider (drop db schema met naam 'stemtest' indien deze al aanwezig is)
```sh
$ dotnet-ef migrations add InitialCreate -s ./UI-MVC -p ./DAL
$ dotnet-ef database update -s ./UI-MVC -p ./DAL
```
2) Build de .scss files
```sh
$ cd UI-MVC/Client
$ npm install
$ npm run build
```
3) Build en Run UI-MVC:UI_MVC in Rider
4) Account aanmaken, hiermee kan u in uw account pagina rollen toewijzen (https://localhost:5001/Identity/Account/Manage/Roles)
5) in wwwroot staat een csv bestand, hiermee kan u via de link localhost/spellen/importcsv stellingen importeren (https://localhost:5001/spellen/importcsv)

