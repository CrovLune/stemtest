﻿// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo

using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ST.BL.Domain.Sessies;
using ST.DAL.EF;
using ST.DAL.Interfaces;

namespace ST.DAL
{
    public class SessieRepository : ISessieRepository
    {
        private readonly TreeProjectDbContext _ctx;

        public SessieRepository(TreeProjectDbContext context)
        {
            _ctx = context;
        }

        public Sessie CreateSessie(Sessie sessie)
        {
            var item = _ctx.Sessies.Add(sessie);
            _ctx.SaveChanges();
            return item.Entity;
        }

        public IEnumerable<Sessie> ReadSessies()
        {
            return _ctx.Sessies
                .Include(s => s.Creator)
                .Include(s => s.Klas)
                .Include(s => s.Spel)
                .ThenInclude(s => s.Stellingen)
                .Include(s=>s.AnoniemeGebruikers)
                .AsEnumerable();
        }

        public Sessie ReadSessieById(int id)
        {
            return ReadSessies()
                .ToList()
                .Find(s => s.Id == id);
        }

        public int UpdateSessie(Sessie sessie)
        {
            _ctx.Sessies.Update(sessie);
            return _ctx.SaveChanges();
        }

        public int UpdateSessieRange(IEnumerable<Sessie> sessies)
        {
            _ctx.Sessies.UpdateRange(sessies);
            return _ctx.SaveChanges();
        }

        public int DeleteSessie(Sessie sessie)
        {
            _ctx.Sessies.Remove(sessie);
            return _ctx.SaveChanges();
        }
    }
}