﻿using System.Collections.Generic;
using System.Linq;
using ST.BL.Domain.Antwoorden;
using ST.DAL.EF;
using ST.DAL.Interfaces;

namespace ST.DAL
{
    public class AntwoordRepository : IAntwoordRepository
    {
        private readonly TreeProjectDbContext _ctx;

        public AntwoordRepository(TreeProjectDbContext context)
        {
            _ctx = context;
        }

        public Antwoord CreateAntwoord(Antwoord antwoord)
        {
            var item = _ctx.Add(antwoord);
            _ctx.SaveChanges();
            return item.Entity;
        }

        public IEnumerable<Antwoord> ReadAntwoorden()
        {
            return _ctx.Antwoorden.AsEnumerable();
        }
        public IEnumerable<Antwoord> ReadPartijAntwoorden()
        {
            return _ctx.Antwoorden.Where(s=>s is PartijAntwoordOpStelling).AsEnumerable();
        }
        public IEnumerable<Antwoord> ReadUserAntwoorden()
        {
            return _ctx.Antwoorden.Where(x=>x is UserAntwoordOpStelling).AsEnumerable();
        }

        public int UpdateAntwoord(Antwoord antwoord)
        {
            _ctx.Antwoorden.Update(antwoord);
            return _ctx.SaveChanges();
        }

        public int UpdateAntwoordRange(List<Antwoord> antwoorden)
        {
            _ctx.Antwoorden.UpdateRange(antwoorden);
            return _ctx.SaveChanges();
        }

        public int DeleteAntwoord(Antwoord antwoord)
        {
            _ctx.Antwoorden.Remove(antwoord);
            return _ctx.SaveChanges();
        }
    }
}