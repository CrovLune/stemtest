using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ST.BL.Domain.Scholen;
using ST.BL.Domain.Users;
using ST.DAL.EF;
using ST.DAL.Interfaces;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace ST.DAL
{
    public class KlasRepository : IKlasRepository
    {
        private readonly TreeProjectDbContext _ctx;

        public KlasRepository(TreeProjectDbContext context)
        {
            _ctx = context;
        }

        public Klas CreateKlas(Klas klas)
        {
            var item = _ctx.Klassen.Add(klas);
            _ctx.SaveChanges();
            return item.Entity;
        }

        public IEnumerable<Klas> ReadKlassen()
        {
            return _ctx.Klassen
                .Include(k=>k.Sessies)
                .AsEnumerable();
        }

        public Klas ReadKlasById(int klasId)
        {
            return ReadKlassen()
                .ToList()
                .Find(k => k.Id == klasId);
        }

        public int UpdateKlas(Klas klas)
        {
            _ctx.Klassen.Update(klas);
            return _ctx.SaveChanges();
        }

        public int DeleteKlas(Klas klas)
        {
            _ctx.Klassen.Remove(klas);
            return _ctx.SaveChanges();
        }
    }
}