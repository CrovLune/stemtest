using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ST.BL.Domain.Partijen;
using ST.DAL.Interfaces;
using ST.DAL.EF;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo

namespace ST.DAL
{
    public class PartijRepository : IPartijRepository
    {
        private readonly TreeProjectDbContext _ctx;

        public PartijRepository(TreeProjectDbContext context)
        {
            _ctx = context;
        }

        //CRUD
        public Partij CreatePartij(Partij partij)
        {
            var item = _ctx.Partijen.Add(partij);
            _ctx.SaveChanges();
            return item.Entity;
        }

        public IEnumerable<Partij> ReadPartijen()
        {
            return _ctx.Partijen
                .AsEnumerable();
        }

        public Partij ReadPartijById(int partijId)
        {
            return ReadPartijen()
                .ToList()
                .Find(p => p.Id == partijId);
        }

        public int UpdatePartij(Partij partij)
        {
            _ctx.Partijen.Update(partij);
            return _ctx.SaveChanges();
        }

        public int DeletePartij(Partij partij)
        {
            _ctx.Partijen.Remove(partij);
            return _ctx.SaveChanges();
        }
    }
}