﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ST.BL.Domain.InternMediateClasses;
using ST.DAL.EF;
using ST.DAL.Interfaces;

// ReSharper disable IdentifierTypo

namespace ST.DAL
{
    public class GebruikerStellingenRepository : IGebruikerStellingenRepository
    {
        private readonly TreeProjectDbContext _ctx;

        public GebruikerStellingenRepository(TreeProjectDbContext context)
        {
            _ctx = context;
        }

        public GebruikerStellingen CreateGebruikerStelling(GebruikerStellingen gebruikerStelling)
        {
            var item = _ctx.GebruikerStellingen.Add(gebruikerStelling);
            _ctx.SaveChanges();
            return item.Entity;
        }

        public IEnumerable<GebruikerStellingen> ReadGebruikerStellingen()
        {
            return _ctx.GebruikerStellingen
                .Include(s => s.Gebruiker)
                .Include(s => s.Stelling)
                    .ThenInclude(s => s.UserAntwoordOpStelling)
                .Include(s => s.Stelling)
                    .ThenInclude(s => s.PartijAntwoordOpStelling)
                        .ThenInclude(p => p.Partij)
                .Include(s => s.Stelling)
                .Include(s => s.Stelling.MoeilijkeWoorden)
                .AsEnumerable();
        }

        public int UpdateGebruikerStelling(GebruikerStellingen gebruikerStelling)
        {
            _ctx.GebruikerStellingen.Update(gebruikerStelling);
            return _ctx.SaveChanges();
        }

        public int UpdateGebruikerStellingRange(IEnumerable<GebruikerStellingen> gebruikerStellingen)
        {
            _ctx.GebruikerStellingen.UpdateRange(gebruikerStellingen);
            return _ctx.SaveChanges();
        }
        public int DeleteGebruikerStelling(GebruikerStellingen gebruikerStelling)
        {
            _ctx.GebruikerStellingen.Remove(gebruikerStelling);
            return _ctx.SaveChanges();
        }

        public int DeleteGebruikerStellingRange(IEnumerable<GebruikerStellingen> gebruikerStellingen)
        {
            _ctx.GebruikerStellingen.RemoveRange(gebruikerStellingen);
            return _ctx.SaveChanges();
        }
    }
}