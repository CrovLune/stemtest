using System.Linq;
using System.Collections.Generic;
using ST.BL.Domain.Users;
using ST.DAL.EF;
using ST.DAL.Interfaces;

namespace ST.DAL
{
    public class AnoniemeGebruikerRepository : IAnoniemeGebruikerRepository
    {
        private readonly TreeProjectDbContext _ctx;


        public AnoniemeGebruikerRepository(TreeProjectDbContext context)
        {
            _ctx = context;
        }

        public AnoniemeGebruiker CreateAnoniemeGebruiker(AnoniemeGebruiker anoniemeGebruiker)
        {
            var item = _ctx.AnoniemeGebruikers.Add(anoniemeGebruiker);
            _ctx.SaveChanges();
            return item.Entity;
        }

        public IEnumerable<AnoniemeGebruiker> ReadAnoniemeGebruikers()
        {
            return _ctx.AnoniemeGebruikers.AsEnumerable();
        }

        public int UpdateAnoniemeGebruiker(AnoniemeGebruiker anoniemeGebruiker)
        {
            _ctx.AnoniemeGebruikers.Update(anoniemeGebruiker);
            return _ctx.SaveChanges();
        }

        public int UpdateAnoniemeGebruikersRange(List<AnoniemeGebruiker> anoniemeGebruikers)
        {
            _ctx.AnoniemeGebruikers.UpdateRange(anoniemeGebruikers);
            return _ctx.SaveChanges();
        }

        public AnoniemeGebruiker ReadAnoniemeGebruikerById(int id)
        {
            return ReadAnoniemeGebruikers().ToList().Find(x => x.Id == id);
        }

        public int DeleteAnoniemeGebruiker(AnoniemeGebruiker anoniemeGebruiker)
        {
            _ctx.AnoniemeGebruikers.Remove(anoniemeGebruiker);
            return _ctx.SaveChanges();
        }

        public int DeleteAnoniemeGebruikesRange(List<AnoniemeGebruiker> anoniemeGebruikers)
        {
            _ctx.AnoniemeGebruikers.RemoveRange(anoniemeGebruikers);
            return _ctx.SaveChanges();
        }
    }
}