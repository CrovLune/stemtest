using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ST.BL.Domain.Scholen;
using ST.BL.Domain.Users;
using ST.DAL.EF;
using ST.DAL.Interfaces;

// ReSharper disable IdentifierTypo

namespace ST.DAL
{
    public class SchoolRepository : ISchoolRepository
    {
        private readonly TreeProjectDbContext _ctx;

        public SchoolRepository(TreeProjectDbContext context)
        {
            _ctx = context;
        }

        public School CreateSchool(School school)
        {
            var item = _ctx.Scholen.Add(school);
            _ctx.SaveChanges();
            return item.Entity;
        }

        public IEnumerable<School> ReadScholen()
        {
            return _ctx.Scholen
                .Include(s => s.Gebruikers)
                .Include(s => s.Klassen)
                .Include(s => s.Adres)
                .AsEnumerable();
        }

        public School ReadSchoolById(int schoolId)
        {
            return ReadScholen()
                .ToList()
                .Find(s => s.Id == schoolId);
        }

        public int UpdateSchool(School school)
        {
            _ctx.Scholen.Update(school);
            return _ctx.SaveChanges();
        }

        public int DeleteSchool(School school)
        {
            _ctx.Scholen.Remove(school);
            return _ctx.SaveChanges();
        }
    }
}