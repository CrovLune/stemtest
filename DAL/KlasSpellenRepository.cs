﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ST.BL.Domain.InternMediateClasses;
using ST.DAL.EF;
using ST.DAL.Interfaces;

namespace ST.DAL
{
    public class KlasSpellenRepository : IKlasSpellenRepository
    {
        private readonly TreeProjectDbContext _ctx;

        public KlasSpellenRepository(TreeProjectDbContext context)
        {
            _ctx = context;
        }

        public KlasSpellen CreateKlasSpel(KlasSpellen klasSpel)
        {
            var item = _ctx.KlasSpellen.Add(klasSpel);
            _ctx.SaveChanges();
            return item.Entity;
        }

        public IEnumerable<KlasSpellen> ReadKlasSpellen()
        {
            return _ctx.KlasSpellen
                .Include(s => s.Klas)
                    .ThenInclude(s=>s.Sessies)
                .Include(s => s.Spel)
                    .ThenInclude(s=>s.options)
                .Include(s=>s.Spel.Stellingen)
                .AsEnumerable();
        }

        public int UpdateKlasSpel(KlasSpellen klasSpel)
        {
            _ctx.KlasSpellen.Update(klasSpel);
            return _ctx.SaveChanges();
        }

        public int UpdateKlasSpelRange(IEnumerable<KlasSpellen> klasSpellen)
        {
            _ctx.KlasSpellen.UpdateRange(klasSpellen);
            return _ctx.SaveChanges();
        }
        public int DeleteKlasSpel(KlasSpellen klasSpel)
        {
            _ctx.KlasSpellen.Remove(klasSpel);
            return _ctx.SaveChanges();
        }
    }
}