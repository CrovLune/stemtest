﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ST.BL.Domain.Antwoorden;
using ST.BL.Domain.Identity;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Domain.Partijen;
using ST.BL.Domain.Scholen;
using ST.BL.Domain.Sessies;
using ST.BL.Domain.Spellen;
using ST.BL.Domain.Stellingen;
using ST.BL.Domain.Users;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo

namespace ST.DAL.EF
{
    public class TreeProjectDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int>
    {
        private readonly string _connectionString;
        private readonly bool _useConnectionString;
        
        public TreeProjectDbContext(DbContextOptions<TreeProjectDbContext> options) : base(options)
        {
            TreeProjectDbInitializer.Initialize(this, dropCreateDatabase: false);
        }

        public TreeProjectDbContext()
        {
            //Environment variable ophalen = Development of Production
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            //Builder opject maken
            var builder = new ConfigurationBuilder();
            //json files inladen
            builder.AddJsonFile("appsettings.json", true, true)
                .AddJsonFile("appsettings." + environmentName + ".json", true, true);
            var configuration = builder.Build();
            //Connection string ophalen uit configuration
            _connectionString = configuration.GetConnectionString("MySQL");
            //Aangeven dat we connection string gebruiken -> voor later
            _useConnectionString = true;

            TreeProjectDbInitializer.Initialize(this, dropCreateDatabase: false);
        }

        public DbSet<AnoniemeGebruiker> AnoniemeGebruikers { get; set; }
        public DbSet<Antwoord> Antwoorden { get; set; }
        public DbSet<GebruikerKlassen> GebruikerKlasses { get; set; }
        public DbSet<PartijStellingen> PartijStellingen { get; set; }
        public DbSet<GebruikerStellingen> GebruikerStellingen { get; set; }
        public DbSet<KlasSpellen> KlasSpellen { get; set; }
        public DbSet<Partij> Partijen { get; set; }
        public DbSet<Klas> Klassen { get; set; }
        public DbSet<School> Scholen { get; set; }
        public DbSet<Spel> Spellen { get; set; }
        public DbSet<Sessie> Sessies { get; set; }
        public DbSet<Stelling> Stellingen { get; set; }
        public DbSet<Gebruiker> Gebruikers { get; set; }
        public DbSet<MoeilijkWoord> MoeilijkWoorden { get; set; }
        public DbSet<SpelStellingen> SpelStellingen { get; set; }


     

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //* Lazy-Loading
            optionsBuilder.UseLazyLoadingProxies();
            
            if (_useConnectionString)
            {
                optionsBuilder.UseMySql(_connectionString);
            }
            

            // configure logging-information
            optionsBuilder.UseLoggerFactory(LoggerFactory.Create(
                builder => builder.AddDebug()
            ));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Cascade;
            }

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<DebatSpel>().HasBaseType<Spel>();
            modelBuilder.Entity<PartijSpel>().HasBaseType<Spel>();
            modelBuilder.Entity<PartijAntwoordOpStelling>().HasBaseType<Antwoord>();
            modelBuilder.Entity<UserAntwoordOpStelling>().HasBaseType<Antwoord>();

            modelBuilder.Entity<AnoniemeGebruiker>()
                .HasKey(x => x.Id);
            modelBuilder.Entity<SpelStellingen>()
                .HasKey(key => new {key.SpelId, key.StellingId});
            modelBuilder.Entity<MoeilijkWoord>()
                .HasKey(key => key.Id);
            modelBuilder.Entity<Antwoord>()
                .HasKey(key => key.Id);
            modelBuilder.Entity<GebruikerKlassen>()
                .HasKey(key => new {key.GebruikerId, key.KlasId});
            modelBuilder.Entity<PartijStellingen>()
                .HasKey(key => new {key.PartijId, key.StellingId});
            modelBuilder.Entity<GebruikerStellingen>()
                .HasKey(key => new {key.GebruikerId, key.StellingId});
            modelBuilder.Entity<KlasSpellen>()
                .HasKey(key => new {key.KlasId, key.SpelId});
            modelBuilder.Entity<Partij>()
                .HasKey(key => key.Id);
            modelBuilder.Entity<Klas>()
                .HasKey(key => key.Id);
            modelBuilder.Entity<School>()
                .HasKey(key => key.Id);
            modelBuilder.Entity<Spel>()
                .HasKey(key => key.SpelId);
            modelBuilder.Entity<Sessie>()
                .HasKey(key => key.Id);
            modelBuilder.Entity<Stelling>()
                .HasKey(key => key.Id);
            modelBuilder.Entity<Gebruiker>()
                .HasKey(key => key.Id);

            //* shadowprops

            modelBuilder.Entity<AnoniemeGebruiker>()
                .HasOne(x => x.Sessie)
                .WithMany(x => x.AnoniemeGebruikers);
            modelBuilder.Entity<Sessie>()
                .HasMany(x => x.AnoniemeGebruikers)
                .WithOne(x => x.Sessie);
            modelBuilder.Entity<AnoniemeGebruiker>()
                .HasMany(x => x.AntwoordenOpStellingen)
                .WithOne(x => x.AnoniemeGebruiker);
            modelBuilder.Entity<UserAntwoordOpStelling>()
                .HasOne(x => x.AnoniemeGebruiker)
                .WithMany(x => x.AntwoordenOpStellingen);
            modelBuilder.Entity<Stelling>()
                .HasMany(s => s.MoeilijkeWoorden)
                .WithOne(m => m.Stelling);

            modelBuilder.Entity<Stelling>()
                .HasMany(s => s.PartijAntwoordOpStelling)
                .WithOne(a => a.Stelling);
            modelBuilder.Entity<Stelling>()
                .HasMany(s => s.UserAntwoordOpStelling)
                .WithOne(a => a.Stelling);

            modelBuilder.Entity<MoeilijkWoord>()
                .HasOne(m => m.Stelling)
                .WithMany(s => s.MoeilijkeWoorden);

            modelBuilder.Entity<GebruikerKlassen>()
                .HasOne(g => g.Gebruiker)
                .WithMany(k => k.GebruikerKlassen)
                .HasForeignKey(t => t.GebruikerId);

            modelBuilder.Entity<GebruikerKlassen>()
                .HasOne(k => k.klas)
                .WithMany(g => g.GebruikerKlassen)
                .HasForeignKey(t => t.KlasId);

            modelBuilder.Entity<PartijStellingen>()
                .HasOne(p => p.Partij)
                .WithMany(s => s.PartijStellingen)
                .HasForeignKey(fkey => fkey.PartijId);

            modelBuilder.Entity<PartijStellingen>()
                .HasOne(s => s.Stelling)
                .WithMany(p => p.PartijStellingen)
                .HasForeignKey(fkey => fkey.StellingId);

            modelBuilder.Entity<GebruikerStellingen>()
                .HasOne(s => s.Gebruiker)
                .WithMany(s => s.GebruikerStellingen)
                .HasForeignKey(fkey => fkey.GebruikerId);

            modelBuilder.Entity<GebruikerStellingen>()
                .HasOne(s => s.Stelling)
                .WithMany(s => s.GebruikerStellingen)
                .HasForeignKey(fkey => fkey.StellingId);

            modelBuilder.Entity<KlasSpellen>()
                .HasOne(s => s.Klas)
                .WithMany(s => s.KlasSpellen)
                .HasForeignKey(fkey => fkey.KlasId);

            modelBuilder.Entity<KlasSpellen>()
                .HasOne(s => s.Spel)
                .WithMany(s => s.KlasSpellen)
                .HasForeignKey(fkey => fkey.SpelId);

            modelBuilder.Entity<Klas>()
                .HasOne(k => k.School)
                .WithMany(s => s.Klassen);

            modelBuilder.Entity<School>()
                .HasMany(s => s.Gebruikers)
                .WithOne(g => g.School);

            modelBuilder.Entity<Gebruiker>()
                .HasOne(g => g.School)
                .WithMany(s => s.Gebruikers);

            modelBuilder.Entity<ApplicationUser>().Property<int>("FK_idenGeb");
            modelBuilder.Entity<ApplicationUser>()
                .HasOne(a => a.Gebruiker)
                .WithOne(g => g.Identity)
                .HasForeignKey<ApplicationUser>("FK_idenGeb")
                .IsRequired();
            
            modelBuilder.Entity<SpelStellingen>()
                .HasOne(s => s.Stelling)
                .WithMany(sp => sp.SpelStellingen)
                .HasForeignKey(t => t.StellingId);

            modelBuilder.Entity<SpelStellingen>()
                .HasOne(s => s.Spel)
                .WithMany(sp => sp.SpelStellingen)
                .HasForeignKey(t => t.SpelId);

        }
    }
}