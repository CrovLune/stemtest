﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using ST.BL.Domain;
using ST.BL.Domain.Antwoorden;
using ST.BL.Domain.Identity;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Domain.Partijen;
using ST.BL.Domain.Scholen;
using ST.BL.Domain.Stellingen;
using ST.BL.Domain.Users;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo


namespace ST.DAL.EF
{
    internal static class TreeProjectDbInitializer
    {
        public static void Initialize(TreeProjectDbContext context, bool dropCreateDatabase = false)
        {
            if (context.Database.CanConnect() && !context.Gebruikers.Any())
            {
                 Seed(context);
            }
        }

         private static void Seed(TreeProjectDbContext context)
                   {   var p1 = new Partij()
            {
                Naam = "CD&V",
                Beschrijving = "",
                PartijInfo = new PartijInfo
                {
                    Info =
                        "CD&V - de Vlaamse christendemocraten - is meer nog dan een politieke partij, een brede beweging van geëngageerde mensen. CD&V profileert zich als enthousiaste partner van gezinnen, ondernemers en verenigingen. Met een hecht team van ervaren en bekwame politici versterken en ondersteunen we uw verhaal. Want wij liggen wakker van datgene waar u van droomt. En… we maken er graag samen werk van!"
                },
                VisieOmschrijving = "",
                VolledigeNaam = "Christen-Democratisch en Vlaams",
                Logo = "CD&V.png"
            };
            var p2 = new Partij()
            {
                Naam = "sp.a",
                Beschrijving = "",
                PartijInfo = new PartijInfo
                {
                    Info =
                        "Om het samen beter te maken voor iedereen - zonder onderscheid - is een sterke socialistische partij nodig. Dat heeft de geschiedenis al aangetoond. Dit jaar bestaat de sociale zekerheid 75 jaar. Destijds was het onze verdienste dat die zekerheid voor de mensen er kwam.Vandaag leven we opnieuw in tijden waarin niets nog zeker lijkt. Of het nu een deftig pensioen is, een job die loont om de facturen te betalen of de beste en betaalbare zorg wanneer die nodig is. Wij vinden die onzekerheid niet normaal.Daarom is het aan ons om opnieuw voor die zekerheid te zorgen. Voor iedereen. En de reden waarom sp.a dat moet doen, is heel simpel: omdat niemand anders het zal doen.Zekerheid. Voor iedereen. #NieuweStrijd"
                },
                VisieOmschrijving = "",
                VolledigeNaam = "Socialistische Partij Anders",
                Logo = "sp.a.png"
            };
            var p3 = new Partij()
            {
                Naam = "openVLD",
                Beschrijving = "",
                PartijInfo = new PartijInfo
                {
                    Info =
                        "OpenVld is een liberale democratische partij die resoluut kiest voor een open samenleving. Een samenleving waarin elke persoon zoveel mogelijk kansen krijgt om zijn of haar leven uit te bouwen. Niet om zichzelf ten koste van anderen te realiseren, maar om op een vrije en verantwoordelijke manier bij te dragen tot die open samenleving."
                },
                VisieOmschrijving = "",
                VolledigeNaam = "Open Vlaams Liberalen en Democraten",
                Logo = "openVLD.png"
            };
            var p4 = new Partij()
            {
                Naam = "Groen",
                Beschrijving = "",
                PartijInfo = new PartijInfo
                {
                    Info =
                        "Met het verhaal dat het allemaal slecht gaat, nemen we geen genoegen. Duizenden doeners tonen dat het anders kan. Samen bouwen we aan een menselijkere, eerlijkere en gezondere samenleving. Overheid en economie bestaan niet voor zichzelf. Ze staan ten dienste van mensen. Wie een job wil, verdient werkbaar werk. Wie nood heeft aan hulp, krijgt gepaste en betaalbare zorg op het juiste moment. Wie kinderen heeft, moet kunnen vertrouwen op kwaliteitsvolle opvang en onderwijs. Wie de ouderdom voelt wegen, heeft recht op een omgeving om waardig te leven. En wie z'n thuisland moest ontvluchten, kan hier rekenen op een menselijke behandeling. Ongelijkheid geeft in het beste geval een handvol winnaars. Groen plaatst eerlijkheid en gelijke kansen voorop. Dat begint bij de belastingen. Vervuilers en grote vermogens moeten meer bijdragen dan mensen die werken en ondernemen. Grote bedrijven mogen de dans niet ontspringen. Bovendien heeft iedereen recht op betaalbaar wonen en een menswaardig inkomen. Armoede aanvaarden we niet. En onze samenleving mag niemand uitsluiten op basis van leeftijd, geloof, etnisch-culturele achtergrond, seksualiteit of gender. De mens heeft nog nooit zo'n grote impact gehad op het milieu. Hoog tijd voor actie. Voor een klimaat dat niet op hol slaat, doordat we met hernieuwbare energie de CO2-uitstoot verminderen in plaats van fossiele brandstoffen te blijven stoken. Ruimte voor natuur en bossen: behouden wat er is en extra planten waar te weinig is, ook dichtbij woonkernen. Respectvol omgaan met dieren. Een evenwicht waarbij levenskwaliteit voor mens en dier hand in hand gaat met propere lucht, zuiver water en vruchtbare grond. Als we onze omgeving met zorg behandelen, staan we ook zelf gezonder in het leven."
                },
                VisieOmschrijving = "",
                VolledigeNaam = "Groen",
                Logo = "Groen.png"
            };
            var p5 = new Partij()
            {
                Naam = "N-VA",
                Beschrijving = "",
                PartijInfo = new PartijInfo
                {
                    Info =
                        "De N-VA is een relatief jonge politieke partij die een moderne, vooruitziende en democratische vorm van het Vlaamse Nationalisme promoot. De N-VA verdedigt de politieke, sociale, economische en culturele interesses van 6.5miljoen mensen in Vlaanderen door het"
                },
                VisieOmschrijving = "",
                VolledigeNaam = "Nieuwe-Vlaamse-Alliantie",
                Logo = "N-VA.png"
            };
            var p6 = new Partij()
            {
                Naam = "VB",
                Beschrijving = "",
                PartijInfo = new PartijInfo
                {
                    Info =
                        "De partij is de partijpolitieke tolk van de Vlaamse Beweging, zoals die historisch is gegroeid, en ze verdedigt op het politieke forum de eisen van die Vlaamse Beweging, zoals ondermeer het zelfbestuur, de afschaffing van de faciliteiten, de terugkeer van de ons ontstolen gebieden, het nooit-meer-oorlog en de amnestie-eis.De partij is een Vlaams-nationalistische partij, een instrument voor een politiek van nationale en culturele identiteit in Vlaanderen. Dit betekent dat de partij met haar politieke actie ervoor wil zorgen dat culturele identiteit en volksgemeenschap (mede) bepalend worden voor de inrichting en het bestuur van de staat. De staat is slechts een structuur en is op grond van het zelfbeschikkingsrecht ondergeschikt aan de volksgemeenschap. De staat dient de belangen van het volk en niet omgekeerd.De partij is tevens een rechts-nationalistische partij, omdat zij de vrije mens erkent zoals hij is en dus de ideologieën verwerpt die van de maakbaarheid van de mens uitgaan. Tradities, waarden en normen, zoals die zijn gegroeid, moeten gerespecteerd worden en mee deel uitmaken van de manier waarop de toekomst vorm wordt gegeven."
                },
                VisieOmschrijving = "",
                VolledigeNaam = "Vlaams Belang",
                Logo = "VB.png"
            };
            var p7 = new Partij()
            {
                Naam = "PVDA",
                Beschrijving = "",
                PartijInfo = new PartijInfo
                {
                    Info =
                        "Eerst de mensen, niet de winst. Niets typeert de PVDA (Partij van de Arbeid) beter dan dit uitgangspunt. Mensen zijn geen nummer. Een mens is meer. Een samenleving is geen jackpot. Een samenleving is meer.De commercie wint steeds meer veld. Gezondheid, werk, onderwijs, milieu, inspraak, internationale solidariteit en vrede worden opgeofferd aan het koude geldgewin. Wij willen de prioriteiten opnieuw juist stellen. De samenleving weer op haar voeten zetten. Vandaar ‘eerst de mensen, niet de winst’."
                },
                VisieOmschrijving = "",
                VolledigeNaam = "Partij van de Arbeid",
                Logo = "PVDA.png"
            };
            
            var s1 = new School
            {
                Naam = "Heilig Graf Turnhout", MailContactpersoon = "HeiligGrafTh@hotmail.com",
                Adres = new Adres {HuisNr = 1, Land = "Belgie", Postcode = 2400, StraatNaam = "Oude Markt"}
            };
            var s2 = new School
            {
                Naam = "Karel de Grote hogeschool", 
                MailContactpersoon = "info@kdg.be",
                Adres = new Adres {HuisNr = 5, Land = "Belgie", Postcode = 2000, StraatNaam = "Nationalestraat"}
            };
            context.Scholen.Add(s1);
            context.Scholen.Add(s2);
            
            var leerkracht = new Gebruiker()
            {
                Username = "leerkracht@school.be",
                Voornaam = "Meneer",
                Achternaam = "De Leerkracht",
                Email = "superadmin@school.be",
                School = s1
            };
            var admin = new Gebruiker()
            {
                Username = "admin@school.be",
                Voornaam = "Meneer",
                Achternaam = "De admin",
                Email = "superadmin@school.be",
                School = s1
            };
            var superadmin = new Gebruiker()
            {
                Username = "superadmin@school.be",
                Voornaam = "Meneer",
                Achternaam = "De superadmin",
                Email = "superadmin@school.be"
            };
            context.Gebruikers.Add(leerkracht);
            context.Gebruikers.Add(admin);
            context.Gebruikers.Add(superadmin);
            
            var k1 = new Klas()
            {
                AantalLeerlingen = 20, Naam = "INF201", StudieJaar = 2, School = s1
            };
            var k2 = new Klas()
            {
                AantalLeerlingen = 20, Naam = "INF202", StudieJaar = 2, School = s1
            };
            var k3 = new Klas()
            {
                AantalLeerlingen = 20, Naam = "INF203", StudieJaar = 2, School = s1
            };
            context.Klassen.Add(k1);
            context.Klassen.Add(k2);
            context.Klassen.Add(k3);
            
            var gk1 = new GebruikerKlassen()
            {
                Gebruiker = leerkracht,
                klas = k1
            };
            var gk2 = new GebruikerKlassen()
            {
                Gebruiker = leerkracht,
                klas = k2
            };
            var gk3 = new GebruikerKlassen()
            {
                Gebruiker = leerkracht,
                klas = k3
            };
            context.GebruikerKlasses.Add(gk1);
            context.GebruikerKlasses.Add(gk2);
            context.GebruikerKlasses.Add(gk3);

            var st1 = new Stelling
            {
                StellingOnderwerp = "Erfenisbelasting"
                ,
                Vraag =
                    "De erfenisbelastingen moeten verder dalen",
                Status = Status.Open
            };
            context.Stellingen.Add(st1);
            var st2 = new Stelling
            {
                StellingOnderwerp = "Catalonië steunen",
                Vraag = "Vlaanderen moet Catalonië steunen in zijn stappen naar onafhankelijkheid",
                Status = Status.Open
            };
            context.Stellingen.Add(st2);
            var st3 = new Stelling
            {
                StellingOnderwerp = "Vrouwen en mannen in regering",
                Vraag = "Er moeten evenveel vrouwen als mannen in de Vlaamse regering zitten",
                Status = Status.Open
            };
            context.Stellingen.Add(st3);
            var st4 = new Stelling
            {
                StellingOnderwerp = "Verplicht Nederlands op speelplaats",
                Vraag = "Scholen moeten kinderen verplichten om ook op de speelplaats Nederlands te praten",
                Status = Status.Open
            };
            context.Stellingen.Add(st4);
            
            var pa1 = new PartijAntwoordOpStelling()
            {
                Antwoord = "Eens",
                Stelling = st1,
                Partij = p1,
                Uitleg = "De erfenisrechten zijn deze legislatuur al fors verlaagd. Deze inkomsten zijn een belangrijk onderdeel van de Vlaamse begroting, waarmee we bijvoorbeeld onderwijs en welzijn financieren."
            };
            var pa2 = new PartijAntwoordOpStelling()
            {
                Antwoord = "Oneens",
                Stelling = st1,
                Partij = p2,
                Uitleg = "Erfenisbelastingen raken vooral de middenklasse en onverwachte overlijdens. De grote vermogens gebruiken massaal (perfect legale) ontwijkingsmogelijkheden. De invoering van een vermogensrendementsheffing maakt de erfbelasting overbodig."
            };
            var pa3 = new PartijAntwoordOpStelling
            {
                Antwoord = "Oneens",
                Stelling = st1,
                Partij = p3,
                Uitleg = "De erfbelasting is nog steeds hoog en willen we verder verlagen"
            };
            var pa4 = new PartijAntwoordOpStelling()
            {
                Antwoord = "Oneens",
                Stelling = st1,
                Partij = p4,
                Uitleg = "Erfenissen zijn het resultaat van jarenlang werken, sparen en ondernemen. We zetten daarom opnieuw in op een verdere verlaging van de successierechten, waarbij minstens een eerste schijf steeds is vrijgesteld van belasting."
            };
            var pa5 = new PartijAntwoordOpStelling()
            {
                Antwoord = "Eens",
                Stelling = st1,
                Partij = p5,
                Uitleg = "We willen dat iedereen gedurende zijn of haar leven 25Eens.EensEensEens euro belastingvrij kan erven of verwerven uit schenkingen. Dat is perfect betaalbaar als we de achterpoorten sluiten die grote vermogens vandaag gebruiken om geen erfbelasting te betalen.."
            };
            var pa6 = new PartijAntwoordOpStelling()
            {
                Antwoord = "Eens",
                Stelling = st1,
                Partij = p6,
                Uitleg = "Wij zijn principieel gekant tegen het heffen van successierechten en belastingen op nalatenschappen. Gedurende heel hun samenstellingsproces werden deze immers al aan alle mogelijke vormen van belasting onderworpen. "
            };
            var pa7 = new PartijAntwoordOpStelling()
            {
                Antwoord = "Oneens",
                Stelling = st1,
                Partij = p7,
                Uitleg = "We pleiten voor een sterkere progressiviteit bij erfenisbelastingen, zodat de sterkste schouders effectief de zwaarste lasten dragen. We zorgen er ook voor dat miljonairs de successierechten niet langer kunnen ontwijken."
            };
            var pa8 = new PartijAntwoordOpStelling()
            {
                Antwoord = "Eens",
                Stelling = st2,
                Partij = p1,
                Uitleg = "We zijn eens omdat..."
            };
            var pa9 = new PartijAntwoordOpStelling()
            {
                Antwoord = "Oneens",
                Stelling = st2,
                Partij = p2,
                Uitleg = "De institutionele afspraken tussen Spanje en zijn deelstaat Catalonië worden in Spanje gemaakt. Wij hebben ook niet graag dat andere landen zich mengen in onze communautaire uitdagingen."
            };
            var pa10 = new PartijAntwoordOpStelling()
            {
                Antwoord = "Eens",
                Stelling = st2,
                Partij = p3,
                Uitleg = "De Catalaanse kwestie is een Spaanse aangelegenheid. Vlaanderen heeft daar geen rol te spelen. Vlaanderen wel kan aandringen op meer overleg en zich kan uitspreken tegen het gebruik van geweld langs twee kanten."
            };
            var pa11 = new PartijAntwoordOpStelling()
            {
                Antwoord = "Oneens",
                Stelling = st2,
                Partij = p4,
                Uitleg = "Elk volk heeft het recht zijn eigen toekomst te kiezen: we steunen dus onomwonden het zelfbeschikkingsrecht van de Catalanen en de vreedzame en democratische onafhankelijkheidsbeweging in Catalonië. We sporen de EU aan tot meer respect. "
            };
            var pa12 = new PartijAntwoordOpStelling()
            {
                Antwoord = "Eens",
                Stelling = st2,
                Partij = p5,
                Uitleg = "We roepen op tot een politieke dialoog om het conflict te ontmijnen, maar we mengen ons niet in een onafhankelijksdiscussie binnen andere Europese lidstaten. We zouden dat omgekeerd ook niet op prijs stellen."
            };
            var pa13 = new PartijAntwoordOpStelling()
            {
                Antwoord = "Eens",
                Stelling = st2,
                Partij = p6,
                Uitleg = "Vlaanderen moet geen buitenlandse conflicten importeren. We garanderen respect voor de politieke rechten, maar spreken ons over de zaak niet uit."
            };
            var pa14 = new PartijAntwoordOpStelling()
            {
                Antwoord = "Oneens",
                Stelling = st2,
                Partij = p7,
                Uitleg = "Onze partij steunt de Catalanen ten volle in hun streven naar een onafhankelijke en soevereine Catalaanse republiek en nodigt Vlaanderen uit het zelfbeschikkingsrecht van de Catalanen te respecteren en de Catalaanse republiek te erkennen."
            };
            context.Antwoorden.Add(pa1);
            context.Antwoorden.Add(pa2);
            context.Antwoorden.Add(pa3);
            context.Antwoorden.Add(pa4);
            context.Antwoorden.Add(pa5);
            context.Antwoorden.Add(pa6);
            context.Antwoorden.Add(pa7);
            context.Antwoorden.Add(pa8);
            context.Antwoorden.Add(pa9);
            context.Antwoorden.Add(pa10);
            context.Antwoorden.Add(pa11);
            context.Antwoorden.Add(pa12);
            context.Antwoorden.Add(pa13);
            context.Antwoorden.Add(pa14);

            context.SaveChanges();
            foreach (var entry in context.ChangeTracker.Entries().ToList())
            {
                entry.State = EntityState.Detached;
            }
        }
    }
}