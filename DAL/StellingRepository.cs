﻿// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo

using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ST.BL.Domain.Stellingen;
using ST.DAL.EF;
using ST.DAL.Interfaces;

namespace ST.DAL
{
    public class StellingRepository : IStellingRepository
    {
        private readonly TreeProjectDbContext _ctx;

        public StellingRepository(TreeProjectDbContext context)
        {
            _ctx = context;
        }

        public Stelling CreateStelling(Stelling stelling)
        {
           var s =  _ctx.Stellingen.Add(stelling);
            _ctx.SaveChanges();
            return s.Entity;
        }

        public IEnumerable<Stelling> ReadStellingen()
        {
            return _ctx.Stellingen
                .Include(s=>s.MoeilijkeWoorden)
                .Include(s=>s.PartijAntwoordOpStelling)
                .Include(s=>s.UserAntwoordOpStelling)
                .AsEnumerable();
        }

        public Stelling ReadStellingById(int id)
        {
            return ReadStellingen()
                .ToList()
                .Find(s => s.Id == id);
        }

        public int UpdateStelling(Stelling stelling)
        {
            _ctx.Stellingen.Update(stelling);
            return _ctx.SaveChanges();
        }

        public int DeleteStelling(Stelling stelling)
        {
            _ctx.Stellingen.Remove(stelling);
            return _ctx.SaveChanges();
        }
    }
}