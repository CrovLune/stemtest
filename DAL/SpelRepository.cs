// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo

using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using ST.BL.Domain.Spellen;
using ST.DAL.EF;
using ST.DAL.Interfaces;

namespace ST.DAL
{
    public class SpelRepository : ISpelRepository
    {
        private readonly TreeProjectDbContext _ctx;

        public SpelRepository(TreeProjectDbContext context)
        {
            _ctx = context;
        }

        public Spel CreateSpel(Spel spel)
        {
            var item = _ctx.Spellen.Add(spel);
            _ctx.SaveChanges();
            return item.Entity;
        }

        public IEnumerable<Spel> ReadSpellen()
        {
            return _ctx.Spellen
                .Include(s => s.options)
                .Include(s=>s.Creator)
                .ThenInclude(c=>c.School)
                .AsEnumerable();
        }

        public IEnumerable<Spel> ReadPartijSpellen()
        {
            return ReadSpellen().Where(s => s is PartijSpel).AsEnumerable();
        }

        public IEnumerable<Spel> ReadDebatSpellen()
        {
            return ReadSpellen().Where(s => s is DebatSpel).AsEnumerable();
        }

        public Spel ReadSpelById(int id)
        {
            return ReadSpellen().ToList().Find(s => s.SpelId == id);
        }

        public int UpdateSpel(Spel spel)
        {
            _ctx.Spellen.Update(spel);
            return _ctx.SaveChanges();
        }

        public int DeleteSpel(Spel spel)
        {
            _ctx.Spellen.Remove(spel);
            return _ctx.SaveChanges();
        }
    }
}