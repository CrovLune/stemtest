﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ST.BL.Domain.InternMediateClasses;
using ST.DAL.EF;
using ST.DAL.Interfaces;

namespace ST.DAL
{
    public class SpelStellingenRepository : ISpelStellingenRepository
    {
        private readonly TreeProjectDbContext _ctx;

        public SpelStellingenRepository(TreeProjectDbContext ctx)
        {
            _ctx = ctx;
        }

        public SpelStellingen CreateSpelStelling(SpelStellingen spelStellingen)
        {
            var item = _ctx.SpelStellingen.Add(spelStellingen);
            _ctx.SaveChanges();
            return item.Entity;
        }

        public IEnumerable<SpelStellingen> ReadSpelStellingen()
        {
            return _ctx.SpelStellingen
                .Include(s => s.Spel)
                .Include(s => s.Stelling)
                .AsEnumerable();
        }

        public int UpdateSpelStelling(SpelStellingen spelStellingen)
        {
            _ctx.SpelStellingen.Update(spelStellingen);
            return _ctx.SaveChanges();
        }

        public int DeleteSpelStelling(SpelStellingen spelStellingen)
        {
            _ctx.SpelStellingen.Remove(spelStellingen);
            return _ctx.SaveChanges();
        }

    }
}