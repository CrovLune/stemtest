﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ST.BL.Domain.Identity;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Domain.Users;
using ST.DAL.EF;
using ST.DAL.Interfaces;

namespace ST.DAL
{
    [SuppressMessage("ReSharper", "IdentifierTypo")]
    public class GebruikerKlassenRepository : IGebruikerKlassenRepository
    {
        private readonly TreeProjectDbContext _context;

        public GebruikerKlassenRepository(TreeProjectDbContext context)
        {
            _context = context;
        }


        public GebruikerKlassen CreateGebruikerKlassen(GebruikerKlassen gebruikerKlassen)
        {
            var item = _context.GebruikerKlasses.Add(gebruikerKlassen);
            _context.SaveChanges();
            return item.Entity;
        }

        public IEnumerable<GebruikerKlassen> ReadGebruikerKlassen()
        {
            return _context.GebruikerKlasses
                .Include(c => c.klas)
                .Include(c => c.Gebruiker)
                .AsEnumerable();
        }

        public int UpdateGebruikerKlassen(GebruikerKlassen gebruikerKlassen)
        {
            _context.GebruikerKlasses.Update(gebruikerKlassen);
            return _context.SaveChanges();
        }

        public int DeleteGebruikerKlassen(GebruikerKlassen gebruikerKlassen)
        {
            _context.GebruikerKlasses.Remove(gebruikerKlassen);
            return _context.SaveChanges();
        }
    }
}