using System.Collections.Generic;
using ST.BL.Domain.Partijen;
using ST.BL.Domain.Spellen;
using ST.BL.Domain.Users;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace ST.DAL.Interfaces
{
    public interface ISpelRepository
    {
        public Spel CreateSpel(Spel spel);
        public IEnumerable<Spel> ReadSpellen();
        public IEnumerable<Spel> ReadPartijSpellen();
        public IEnumerable<Spel> ReadDebatSpellen();
        public Spel ReadSpelById(int id);
        public int UpdateSpel(Spel spel);
        public int DeleteSpel(Spel spel);
    }
}