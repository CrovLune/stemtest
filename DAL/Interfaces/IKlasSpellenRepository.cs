﻿using System.Collections.Generic;
using ST.BL.Domain.Identity;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Domain.Users;

namespace ST.DAL.Interfaces
{
    public interface IKlasSpellenRepository
    {
        public KlasSpellen CreateKlasSpel(KlasSpellen klasSpel);
        public IEnumerable<KlasSpellen> ReadKlasSpellen();
        public int UpdateKlasSpel(KlasSpellen klasSpel);
        public int UpdateKlasSpelRange(IEnumerable<KlasSpellen> klasSpellen);
        public int DeleteKlasSpel(KlasSpellen klasSpel);
    }
}