﻿using System.Collections.Generic;
using ST.BL.Domain.InternMediateClasses;

// ReSharper disable IdentifierTypo

namespace ST.DAL.Interfaces
{
    public interface IPartijStellingenRepository
    {
        public PartijStellingen CreatePartijStelling(PartijStellingen partijStellingen);
        public IEnumerable<PartijStellingen> ReadPartijStellingen();
        public int UpdatePartijStellingen(PartijStellingen partijStellingen);
        public int DeletePartijStellingen(PartijStellingen partijStellingen);
    }
}