﻿using System.Collections.Generic;
using ST.BL.Domain.Sessies;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace ST.DAL.Interfaces
{
    public interface ISessieRepository
    {
        // Create Read Update Delete
        public Sessie CreateSessie(Sessie sessie);
        public IEnumerable<Sessie> ReadSessies();
        public Sessie ReadSessieById(int id);
        public int UpdateSessie(Sessie sessie);
        public int UpdateSessieRange(IEnumerable<Sessie> sessies);
        public int DeleteSessie(Sessie sessie);
    }
}