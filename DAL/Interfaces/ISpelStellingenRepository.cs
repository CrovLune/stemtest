﻿using System.Collections.Generic;
using ST.BL.Domain.InternMediateClasses;

namespace ST.DAL.Interfaces
{
    public interface ISpelStellingenRepository
    {
        public SpelStellingen CreateSpelStelling(SpelStellingen spelStellingen);
        public IEnumerable<SpelStellingen> ReadSpelStellingen();
        public int UpdateSpelStelling(SpelStellingen spelStellingen);
        public int DeleteSpelStelling(SpelStellingen spelStellingen);

    }
}