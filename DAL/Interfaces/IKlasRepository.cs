﻿using System.Collections.Generic;
using ST.BL.Domain.Scholen;
using ST.BL.Domain.Users;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace ST.DAL.Interfaces
{
    public interface IKlasRepository
    {
        // Create Read Update Delete
        public Klas CreateKlas(Klas klas);
        public IEnumerable<Klas> ReadKlassen();
        public Klas ReadKlasById(int id);
        public int UpdateKlas(Klas klas);
        public int DeleteKlas(Klas klas);
    }
}