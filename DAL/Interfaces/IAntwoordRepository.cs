﻿using System.Collections.Generic;
using ST.BL.Domain.Antwoorden;

namespace ST.DAL.Interfaces
{
    public interface IAntwoordRepository
    {
        public Antwoord CreateAntwoord(Antwoord antwoord);
        public IEnumerable<Antwoord> ReadAntwoorden();
        public IEnumerable<Antwoord> ReadPartijAntwoorden();
        public IEnumerable<Antwoord> ReadUserAntwoorden();
        public int UpdateAntwoord(Antwoord antwoord);
        public int UpdateAntwoordRange(List<Antwoord> antwoorden);

        public int DeleteAntwoord(Antwoord antwoord);
    }
}