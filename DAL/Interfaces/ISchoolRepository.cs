using System.Collections.Generic;
using ST.BL.Domain.Scholen;
using ST.BL.Domain.Users;
// ReSharper disable IdentifierTypo

namespace ST.DAL.Interfaces
{
    public interface ISchoolRepository
    {
        // Create Read Update Delete
        public School CreateSchool(School school);
        public IEnumerable<School> ReadScholen();
        public School ReadSchoolById(int id);
        public int UpdateSchool(School school);
        public int DeleteSchool(School school);
        
    }
}