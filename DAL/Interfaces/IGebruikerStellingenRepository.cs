﻿using System.Collections.Generic;
using ST.BL.Domain.InternMediateClasses;

namespace ST.DAL.Interfaces
{
    public interface IGebruikerStellingenRepository
    {
        public GebruikerStellingen CreateGebruikerStelling(GebruikerStellingen gebruikerStelling);
        public IEnumerable<GebruikerStellingen> ReadGebruikerStellingen();
        public int UpdateGebruikerStelling(GebruikerStellingen gebruikerStelling);
        public int UpdateGebruikerStellingRange(IEnumerable<GebruikerStellingen> gebruikerStellingen);

        public int DeleteGebruikerStelling(GebruikerStellingen gebruikerStelling);
        public int DeleteGebruikerStellingRange(IEnumerable<GebruikerStellingen> gebruikerStellingen);

    }
}