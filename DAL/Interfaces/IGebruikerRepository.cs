﻿using System.Collections.Generic;
using ST.BL.Domain.Identity;
using ST.BL.Domain.Users;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace ST.DAL.Interfaces
{
    public interface IGebruikerRepository
    {
        // Create Read Update Delete
        public Gebruiker CreateGebruiker(Gebruiker gebruiker);
        public IEnumerable<Gebruiker> ReadGebruikers();
        public Gebruiker ReadGebruikerByIdentity(ApplicationUser user);
        public Gebruiker ReadGebruikerById(int id);
        public int UpdateGebruiker(Gebruiker gebruiker);
        public int DeleteGebruiker(Gebruiker gebruiker);
    }
}