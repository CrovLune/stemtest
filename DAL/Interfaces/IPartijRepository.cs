﻿using System.Collections.Generic;
using ST.BL.Domain.Partijen;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace ST.DAL.Interfaces
{
    public interface IPartijRepository
    {
        // Create Read Update Delete
        public Partij CreatePartij(Partij partij);
        public IEnumerable<Partij> ReadPartijen();
        public Partij ReadPartijById(int id);
        public int UpdatePartij(Partij partij);
        public int DeletePartij(Partij partij);
    }
}