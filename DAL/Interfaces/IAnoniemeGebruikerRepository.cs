﻿using System.Collections.Generic;
using ST.BL.Domain.Users;

namespace ST.DAL.Interfaces
{
    public interface IAnoniemeGebruikerRepository
    {
        //CRUD
        public AnoniemeGebruiker CreateAnoniemeGebruiker(AnoniemeGebruiker anoniemeGebruiker);
        public AnoniemeGebruiker ReadAnoniemeGebruikerById(int id);
        public IEnumerable<AnoniemeGebruiker> ReadAnoniemeGebruikers();
        public int UpdateAnoniemeGebruiker(AnoniemeGebruiker anoniemeGebruiker);
        public int UpdateAnoniemeGebruikersRange(List<AnoniemeGebruiker> anoniemeGebruikers);
        public int DeleteAnoniemeGebruiker(AnoniemeGebruiker anoniemeGebruiker);
        public int DeleteAnoniemeGebruikesRange(List<AnoniemeGebruiker> anoniemeGebruikers);

    }
}