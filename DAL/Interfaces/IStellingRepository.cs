﻿using System.Collections.Generic;
using ST.BL.Domain.Stellingen;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo

namespace ST.DAL.Interfaces
{
    public interface IStellingRepository
    {
        // Create Read Update Delete
        public Stelling CreateStelling(Stelling stelling);
        public IEnumerable<Stelling> ReadStellingen();
        public Stelling ReadStellingById(int id);
        public int UpdateStelling(Stelling stelling);
        public int DeleteStelling(Stelling stelling);
    }
}