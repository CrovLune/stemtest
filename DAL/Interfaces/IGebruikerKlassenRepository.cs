﻿using System.Collections.Generic;
using ST.BL.Domain.Identity;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Domain.Users;
// ReSharper disable IdentifierTypo

namespace ST.DAL.Interfaces
{
    public interface IGebruikerKlassenRepository
    {
        // Create Read Update Delete
        public GebruikerKlassen CreateGebruikerKlassen(GebruikerKlassen gebruikerKlassen);
        public IEnumerable<GebruikerKlassen> ReadGebruikerKlassen();

        
        public int UpdateGebruikerKlassen(GebruikerKlassen gebruikerKlassen);
        public int DeleteGebruikerKlassen(GebruikerKlassen gebruikerKlassen);
    }
}