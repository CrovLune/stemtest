﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ST.BL.Domain.Identity;
using ST.BL.Domain.Users;
using ST.DAL.EF;
using ST.DAL.Interfaces;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace ST.DAL
{
    public class GebruikerRepository : IGebruikerRepository
    {
        private readonly TreeProjectDbContext _ctx;

        public GebruikerRepository(TreeProjectDbContext context)
        {
            _ctx = context;
        }

        public Gebruiker CreateGebruiker(Gebruiker gebruiker)
        {
            var item = _ctx.Gebruikers.Add(gebruiker);
            _ctx.SaveChanges();
            return item.Entity;
        }

        public IEnumerable<Gebruiker> ReadGebruikers()
        {
            return _ctx.Gebruikers
                .Include(g => g.Identity)
                .Include(g => g.School)
                .AsEnumerable();
        }

        public Gebruiker ReadGebruikerByIdentity(ApplicationUser user)
        {
            return ReadGebruikers()
                .ToList()
                .Find(g => g.Email.ToLower().Equals(user.UserName.ToLower()));
        }

        public Gebruiker ReadGebruikerById(int gebruikerId)
        {
            return ReadGebruikers()
                .ToList()
                .Find(g => g.Id == gebruikerId);
        }

        public int UpdateGebruiker(Gebruiker gebruiker)
        {
            _ctx.Gebruikers.Update(gebruiker);
            return _ctx.SaveChanges();
        }

        public int DeleteGebruiker(Gebruiker gebruiker)
        {
            _ctx.Gebruikers.Remove(gebruiker);
            return _ctx.SaveChanges();
        }
    }
}