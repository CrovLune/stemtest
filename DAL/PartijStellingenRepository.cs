﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ST.BL.Domain.InternMediateClasses;
using ST.DAL.EF;
using ST.DAL.Interfaces;

// ReSharper disable IdentifierTypo

namespace ST.DAL
{
    public class PartijStellingenRepository : IPartijStellingenRepository
    {
        private readonly TreeProjectDbContext _ctx;

        public PartijStellingenRepository(TreeProjectDbContext context)
        {
            _ctx = context;
        }

        public PartijStellingen CreatePartijStelling(PartijStellingen partijStellingen)
        {
            var item = _ctx.PartijStellingen.Add(partijStellingen);
            _ctx.SaveChanges();
            return item.Entity;
        }

        public IEnumerable<PartijStellingen> ReadPartijStellingen()
        {
            return _ctx.PartijStellingen
                .Include(p => p.Partij)
                .Include(p => p.Stelling)
                .AsEnumerable();
        }


        public int UpdatePartijStellingen(PartijStellingen partijStellingen)
        {
            _ctx.PartijStellingen.Update(partijStellingen);
            return _ctx.SaveChanges();
        }

        public int DeletePartijStellingen(PartijStellingen partijStellingen)
        {
            _ctx.PartijStellingen.Remove(partijStellingen);
            return _ctx.SaveChanges();
        }
    }
}