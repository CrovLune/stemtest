﻿using TechTalk.SpecFlow;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace Testen.GebruikerWilResultaatZien
{
    [Binding]
    public class GebruikerWilResultaatZienSteps
    {
        private ScenarioContext _scenarioContext;
        
        public GebruikerWilResultaatZienSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }
        
        [Given(@"Leerkracht heeft op “Bekijk Resultaten” gedrukt")]
        public void GivenLeerkrachtHeeftOpBekijkResultatenGedrukt()
        {
            _scenarioContext.Pending();
        }
        
        [Given(@"Leerkracht heeft nog niet op “Bekijk Resultaten” gedrukt")]
        public void GivenLeerkrachtHeeftNogNietOpBekijkResultatenGedrukt()
        {
            _scenarioContext.Pending();
        }
        
        [When(@"Gebruiker (.*) wilt zijn resultaten bekijken")]
        public void WhenGebruikerWiltZijnResultatenBekijken(int p0)
        {
            _scenarioContext.Pending();
        }
        
        [When(@"Gebruiker (.*) heeft al de stellingen beantwoord")]
        public void WhenGebruikerHeeftAlDeStellingenBeantwoord(int p0)
        {
            _scenarioContext.Pending();
        }
        
        [Then(@"Gebruiker (.*) krijgt zijn persoonlijk resultaat te zien")]
        public void ThenGebruikerKrijgtZijnPersoonlijkResultaatTeZien(int p0)
        {
            _scenarioContext.Pending();
        }
        
        [Then(@"Gebruiker (.*) ziet laatste stelling tot leerkracht resultaten toont")]
        public void ThenGebruikerZietLaatsteStellingTotLeerkrachtResultatenToont(int p0)
        {
            _scenarioContext.Pending();
        }
    }
}
