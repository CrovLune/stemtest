﻿Feature: GebruikerWilResultaatZien
Als gebruiker
Wil ik mijn resultaat kunnen zien
Zodat ik weet hoe de test verlopen is

@SuccesvolScenario
Scenario: Gebruiker bekijkt resultaten
	Given Leerkracht heeft op “Bekijk Resultaten” gedrukt
	When Gebruiker 3 wilt zijn resultaten bekijken
	And Gebruiker 3 heeft al de stellingen beantwoord
	Then Gebruiker 3 krijgt zijn persoonlijk resultaat te zien

@LeerkrachtHeeftNogNietResultatenBekeken
Scenario: Leerkracht heeft nog niet de resultaten bekeken
	Given Leerkracht heeft nog niet op “Bekijk Resultaten” gedrukt
	When Gebruiker 3 wilt zijn resultaten bekijken
	Then Gebruiker 3 ziet laatste stelling tot leerkracht resultaten toont