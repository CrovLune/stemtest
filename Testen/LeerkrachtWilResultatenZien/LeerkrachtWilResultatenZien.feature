﻿Feature: LeerkrachtWilResultatenZien
Als leerkracht
Wil ik de resultaten van de klas zien
Zodat ik hierop kan inspelen voor een debat te voeren

@SuccesvolScenario
Scenario: Leerkracht bekijkt resultaten
	Given er is een actieve test met gebruikers die alle stellingen hebben doorlopen
	When Dominique wilt de resultaten bekijken
	And Dominique drukt op “Bekijk Resultaten”
	Then Dominique krijgt de resultaten te zien

@NietIedereenKlaar
Scenario: Niet alle studenten zijn klaar
	Given er is een actieve test met gebruikers die nog niet alle stellingen hebben doorlopen
	When Dominique wilt de resultaten bekijken
	And Dominique drukt op “Bekijk Resultaten”
	Then  Dominique ziet de resultaten van de gebruikers die klaar zijn en de aantal gebruikers die klaar zijn. De resultaten worden aangepast wanneer een gebruiker alle stellingen heeft ingevuld.