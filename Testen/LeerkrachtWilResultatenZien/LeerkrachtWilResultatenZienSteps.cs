﻿using TechTalk.SpecFlow;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace Testen.LeerkrachtWilResultatenZien
{
    [Binding]
    public class LeerkrachtWilResultatenZienSteps
    {
        private ScenarioContext _scenarioContext;
        
        public LeerkrachtWilResultatenZienSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }
        
        [Given(@"er is een actieve test met gebruikers die alle stellingen hebben doorlopen")]
        public void GivenErIsEenActieveTestMetGebruikersDieAlleStellingenHebbenDoorlopen()
        {
            _scenarioContext.Pending();
        }
        
        [Given(@"er is een actieve test met gebruikers die nog niet alle stellingen hebben doorlopen")]
        public void GivenErIsEenActieveTestMetGebruikersDieNogNietAlleStellingenHebbenDoorlopen()
        {    _scenarioContext.Pending();
        }
        
        [When(@"Dominique wilt de resultaten bekijken")]
        public void WhenDominiqueWiltDeResultatenBekijken()
        {
            _scenarioContext.Pending();        }
        
        [When(@"Dominique drukt op “Bekijk Resultaten”")]
        public void WhenDominiqueDruktOpBekijkResultaten()
        {
            _scenarioContext.Pending();        }
        
        [Then(@"Dominique krijgt de resultaten te zien")]
        public void ThenDominiqueKrijgtDeResultatenTeZien()
        {
            _scenarioContext.Pending();        }
        
        [Then(@"Dominique ziet de resultaten van de gebruikers die klaar zijn en de aantal gebruikers die klaar zijn\. De resultaten worden aangepast wanneer een gebruiker alle stellingen heeft ingevuld\.")]
        public void ThenDominiqueZietDeResultatenVanDeGebruikersDieKlaarZijnEnDeAantalGebruikersDieKlaarZijn_DeResultatenWordenAangepastWanneerEenGebruikerAlleStellingenHeeftIngevuld_()
        {
            _scenarioContext.Pending();        }
    }
}
