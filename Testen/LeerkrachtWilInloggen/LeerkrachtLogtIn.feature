﻿Feature: LeerkrachtLogtIn
	Als Leerkracht
	Wil ik op de inlogpagina kunnen inloggen
	Zodat ik mijn testen en klassen kan beheren

@succesVolScenario
Scenario: Succesvolle login
	Given Dominique heeft account
	When  Dominique vult inloggegevens in
	And Dominique drukt op inloggen
	Then Dominique is ingelogd

@FoutieveGegevens
Scenario: Foutieve login gegevens
	Given Dominique heeft account
	When  Dominique vult verkeerde inloggegevens in
	And Dominique drukt op inloggen
	Then Dominique krijgt foutmelding

@FoutieveGegevens
Scenario: Onbestaand account
	Given Dominique heeft account
	When  Dominique vult onbestaande inloggegevens in
	And Dominique drukt op inloggen
	Then Dominique krijgt melding om account aan te maken