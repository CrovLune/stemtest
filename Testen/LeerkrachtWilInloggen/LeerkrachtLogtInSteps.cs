﻿using TechTalk.SpecFlow;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace Testen.LeerkrachtWilInloggen
{
    [Binding]
    public class LeerkrachtLogtInSteps
    {
        private ScenarioContext _scenarioContext;
        
        public LeerkrachtLogtInSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }


        [Given(@"Dominique heeft account")]
        public void GivenDominiqueHeeftAccount()
        {
            _scenarioContext.Pending();
        }

        [When(@"Dominique vult inloggegevens in")]
        public void WhenDominiqueVultInloggegevensIn()
        {
            _scenarioContext.Pending();
        }

        [When(@"Dominique drukt op inloggen")]
        public void WhenDominiqueDruktOpInloggen()
        {
            _scenarioContext.Pending();
        }

        [When(@"Dominique vult verkeerde inloggegevens in")]
        public void WhenDominiqueVultVerkeerdeInloggegevensIn()
        {
            _scenarioContext.Pending();
        }

        [When(@"Dominique vult onbestaande inloggegevens in")]
        public void WhenDominiqueVultOnbestaandeInloggegevensIn()
        {
            _scenarioContext.Pending();
        }

        [Then(@"Dominique is ingelogd")]
        public void ThenDominiqueIsIngelogd()
        {
            _scenarioContext.Pending();
        }

        [Then(@"Dominique krijgt foutmelding")]
        public void ThenDominiqueKrijgtFoutmelding()
        {
            _scenarioContext.Pending();
        }

        [Then(@"Dominique krijgt melding om account aan te maken")]
        public void ThenDominiqueKrijgtMeldingOmAccountAanTeMaken()
        {
            _scenarioContext.Pending();
        }
    }
}