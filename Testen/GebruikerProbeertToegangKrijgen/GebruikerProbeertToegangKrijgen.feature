﻿Feature: GebruikerProbeertToegangKrijgen
Als	gebruiker
Wil Ik de sessie kunnen joinen
Zodat ik kan meedoen aan een partijspel

@SuccesvolScenario
Scenario: Toegang wordt verleend
	Given Gebruiker 1 heeft een toegangsmethode gekregen van de leerkracht
	When Gebruiker 1 probeert via een van de methoden toegang te verkrijgen
	Then Gebruiker 1 krijgt toegang en joint de sessie

@ToegangWordtNietVerleend
Scenario: Toegang wordt niet verleend
	Given Gebruiker 1 heeft een toegangsmethode gekregen van de leerkracht
	When Gebruiker 1 probeert via een van de methoden toegang te verkrijgen
	And Gebruiker 1 vult foutieve code in of de connectie faalt
	Then Gebruiker 1 wordt naar de homepagina geleid en kan hier opnieuw een code invullen