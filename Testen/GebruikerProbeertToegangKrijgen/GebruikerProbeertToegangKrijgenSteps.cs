﻿using TechTalk.SpecFlow;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace Testen.GebruikerProbeertToegangKrijgen
{
    [Binding]
    public class GebruikerProbeertToegangKrijgenSteps
    {

        private ScenarioContext _scenarioContext;

        public GebruikerProbeertToegangKrijgenSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        [Given(@"Gebruiker (.*) heeft een toegangsmethode gekregen van de leerkracht")]
        public void GivenGebruikerHeeftEenToegangsmethodeGekregenVanDeLeerkracht(int p0)
        {
            _scenarioContext.Pending();
        }
        
        [When(@"Gebruiker (.*) probeert via een van de methoden toegang te verkrijgen")]
        public void WhenGebruikerProbeertViaEenVanDeMethodenToegangTeVerkrijgen(int p0)
        {
            _scenarioContext.Pending();
        }
        
        [When(@"Gebruiker (.*) vult foutieve code in of de connectie faalt")]
        public void WhenGebruikerVultFoutieveCodeInOfDeConnectieFaalt(int p0)
        {
            _scenarioContext.Pending();
        }
        
        [Then(@"Gebruiker (.*) krijgt toegang en joint de sessie")]
        public void ThenGebruikerKrijgtToegangEnJointDeSessie(int p0)
        {
            _scenarioContext.Pending();
        }
        
        [Then(@"Gebruiker (.*) wordt naar de homepagina geleid en kan hier opnieuw een code invullen")]
        public void ThenGebruikerWordtNaarDeHomepaginaGeleidEnKanHierOpnieuwEenCodeInvullen(int p0)
        {
            _scenarioContext.Pending();
        }
    }
}
