﻿Feature: LeerkrachtWilTestStarten
Als leerkracht
Wil ik een test opstarten
Zodat de klas kan beginnen met antwoorden

@SuccesvolScenario
Scenario: Leerkracht start de test op
	Given er is een bestaande sessie en er zijn gebruikers in de sessie
	When Dominique wilt de test starten
	And Dominique duwt op “Start”
	Then de test begint

@GeenGebruikersInSessie
Scenario: Er zijn geen gebruikers in de sessie
	Given er is een bestaande sessie en er zijn geen gebruikers in de sessie
	When Dominique wilt de test starten
	Then Dominique kan niet op “Start” duwen
