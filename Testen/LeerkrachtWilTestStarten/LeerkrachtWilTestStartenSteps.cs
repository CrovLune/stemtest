﻿using TechTalk.SpecFlow;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace Testen.LeerkrachtWilTestStarten
{
    [Binding]
    public class LeerkrachtWilTestStartenSteps
    {
        private ScenarioContext _scenarioContext;
        
        public LeerkrachtWilTestStartenSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        [Given(@"er is een bestaande sessie en er zijn gebruikers in de sessie")]
        public void GivenErIsEenBestaandeSessieEnErZijnGebruikersInDeSessie()
        {
            _scenarioContext.Pending();
        }

        [Given(@"er is een bestaande sessie en er zijn geen gebruikers in de sessie")]
        public void GivenErIsEenBestaandeSessieEnErZijnGeenGebruikersInDeSessie()
        {
            _scenarioContext.Pending();
        }

        [When(@"Dominique wilt de test starten")]
        public void WhenDominiqueWiltDeTestStarten()
        {
            _scenarioContext.Pending();
        }

        [When(@"Dominique duwt op “Start”")]
        public void WhenDominiqueDuwtOpStart()
        {
            _scenarioContext.Pending();
        }

        [Then(@"de test begint")]
        public void ThenDeTestBegint()
        {
            _scenarioContext.Pending();
        }

        [Then(@"Dominique kan niet op “Start” duwen")]
        public void ThenDominiqueKanNietOpStartDuwen()
        {
            _scenarioContext.Pending();
        }
    }
}