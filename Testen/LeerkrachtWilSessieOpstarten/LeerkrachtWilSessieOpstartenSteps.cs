﻿using ST.BL;
using ST.BL.Domain.Scholen;
using ST.BL.Domain.Users;
using TechTalk.SpecFlow;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace Testen.LeerkrachtWilSessieOpstarten
{
    [Binding]
    public class LeerkrachtWilSessieOpstartenSteps
    {
        private ScenarioContext _scenarioContext;
    

        public LeerkrachtWilSessieOpstartenSteps(ScenarioContext scenarioContext)
        {
         _scenarioContext = scenarioContext;
  
        }

        [Given(@"Dominique heeft een klas geselecteerd")]
        public void GivenDominiqueHeeftEenKlasGeselecteerd()
        {
            _scenarioContext.Pending();

        }

        [Given(@"Dominique heeft geen klas geselecteerd")]
        public void GivenDominiqueHeeftGeenKlasGeselecteerd()
        {
            _scenarioContext.Pending();
        }

        [Given(@"Dominique heeft een klas geselecteerd en sessie opgestart")]
        public void GivenDominiqueHeeftEenKlasGeselecteerdEnSessieOpgestart()
        {
            _scenarioContext.Pending();

        }

        [When(@"Dominique wilt een sessie starten")]
        public void WhenDominiqueWiltEenSessieStarten()
        {
            _scenarioContext.Pending();
        }

        [When(@"Dominique drukt op de start knop")]
        public void WhenDominiqueDruktOpDeStartKnop()
        {
            _scenarioContext.Pending();
        }

        [When(@"Dominique wilt een tweede sessie starten")]
        public void WhenDominiqueWiltEenTweedeSessieStarten()
        {
            _scenarioContext.Pending();
        }

        [Then(@"sessie wordt opgestart")]
        public void ThenSessieWordtOpgestart()
        {
            _scenarioContext.Pending();

            
        }

        [Then(@"sessie kan niet worden opgestart")]
        public void ThenDominiqueKanGeenSessieOpstarten()
        {
            _scenarioContext.Pending();
        }

        [Then(@"Dominique kan geen tweede sessie niet starten")]
        public void ThenDominiqueKanGeenTweedeSessieNietStarten()
        {
            _scenarioContext.Pending();
        }
    }
}