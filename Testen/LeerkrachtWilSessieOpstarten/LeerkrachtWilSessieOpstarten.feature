﻿Feature: LeerkrachtWilSessieOpstarten
Als	leerkracht
Wil Ik een sessie opstarten
Zodat de klas de sessie kunnen joinen

@SuccesvolScenario
Scenario: Sessie wordt opgestart
	Given Dominique heeft een klas geselecteerd
	When Dominique wilt een sessie starten
	And Dominique drukt op de start knop 
	Then sessie wordt opgestart

@SessieKanNietWordenOpgestart
Scenario: Sessie kan niet worden opgestart
	Given Dominique heeft geen klas geselecteerd
	When Dominique wilt een sessie starten
	Then sessie wordt niet opgestart

@ErIsAlEenSessieBezig
Scenario: Er is al een sessie bezig
	Given Dominique heeft een klas geselecteerd en sessie opgestart
	When Dominique wilt een tweede sessie starten
	Then Dominique kan geen tweede sessie niet starten
