﻿Feature: LeerkrachtWilSessieStoppen
Als	leerkracht
Wil ik sessie stoppen
Zodat nieuwe sessie gestart kan worden

@SuccesvolScenario
Scenario: Leerkracht beëindigt sessie 
	Given Dominique heeft een actieve sessie
	When Dominique wilt deze sessie beëindigen
	Then Dominique drukt op “Beëindig deze sessie”
