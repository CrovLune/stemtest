﻿using TechTalk.SpecFlow;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace Testen.LeerkrachtWilSessieStoppen
{
    [Binding]
    public class LeerkrachtWilSessieStoppenSteps
    {
        private ScenarioContext _scenarioContext;
        
        public LeerkrachtWilSessieStoppenSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        
        [Given(@"Dominique heeft een actieve sessie")]
        public void GivenDominiqueHeeftEenActieveSessie()
        {
            _scenarioContext.Pending();
        }
        
        [When(@"Dominique wilt deze sessie beëindigen")]
        public void WhenDominiqueWiltDezeSessieBeeindigen()
        {
            _scenarioContext.Pending();
        }
        
        [Then(@"Dominique drukt op “Beëindig deze sessie”")]
        public void ThenDominiqueDruktOpBeeindigDezeSessie()
        {
            _scenarioContext.Pending();
        }
    }
}
