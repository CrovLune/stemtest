﻿Feature: LeerkrachtWilLeerlingenToegangGevenTotTest
Als	leerkracht
Wil Ik een test opstarten
Zodat de klas kan beginnen met antwoorden

@SuccesvolScenario
Scenario: Codes worden getoond
	Given Dominique heeft een sessie opgestart
	When Dominique wilt test starten
	Then qr-code en unieke code worden getoond

@CodesKunnenNietGetoondWorden
Scenario: Codes kunnen niet getoond worden
	Given Dominique heeft een sessie opgestart
	When Dominique wilt test starten
	Then Dominique kan geen codes tonen, er is een foutmelding

@LeerkrachtVerstuurtLink
Scenario: Leerkracht verstuurd link naar leerlingen
	Given Dominique heeft een sessie opgestart
	When Dominique wilt link versturen naar leerlingen
	Then de link wordt aangemaakt en Dominique kan deze kopiëren en vervolgens verspreiden

@LinkWordtNietAangemaakt
Scenario: Link wordt niet aangemaakt
	Given Dominique heeft een sessie opgestart
	When Dominique wilt link versturen naar leerlingen
	Then de link kan niet worden aangemaakt