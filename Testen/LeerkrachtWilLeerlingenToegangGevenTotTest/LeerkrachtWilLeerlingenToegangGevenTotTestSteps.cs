﻿using TechTalk.SpecFlow;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace Testen.LeerkrachtWilLeerlingenToegangGevenTotTest
{
    [Binding]
    public class LeerkrachtWilLeerlingenToegangGevenTotTestSteps
    {
        private ScenarioContext _scenarioContext;
        
        public LeerkrachtWilLeerlingenToegangGevenTotTestSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }
        
        [Given(@"Dominique heeft een sessie opgestart")]
        public void GivenDominiqueHeeftEenSessieOpgestart()
        {
            _scenarioContext.Pending();
        }
        
        [When(@"Dominique wilt test starten")]
        public void WhenDominiqueWiltTestStarten()
        {
            _scenarioContext.Pending();
        }
        
        [When(@"Dominique wilt link versturen naar leerlingen")]
        public void WhenDominiqueWiltLinkVersturenNaarLeerlingen()
        {
            _scenarioContext.Pending();
        }
        
        [Then(@"qr-code en unieke code worden getoond")]
        public void ThenQr_CodeEnUniekeCodeWordenGetoond()
        {
            _scenarioContext.Pending();
        }
        
        [Then(@"Dominique kan geen codes tonen, er is een foutmelding")]
        public void ThenDominiqueKanGeenCodesTonenErIsEenFoutmelding()
        {
            _scenarioContext.Pending();
        }
        
        [Then(@"de link wordt aangemaakt en Dominique kan deze kopiëren en vervolgens verspreiden")]
        public void ThenDeLinkWordtAangemaaktEnDominiqueKanDezeKopierenEnVervolgensVerspreiden()
        {
            _scenarioContext.Pending();
        }
        
        [Then(@"de link kan niet worden aangemaakt")]
        public void ThenDeLinkKanNietWordenAangemaakt()
        {
            _scenarioContext.Pending();
        }
    }
}
