﻿Feature: LeerkrachtWilKlasSelecteren
	Als Leerkracht 
	Wil ik een klas kunnen selecteren
	Zodat ik een sessie kan aanmaken

@SuccesvolScenario
Scenario: Klas wordt geselecteerd
	Given Dominique heeft een klas INF201A
	When Dominique wilt een sessie starten in INF201A
	And Dominique drukt op “INF201A” in het overzicht
	Then is INF201A geselecteerd

@KlasIsNogNietAangemaakt
Scenario: Klas is nog niet aangemaakt
	Given Dominique heeft geen klas
	When Dominique wil een sessie starten
	Then Dominique wordt gevraagd om een klas aan te maken

