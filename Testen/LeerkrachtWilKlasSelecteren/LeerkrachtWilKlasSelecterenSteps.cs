﻿using ST.BL;
using ST.BL.Domain.Scholen;
using ST.BL.Domain.Users;
using TechTalk.SpecFlow;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace Testen.LeerkrachtWilKlasSelecteren
{
    [Binding]
    public class LeerkrachtWilKlasSelecterenSteps
    {
        private ScenarioContext _scenarioContext;
        private KlasManager _klasManager;

        private Gebruiker _leerkracht;
        private Klas _klas;
        private SessieManager _sessieManager;
        // private SpelSessie _spelSessie;
        // private PartijSpel _partijSpel;
        // private TestManager _testManager;


        public LeerkrachtWilKlasSelecterenSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
           /* _klasManager = new KlasManager();
            var school = new School
            {
                Naam = "KdG Antwerpen", Adres = "Groenplaats, Antwerpen", MailContactpersoon = "kdg@support.be"
            };
            _leerkracht = new GeregistreerdeGebruiker
            {
                Voornaam = "Dominique", Achternaam = "Burssens", Username = "dominique.burssens",
                Wachtwoord = "azerty123", Email = "Dominique.Burssens@kdg.be", School = school
            };
            _klas = _klasManager.CreateKlas("INF201A", _leerkracht);
            _spelSessieManager = new SpelSessieManager();
            _testManager = new TestManager();*/
        }

        [Given(@"Dominique heeft een klas INF(.*)A")]
        public void GivenDominiqueHeeftEenKlasINFA()
        {  
        //  Assert.IsTrue(_leerkracht.Klassen.Contains(_klas));
        _scenarioContext.Pending();

        }

        [Given(@"Dominique heeft geen klas")]
        public void GivenDominiqueHeeftGeenKlas()
        {
        //    Assert.IsFalse(_leerkracht.Klassen.Contains(_klas));
        _scenarioContext.Pending();

        }

        [When(@"Dominique wilt een sessie starten in INF(.*)A")]
        public void WhenDominiqueWiltEenSessieStartenInINFA(int p0)
        {
            //_spelSessie = _sessieManager.CreateSpelSessie(_klas);
            _scenarioContext.Pending();

        }

        [When(@"Dominique drukt op “INF(.*)A” in het overzicht")]
        public void WhenDominiqueDruktOpINFAInHetOverzicht(int p0)
        {
            _scenarioContext.Pending();
        }

        [When(@"Dominique wil een sessie starten")]
        public void WhenDominiqueWilEenSessieStarten()
        {
            _scenarioContext.Pending();
        }

        [Then(@"is INF(.*)A geselecteerd")]
        public void ThenIsINFAGeselecteerd(int p0)
        {
      /*      _partijSpel = _testManager.CreatePartijSpel(1,_leerkracht);
            _spelSessieManager.StartSpelSessie(_spelSessie, _partijSpel);
            Assert.IsTrue(_spelSessie.Status.Equals(Status.Open));*/
      _scenarioContext.Pending();

        }

        [Then(@"Dominique wordt gevraagd om een klas aan te maken")]
        public void ThenDominiqueWordtGevraagdOmEenKlasAanTeMaken()
        {
            _scenarioContext.Pending();
        }
    }
}