﻿using ST.BL;
using ST.BL.Domain.Partijen;
using ST.BL.Domain.Scholen;
// using ST.BL.Domain.SpelSessies.Testen.Spellen;
using ST.BL.Domain.Users;
using TechTalk.SpecFlow;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;


// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace Testen.GebruikerWilPartijKiezen
{
    [Binding]
    public class GebruikerWilPartijKiezenSteps
    {
        private ScenarioContext _scenarioContext;
    

        public GebruikerWilPartijKiezenSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;

        }

        [Given(@"Gebruiker (.*) zit in een sessie")]
        public void GivenGebruikerZitInEenSessie()
        {
            _scenarioContext.Pending();
        }

        [When(@"Gebruiker (.*) wilt “SPA” kiezen")]
        public void WhenGebruikerWiltSpaKiezen(int p0)
        {
            _scenarioContext.Pending();
        }

        [When(@"Gebruiker (.*) drukt op “SPA”")]
        public void WhenGebruikerDruktOpSpa(int p0)
        {
            _scenarioContext.Pending();
        }

        [When(@"Gebruiker (.*) drukt op “NVA”")]
        public void WhenGebruikerDruktOpNva(int p0)
        {
            _scenarioContext.Pending();
        }

        [Then(@"Gebruiker (.*) behoort nu tot “SPA”")]
        public void ThenGebruikerBehoortNuTotSpa(int p0)
        {
            _scenarioContext.Pending();
        }

        [Then(@"Gebruiker (.*) kan gewoon op “SPA” drukken als leerkracht de test nog niet gestart heeft")]
        public void ThenGebruikerKanGewoonOpSpaDrukkenAlsLeerkrachtDeTestNogNietGestartHeeft(int p0)
        {
            _scenarioContext.Pending();
        }
    }
}