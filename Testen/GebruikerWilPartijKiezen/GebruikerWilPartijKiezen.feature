﻿Feature: GebruikerWilPartijKiezen
Als	gebruiker
Wil ik bij een partijspel een partij kiezen
Zodat ik voor die bepaalde partij het partijspel kan spelen

@SuccesvolScenario
Scenario: Gebruiker kiest een partij
	Given Gebruiker 2 zit in een sessie
	When Gebruiker 2 wilt “SPA” kiezen 
	And Gebruiker 2 drukt op “SPA” 
	Then Gebruiker 2 behoort nu tot “SPA”

@GebruikerKiestVerkeerdePartij
Scenario: Gebruiker kiest verkeerde partij
	Given Gebruiker 2 zit in een sessie
	When Gebruiker 2 wilt “SPA” kiezen 
	And Gebruiker 2 drukt op “NVA” 
	Then Gebruiker 2 kan gewoon op “SPA” drukken als leerkracht de test nog niet gestart heeft