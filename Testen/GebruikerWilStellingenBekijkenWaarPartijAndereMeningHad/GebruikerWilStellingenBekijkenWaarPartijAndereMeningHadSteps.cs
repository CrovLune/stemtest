﻿using ST.BL;
using ST.BL.Domain.Partijen;
using ST.BL.Domain.Scholen;
using ST.BL.Domain.Users;
using TechTalk.SpecFlow;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace Testen.GebruikerWilStellingenBekijkenWaarPartijAndereMeningHad
{
    [Binding]
    public class GebruikerWilStellingenBekijkenWaarPartijAndereMeningHadSteps
    {
        private ScenarioContext _scenarioContext;
        
        public GebruikerWilStellingenBekijkenWaarPartijAndereMeningHadSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;

        }

        
        [Given(@"er is een actieve test waar de leerkracht de resultaten heeft getoond en de gebruiker stellingen heeft met een ander antwoord dan de gekozen partij")]
        public void GivenErIsEenActieveTestWaarDeLeerkrachtDeResultatenHeeftGetoondEnDeGebruikerStellingenHeeftMetEenAnderAntwoordDanDeGekozenPartij()
        {
       
            _scenarioContext.Pending();

        }
        
        [Given(@"er is een actieve stelling waar de leerkracht de resultaten heeft getoond en de gebruiker heeft geen stellingen met een ander antwoord dan de gekozen partij")]
        public void GivenErIsEenActieveTestWaarDeLeerkrachtDeResultatenHeeftGetoondEnDeGebruikerHeeftGeenStellingenMetEenAnderAntwoordDanDeGekozenPartij()
        {
            _scenarioContext.Pending();

        }
        
        [When(@"Gebruiker (.*) de stellingen wilt bekijken waar de gekozen partij een andere mening had")]
        public void WhenGebruikerDeStellingenWiltBekijkenWaarDeGekozenPartijEenAndereMeningHad(int p0)
        {
            _scenarioContext.Pending();
        }
        
        [When(@"Gebruiker (.*) drukt op “Bekijk foutieve stellingen”")]
        public void WhenGebruikerDruktOpBekijkFoutieveStellingen(int p0)
        {
            _scenarioContext.Pending();
        }
        
        [Then(@"Gebruiker (.*) krijgt de foutieve stelling te zien met opmerkingen van de partij")]
        public void ThenGebruikerKrijgtDeFoutieveStellingTeZienMetOpmerkingenVanDePartij(int p0)
        { 
            _scenarioContext.Pending();
        }
        
        [Then(@"Gebruiker (.*) krijgt de boodschap te zien dat hij/zij alles juist beantwoord heeft")]
        public void ThenGebruikerKrijgtDeBoodschapTeZienDatHijZijAllesJuistBeantwoordHeeft(int p0)
        {
            _scenarioContext.Pending();
        }
    }
}
