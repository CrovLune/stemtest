﻿Feature: GebruikerWilStellingenBekijkenWaarPartijAndereMeningHad
Als gebruiker
Wil ik ingaan op de stellingen van partijen
Zodat ik kan leren uit mijn fouten

@SuccesvolScenario
Scenario: Gebruiker bekijkt stellingen waar partij andere mening had
	Given er is een actieve test waar de leerkracht de resultaten heeft getoond en de gebruiker stellingen heeft met een ander antwoord dan de gekozen partij
	When  Gebruiker 2 de stellingen wilt bekijken waar de gekozen partij een andere mening had
	And Gebruiker 2 drukt op “Bekijk foutieve stellingen”
	Then Gebruiker 2 krijgt de foutieve stelling te zien met opmerkingen van de partij

@GebruikerHeeftGeenFoutieveStellingen
Scenario: Gebruiker heeft geen foutieve stellingen
	Given er is een actieve test waar de leerkracht de resultaten heeft getoond en de gebruiker heeft geen stellingen met een ander antwoord dan de gekozen partij 
	When  Gebruiker 2 de stellingen wilt bekijken waar de gekozen partij een andere mening had
	Then Gebruiker 2 krijgt de boodschap te zien dat hij/zij alles juist beantwoord heeft