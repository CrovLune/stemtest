﻿Feature: GebruikerBeantwoordStelling
Gebruiker vult stelling in

@succesvolScenario
Scenario: Vul stelling in
	Given er is een actieve sessie waarin een test wordt gevraagd met stellingen waaraan Gebruiker deel neemt
    When Gebruiker 2 wilt een stelling beantwoorden 
    And Gebruiker duidt zijn antwoord aan
    Then Gebruiker moet wachten tot leerkracht de volgende stelling start
