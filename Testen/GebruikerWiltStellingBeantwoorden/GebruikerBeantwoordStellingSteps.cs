﻿using ST.BL;
using ST.BL.Domain.Scholen;
using ST.BL.Domain.Users;
using TechTalk.SpecFlow;

namespace Testen.GebruikerWiltStellingBeantwoorden
{
    [Binding]
    public class GebruikerBeantwoordStellingSteps
    {

        private ScenarioContext _scenarioContext;
        private KlasManager _klasManager;

        /*
        private Gebruiker _leerkracht;
        private AnoniemeGebruiker _leerling;
        private AntwoordSessie _antwoordSessie;


        private Klas _klas;
        private SessieManager _sessieManager;
        private SpelSessie _spelSessie;
        private PartijSpel _partijSpel;
        private TestManager _testManager;
        private AntwoordSessieManager _antwoordSessieManager;
   //     private SpelManager _spelManager;*/

        public void GebruikerBeantwoordStelling(ScenarioContext scenarioContext)
        {
           _scenarioContext = scenarioContext;
    
        }

        [Given(@"er is een actieve sessie waarin een test wordt gevraagd met stellingen waaraan Gebruiker deel neemt")]
        public void GivenErIsEenActieveSessieWaarinEenTestWordtGevraagdMetStellingenWaaraanGebruikerDeelNeemt()
        {
            _scenarioContext.Pending();

        }
        
        [When(@"Gebruiker (.*) wilt een stelling beantwoorden")]
        public void WhenGebruikerWiltEenStellingBeantwoorden(int p0)
        {
            _scenarioContext.Pending();

        }
        
        [When(@"Gebruiker duidt zijn antwoord aan")]
        public void WhenGebruikerDuidtZijnAntwoordAan()
        {
            _scenarioContext.Pending();
        }
        
        [Then(@"Gebruiker moet wachten tot leerkracht de volgende stelling start")]
        public void ThenGebruikerMoetWachtenTotLeerkrachtDeVolgendeStellingStart()
        {
            _scenarioContext.Pending();
        }
    }
}
