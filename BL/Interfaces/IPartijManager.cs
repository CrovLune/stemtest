﻿using System.Collections.Generic;
using ST.BL.Domain.Partijen;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace ST.BL.Interfaces
{
    public interface IPartijManager

    {
        // Add Get Change Remove
        public Partij AddPartij(Partij partij);
        public IEnumerable<Partij> GetPartijen();
        public Partij GetPartijById(int partijId);
        public int ChangePartij(Partij partij);
        public int RemovePartij(Partij partij);
    }
}