﻿using System.Collections.Generic;
using ST.BL.Domain.Sessies;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace ST.BL.Interfaces
{
    public interface ISessieManager

    {
        // Add Get Change Remove
        public Sessie AddSessie(Sessie sessie);
        public IEnumerable<Sessie> GetSessies();
        public Sessie GetSessieById(int id);
        public int ChangeSessie(Sessie sessie);
        public int ChangeSessieRange(IEnumerable<Sessie> sessies);

        public int RemoveSessie(Sessie sessie);
    }
}