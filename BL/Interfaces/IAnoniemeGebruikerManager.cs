﻿using System.Collections.Generic;
using ST.BL.Domain.Users;

namespace ST.BL.Interfaces
{
    public interface IAnoniemeGebruikerManager
    {
        // Add Get Change Remove
        public AnoniemeGebruiker AddAnoniemeGebruiker(AnoniemeGebruiker anoniemeGebruiker);
        public AnoniemeGebruiker GetAnoniemeGebruikerById(int id);
        public IEnumerable<AnoniemeGebruiker> GetAnoniemeGebruikers();
        public int ChangeAnoniemeGebruiker(AnoniemeGebruiker anoniemeGebruiker);
        public int ChangeAnoniemeGebruikersRange(List<AnoniemeGebruiker> anoniemeGebruikers);
        public int RemoveAnoniemeGebruiker(AnoniemeGebruiker anoniemeGebruiker);
        public int RemoveAnoniemeGebruikesRange(List<AnoniemeGebruiker> anoniemeGebruikers);
    }
}