using System.Collections.Generic;
using ST.BL.Domain.Scholen;
using ST.BL.Domain.Users;

// ReSharper disable IdentifierTypo

namespace ST.BL.Interfaces
{
    public interface ISchoolManager
    {
        // Add Get Change Remove
        public School AddSchool(School school);
        public IEnumerable<School> GetScholen();
        public School GetSchoolById(int schoolId);
        public int ChangeSchool(School school);
        public int RemoveSchool(School school);
    }
}