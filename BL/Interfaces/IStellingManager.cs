﻿using System.Collections.Generic;
using ST.BL.Domain.Stellingen;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace ST.BL.Interfaces
{
    public interface IStellingManager

    {
        // Add Get Change Remove
        public Stelling AddStelling(Stelling stelling);
        public IEnumerable<Stelling> GetStellingen();
        public Stelling GetStellingById(int id);
        public int ChangeStelling(Stelling stelling);
        public int RemoveStelling(Stelling stelling);
    }
}