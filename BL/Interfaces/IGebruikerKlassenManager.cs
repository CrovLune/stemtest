﻿using System.Collections.Generic;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Domain.Users;

namespace ST.BL.Interfaces
{
    public interface IGebruikerKlassenManager
    {
        // Create Read Update Delete
        public GebruikerKlassen AddGebruikerKlassen(GebruikerKlassen gebruikerKlassen);

        public IEnumerable<GebruikerKlassen> GetGebruikerKlassen();

  

        public int ChangeGebruikerKlassen(GebruikerKlassen gebruikerKlassen);
        public int RemoveGebruikerKlassen(GebruikerKlassen gebruikerKlassen);
    }
}