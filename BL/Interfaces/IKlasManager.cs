﻿using System.Collections.Generic;
using ST.BL.Domain.Scholen;
using ST.BL.Domain.Users;
using ST.DAL;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace ST.BL.Interfaces
{
    public interface IKlasManager
    {
        // Add Get Change Remove
        public Klas AddKlas(Klas klas);
        public IEnumerable<Klas> GetKlasses();
        public Klas GetKlasById(int klasId);
        public int ChangeKlas(Klas klas);
        public int RemoveKlas(Klas klasId);
    }
}