﻿using System.Collections.Generic;
using ST.BL.Domain.InternMediateClasses;

namespace ST.BL.Interfaces
{
    public interface IKlasSpellenManager
    {
        public KlasSpellen AddKlasSpel(KlasSpellen klasSpel);
        public IEnumerable<KlasSpellen> GetKlasSpellen();
        public int ChangeKlasSpel(KlasSpellen klasSpel);
        public int ChangeKlasSpelRange(IEnumerable<KlasSpellen> klasSpellen);
        public int RemoveKlasSpel(KlasSpellen klasSpel);
    }
}