using System.Collections.Generic;
using ST.BL.Domain.Partijen;
using ST.BL.Domain.Spellen;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace ST.BL.Interfaces
{
   public interface ISpelManager
    {
        public Spel AddSpel(Spel spel);
        public IEnumerable<Spel> GetSpellen();
        public IEnumerable<Spel> GetPartijSpellen();
        public IEnumerable<Spel> GetDebatSpellen();
        public Spel GetSpelById(int id);
        public int ChangeSpel(Spel spel);
        public int RemoveSpel(Spel spel);
    }
}