﻿using System.Collections.Generic;
using ST.BL.Domain.InternMediateClasses;

namespace ST.BL.Interfaces
{
    public interface IGebruikerStellingenManager
    {
        public GebruikerStellingen AddGebruikerStelling(GebruikerStellingen gebruikerStelling);
        public IEnumerable<GebruikerStellingen> GetGebruikerStellingen();
        public int ChangeGebruikerStelling(GebruikerStellingen gebruikerStelling);
        public int ChangeGebruikerStellingRange(IEnumerable<GebruikerStellingen> gebruikerStellingen);

        public int RemoveGebruikerStelling(GebruikerStellingen gebruikerStelling);
        public int RemoveGebruikerStellingRange(IEnumerable<GebruikerStellingen> gebruikerStellingen);

    }
}