﻿using System.Collections.Generic;
using ST.BL.Domain.Identity;
using ST.BL.Domain.Users;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace ST.BL.Interfaces
{
    public interface IGebruikerManager
    {
        // Add Get Change Remove
        public Gebruiker AddGebruiker(Gebruiker gebruiker);
        public IEnumerable<Gebruiker> GetGebruikers();
        public Gebruiker GetGebruikerByIdentity(ApplicationUser user);

        public Gebruiker GetGebruikerById(int gebruikerId);
        public int ChangeGebruiker(Gebruiker gebruiker);
        public int RemoveGebruiker(Gebruiker gebruiker);
    }
}