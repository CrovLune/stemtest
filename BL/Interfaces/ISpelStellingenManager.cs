﻿using System.Collections.Generic;
using ST.BL.Domain.InternMediateClasses;

namespace ST.BL.Interfaces
{
    public interface ISpelStellingenManager
    {
        public SpelStellingen AddSpelStelling(SpelStellingen spelStellingen);
        public IEnumerable<SpelStellingen> GetSpelStellingen();
        public int ChangeSpelStelling(SpelStellingen spelStellingen);
        public int RemoveSpelStelling(SpelStellingen spelStellingen);

    }
}