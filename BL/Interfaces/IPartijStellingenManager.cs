﻿using System.Collections.Generic;
using ST.BL.Domain.InternMediateClasses;

namespace ST.BL.Interfaces
{
    public interface IPartijStellingenManager
    {
        public PartijStellingen AddPartijStelling(PartijStellingen partijStellingen);
        public IEnumerable<PartijStellingen> GetPartijStellingen();
        public int ChangePartijStelling(PartijStellingen partijStellingen);
        public int RemovePartijStelling(PartijStellingen partijStellingen);

    }
}