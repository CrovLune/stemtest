﻿using System.Collections.Generic;
using ST.BL.Domain.Antwoorden;

namespace ST.BL.Interfaces
{
    public interface IAntwoordenManager
    {
        public Antwoord AddAntwoord(Antwoord antwoord);
        public IEnumerable<Antwoord> GetAntwoorden();
        public IEnumerable<Antwoord> GetPartijAntwoorden();
        public IEnumerable<Antwoord> GetUserAntwoorden();

        public int ChangeAntwoord(Antwoord antwoord);
        public int ChangeAntwoordRange(List<Antwoord> antwoorden);

        public int RemoveAntwoord(Antwoord antwoord);
    }
}