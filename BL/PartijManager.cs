using System;
using System.Collections.Generic;
using ST.BL.Domain.Partijen;
using ST.BL.Interfaces;
using ST.DAL;
using ST.DAL.Interfaces;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo

namespace ST.BL
{
    public class PartijManager : IPartijManager
    {
        private readonly IPartijRepository _partijRepository;

        public PartijManager(IPartijRepository partijRepository)
        {
            _partijRepository = partijRepository;
        }

        //CRUD
        public Partij AddPartij(Partij partij)
        {
            return _partijRepository.CreatePartij(partij);
        }

        public IEnumerable<Partij> GetPartijen()
        {
            return _partijRepository.ReadPartijen();
        }

        public Partij GetPartijById(int partijId)
        {
            return _partijRepository.ReadPartijById(partijId);
        }

        public int ChangePartij(Partij partij)
        {
            return _partijRepository.UpdatePartij(partij);
        }

        public int RemovePartij(Partij partij)
        {
            return _partijRepository.DeletePartij(partij);
        }
    }
}