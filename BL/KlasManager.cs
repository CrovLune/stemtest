using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using ST.BL.Domain.Scholen;
using ST.BL.Domain.Users;
using ST.BL.Interfaces;
using ST.DAL;
using ST.DAL.Interfaces;

// ReSharper disable IdentifierTypo

namespace ST.BL
{
    public class KlasManager : IKlasManager
    {
        private readonly IKlasRepository _repo;

        public KlasManager(IKlasRepository klasRepository)
        {
            _repo = klasRepository;
        }

        public Klas AddKlas(Klas klas)
        {
            return _repo.CreateKlas(klas);
        }

        public IEnumerable<Klas> GetKlasses()
        {
            return _repo.ReadKlassen();
        }

        public Klas GetKlasById(int klasId)
        {
            return _repo.ReadKlasById(klasId);
        }

        public int ChangeKlas(Klas klas)
        {
            return _repo.UpdateKlas(klas);
        }

        public int RemoveKlas(Klas klas)
        {
            return _repo.DeleteKlas(klas);
        }
    }
}