﻿using System.Collections.Generic;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Interfaces;
using ST.DAL.Interfaces;

namespace ST.BL
{
    public class KlasSpellenManager : IKlasSpellenManager
    {
        private readonly IKlasSpellenRepository _repo;

        public KlasSpellenManager(IKlasSpellenRepository klasSpellenRepository)
        {
            _repo = klasSpellenRepository;
        }
        public KlasSpellen AddKlasSpel(KlasSpellen klasSpel)
        {
            return _repo.CreateKlasSpel(klasSpel);
        }

        public IEnumerable<KlasSpellen> GetKlasSpellen()
        {
            return _repo.ReadKlasSpellen();
        }

        public int ChangeKlasSpel(KlasSpellen klasSpel)
        {
            return _repo.UpdateKlasSpel(klasSpel);
        }

        public int ChangeKlasSpelRange(IEnumerable<KlasSpellen> klasSpellen)
        {
            return _repo.UpdateKlasSpelRange(klasSpellen);
        }

        public int RemoveKlasSpel(KlasSpellen klasSpel)
        {
            return _repo.DeleteKlasSpel(klasSpel);
        }
    }
}