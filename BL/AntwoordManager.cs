﻿using System.Collections.Generic;
using ST.BL.Domain.Antwoorden;
using ST.BL.Interfaces;
using ST.DAL.Interfaces;

namespace ST.BL
{
    public class AntwoordManager : IAntwoordenManager
    {
        private readonly IAntwoordRepository _repo;

        public AntwoordManager(IAntwoordRepository antwoordRepository)
        {
            _repo = antwoordRepository;
        }

        public Antwoord AddAntwoord(Antwoord antwoord)
        {
            return _repo.CreateAntwoord(antwoord);
        }

        public IEnumerable<Antwoord> GetAntwoorden()
        {
            return _repo.ReadAntwoorden();
        }

        public IEnumerable<Antwoord> GetPartijAntwoorden()
        {
            return _repo.ReadPartijAntwoorden();
        }

        public IEnumerable<Antwoord> GetUserAntwoorden()
        {
            return _repo.ReadUserAntwoorden();
        }

        public int ChangeAntwoord(Antwoord antwoord)
        {
            return _repo.UpdateAntwoord(antwoord);
        }

        public int ChangeAntwoordRange(List<Antwoord> antwoorden)
        {
            return _repo.UpdateAntwoordRange(antwoorden);
        }

        public int RemoveAntwoord(Antwoord antwoord)
        {
            return _repo.DeleteAntwoord(antwoord);
        }
    }
}