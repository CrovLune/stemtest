﻿using System.Collections.Generic;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Interfaces;
using ST.DAL.Interfaces;
// ReSharper disable IdentifierTypo

namespace ST.BL
{
    public class PartijStellingenManager : IPartijStellingenManager
    {
        private readonly IPartijStellingenRepository _repo;

        public PartijStellingenManager(IPartijStellingenRepository partijStellingenRepository)
        {
            _repo = partijStellingenRepository;
        }

        public PartijStellingen AddPartijStelling(PartijStellingen partijStellingen)
        {
            return _repo.CreatePartijStelling(partijStellingen);
        }

        public IEnumerable<PartijStellingen> GetPartijStellingen()
        {
            return _repo.ReadPartijStellingen();
        }

        public int ChangePartijStelling(PartijStellingen partijStellingen)
        {
            return _repo.UpdatePartijStellingen(partijStellingen);
        }

        public int RemovePartijStelling(PartijStellingen partijStellingen)
        {
            return _repo.DeletePartijStellingen(partijStellingen);
        }
    }
}