﻿using System.Collections.Generic;
using ST.BL.Domain.Users;
using ST.BL.Interfaces;
using ST.DAL;
using ST.DAL.Interfaces;

namespace ST.BL
{
    public class AnoniemeGebruikerManager : IAnoniemeGebruikerManager
    {
        private readonly IAnoniemeGebruikerRepository _repo;

        public AnoniemeGebruikerManager(IAnoniemeGebruikerRepository repository)
        {
            _repo = repository;
        }

        public AnoniemeGebruiker AddAnoniemeGebruiker(AnoniemeGebruiker anoniemeGebruiker)
        {
            return _repo.CreateAnoniemeGebruiker(anoniemeGebruiker);
        }

        public AnoniemeGebruiker GetAnoniemeGebruikerById(int id)
        {
            return _repo.ReadAnoniemeGebruikerById(id);
        }

        public IEnumerable<AnoniemeGebruiker> GetAnoniemeGebruikers()
        {
            return _repo.ReadAnoniemeGebruikers();
        }

        public int ChangeAnoniemeGebruiker(AnoniemeGebruiker anoniemeGebruiker)
        {
            return _repo.UpdateAnoniemeGebruiker(anoniemeGebruiker);
        }

        public int ChangeAnoniemeGebruikersRange(List<AnoniemeGebruiker> anoniemeGebruikers)
        {
            return _repo.UpdateAnoniemeGebruikersRange(anoniemeGebruikers);
        }

        public int RemoveAnoniemeGebruiker(AnoniemeGebruiker anoniemeGebruiker)
        {
            return _repo.DeleteAnoniemeGebruiker(anoniemeGebruiker);
        }

        public int RemoveAnoniemeGebruikesRange(List<AnoniemeGebruiker> anoniemeGebruikers)
        {
            return _repo.DeleteAnoniemeGebruikesRange(anoniemeGebruikers);
        }
    }
}