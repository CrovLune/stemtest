﻿// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo

using System.Collections.Generic;
using ST.BL.Domain.Sessies;
using ST.BL.Interfaces;
using ST.DAL.Interfaces;

namespace ST.BL
{
    public class SessieManager : ISessieManager
    {
        private readonly ISessieRepository _repo;

        public SessieManager(ISessieRepository sessieRepository)
        {
            _repo = sessieRepository;
        }

        public Sessie AddSessie(Sessie sessie)
        {
            return _repo.CreateSessie(sessie);
        }

        public IEnumerable<Sessie> GetSessies()
        {
            return _repo.ReadSessies();
        }

        public Sessie GetSessieById(int id)
        {
            return _repo.ReadSessieById(id);
        }

        public int ChangeSessie(Sessie sessie)
        {
            return _repo.UpdateSessie(sessie);
        }

        public int ChangeSessieRange(IEnumerable<Sessie> sessies)
        {
            return _repo.UpdateSessieRange(sessies);
        }

        public int RemoveSessie(Sessie sessie)
        {
            return _repo.DeleteSessie(sessie);
        }
    }
}