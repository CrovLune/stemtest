using System.Collections.Generic;
using ST.BL.Domain.Spellen;
using ST.BL.Interfaces;
using ST.DAL.Interfaces;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace ST.BL
{
    public class SpelManager : ISpelManager
    {
        private readonly ISpelRepository _repo;


        public SpelManager(ISpelRepository spelRepository)
        {
            _repo = spelRepository;
        }

        public Spel AddSpel(Spel spel)
        {
            return _repo.CreateSpel(spel);
        }

        public IEnumerable<Spel> GetSpellen()
        {
            return _repo.ReadSpellen();
        }

        public IEnumerable<Spel> GetPartijSpellen()
        {
            return _repo.ReadPartijSpellen();
        }

        public IEnumerable<Spel> GetDebatSpellen()
        {
            return _repo.ReadDebatSpellen();
        }

        public Spel GetSpelById(int id)
        {
            return _repo.ReadSpelById(id);
        }

        public int ChangeSpel(Spel spel)
        {
            return _repo.UpdateSpel(spel);
        }

        public int RemoveSpel(Spel spel)
        {
            return _repo.DeleteSpel(spel);
        }
    }
}