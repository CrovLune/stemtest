﻿using System.Collections.Generic;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Interfaces;
using ST.DAL.Interfaces;

namespace ST.BL
{
    public class SpelStellingenManager : ISpelStellingenManager
    {
        private readonly ISpelStellingenRepository _repo;

        public SpelStellingenManager(ISpelStellingenRepository repo)
        {
            _repo = repo;
        }

        public SpelStellingen AddSpelStelling(SpelStellingen spelStellingen)
        {
            return _repo.CreateSpelStelling(spelStellingen);
        }

        public IEnumerable<SpelStellingen> GetSpelStellingen()
        {
            return _repo.ReadSpelStellingen();
        }

        public int ChangeSpelStelling(SpelStellingen spelStellingen)
        {
            return _repo.UpdateSpelStelling(spelStellingen);
        }

        public int RemoveSpelStelling(SpelStellingen spelStellingen)
        {
            return _repo.DeleteSpelStelling(spelStellingen);
        }

    }
}