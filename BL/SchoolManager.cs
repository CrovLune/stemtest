using System.Collections.Generic;
using ST.BL.Domain.Scholen;
using ST.BL.Domain.Users;
using ST.BL.Interfaces;
using ST.DAL;
using ST.DAL.Interfaces;

// ReSharper disable IdentifierTypo

namespace ST.BL
{
    public class SchoolManager : ISchoolManager
    {
        private readonly ISchoolRepository _schoolRepo;

        public SchoolManager(ISchoolRepository schoolRepository)
        {
            _schoolRepo = schoolRepository;
        }

        public School AddSchool(School school)
        {
            return _schoolRepo.CreateSchool(school);
        }

        public IEnumerable<School> GetScholen()
        {
            return _schoolRepo.ReadScholen();
        }

        public School GetSchoolById(int schoolId)
        {
            return _schoolRepo.ReadSchoolById(schoolId);
        }

        public int ChangeSchool(School school)
        {
            return _schoolRepo.UpdateSchool(school);
        }


        public int RemoveSchool(School school)
        {
            return _schoolRepo.DeleteSchool(school);
        }
    }
}