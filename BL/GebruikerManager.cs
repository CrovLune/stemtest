using System.Collections.Generic;
using System.Linq;
using ST.BL.Domain.Identity;
using ST.BL.Domain.Users;
using ST.BL.Interfaces;
using ST.DAL.Interfaces;

// ReSharper disable IdentifierTypo

namespace ST.BL
{
    public class GebruikerManager : IGebruikerManager
    {
        private readonly IGebruikerRepository _repo;

        public GebruikerManager(IGebruikerRepository gebruikerRepository)
        {
            _repo = gebruikerRepository;
        }

        public Gebruiker AddGebruiker(Gebruiker gebruiker)
        {
            return _repo.CreateGebruiker(gebruiker);
        }

        public IEnumerable<Gebruiker> GetGebruikers()
        {
            return _repo.ReadGebruikers();
        }
        public Gebruiker GetGebruikerByIdentity(ApplicationUser user)
        {
            return _repo.ReadGebruikerByIdentity(user);
        }

        public Gebruiker GetGebruikerById(int gebruikerId)
        {
            return _repo.ReadGebruikerById(gebruikerId);
        }

        public int ChangeGebruiker(Gebruiker gebruiker)
        {
            return _repo.UpdateGebruiker(gebruiker);
        }

        public int RemoveGebruiker(Gebruiker gebruiker)
        {
            return _repo.DeleteGebruiker(gebruiker);
        }
    }
}