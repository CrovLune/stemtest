﻿using System.Collections.Generic;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Domain.Users;
using ST.BL.Interfaces;
using ST.DAL.Interfaces;

// ReSharper disable IdentifierTypo

namespace ST.BL
{
    public class GebruikerKlassenManager : IGebruikerKlassenManager
    {
        private readonly IGebruikerKlassenRepository _repo;

        public GebruikerKlassenManager(IGebruikerKlassenRepository repo)
        {
            _repo = repo;
        }

        public GebruikerKlassen AddGebruikerKlassen(GebruikerKlassen gebruikerKlassen)
        {
            return _repo.CreateGebruikerKlassen(gebruikerKlassen);
        }

        public IEnumerable<GebruikerKlassen> GetGebruikerKlassen()
        {
            return _repo.ReadGebruikerKlassen();
        }

        public int ChangeGebruikerKlassen(GebruikerKlassen gebruikerKlassen)
        {
            return _repo.UpdateGebruikerKlassen(gebruikerKlassen);
        }

        public int RemoveGebruikerKlassen(GebruikerKlassen gebruikerKlassen)
        {
            return _repo.DeleteGebruikerKlassen(gebruikerKlassen);
        }
    }
}