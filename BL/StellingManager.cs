﻿// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo

using System.Collections.Generic;
using ST.BL.Domain.Stellingen;
using ST.BL.Interfaces;
using ST.DAL.Interfaces;

namespace ST.BL
{
    public class StellingManager : IStellingManager
    {
        private readonly IStellingRepository _repo;

        public StellingManager(IStellingRepository stellingRepository)
        {
            _repo = stellingRepository;
        }

        public Stelling AddStelling(Stelling stelling)
        {
            return _repo.CreateStelling(stelling);
        }

        public IEnumerable<Stelling> GetStellingen()
        {
            return _repo.ReadStellingen();
        }

        public Stelling GetStellingById(int id)
        {
            return _repo.ReadStellingById(id);
        }

        public int ChangeStelling(Stelling stelling)
        {
            return _repo.UpdateStelling(stelling);
        }

        public int RemoveStelling(Stelling stelling)
        {
            return _repo.DeleteStelling(stelling);
        }
    }
}