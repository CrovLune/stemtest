﻿using System.Collections.Generic;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Interfaces;
using ST.DAL.Interfaces;
// ReSharper disable IdentifierTypo

namespace ST.BL
{
    public class GebruikerStellingenManager : IGebruikerStellingenManager
    {
        private readonly IGebruikerStellingenRepository _repo;

        public GebruikerStellingenManager(IGebruikerStellingenRepository gebruikerStellingenRepository)
        {
            _repo = gebruikerStellingenRepository;
        }

        public GebruikerStellingen AddGebruikerStelling(GebruikerStellingen gebruikerStelling)
        {
            return _repo.CreateGebruikerStelling(gebruikerStelling);
        }

        public IEnumerable<GebruikerStellingen> GetGebruikerStellingen()
        {
            return _repo.ReadGebruikerStellingen();
        }

        public int ChangeGebruikerStelling(GebruikerStellingen gebruikerStelling)
        {
            return _repo.UpdateGebruikerStelling(gebruikerStelling);
        }
        
        public int ChangeGebruikerStellingRange(IEnumerable<GebruikerStellingen> gebruikerStellingen)
        {
            return _repo.UpdateGebruikerStellingRange(gebruikerStellingen);
        }

        public int RemoveGebruikerStelling(GebruikerStellingen gebruikerStelling)
        {
            return _repo.DeleteGebruikerStelling(gebruikerStelling);
        }

        public int RemoveGebruikerStellingRange(IEnumerable<GebruikerStellingen> gebruikerStellingen)
        {
            return _repo.DeleteGebruikerStellingRange(gebruikerStellingen);
        }
    }
}