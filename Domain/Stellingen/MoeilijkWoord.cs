﻿// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo

namespace ST.BL.Domain.Stellingen
{
    public class MoeilijkWoord
    {
        public int Id { get; set; }
        public string Woord { get; set; }
        public string Uitleg { get; set; }
        public bool Selected { get; set; }
        public virtual Stelling Stelling { get; set; }
    }
}