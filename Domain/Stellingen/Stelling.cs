﻿// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo

using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using ST.BL.Domain.Antwoorden;
using ST.BL.Domain.InternMediateClasses;
using Newtonsoft.Json;

namespace ST.BL.Domain.Stellingen
{
    public class Stelling
    {
        public int Id { get; set; }
        public string StellingOnderwerp { get; set; }
        public string Vraag { get; set; }
        public string Uitleg { get; set; }
        [JsonIgnore]
        public Status Status { get; set; }
        [JsonIgnore]
        public virtual ICollection<MoeilijkWoord> MoeilijkeWoorden { get; set; }
        [JsonIgnore]
        public virtual ICollection<GebruikerStellingen> GebruikerStellingen { get; set; }
        [JsonIgnore]
        public virtual ICollection<PartijStellingen> PartijStellingen { get; set; }
        [JsonIgnore]
        public virtual ICollection<UserAntwoordOpStelling> UserAntwoordOpStelling { get; set; }
        [JsonIgnore]
        public virtual ICollection<PartijAntwoordOpStelling> PartijAntwoordOpStelling { get; set; }
        [JsonIgnore]
        public virtual ICollection<SpelStellingen> SpelStellingen { get; set; }
    }
}