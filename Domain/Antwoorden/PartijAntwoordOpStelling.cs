﻿using ST.BL.Domain.Partijen;
using ST.BL.Domain.Stellingen;

namespace ST.BL.Domain.Antwoorden
{
    public class PartijAntwoordOpStelling : Antwoord
    {
        public string Antwoord { get; set; }
        public string Uitleg { get; set; }
        public virtual Partij Partij { get; set; }
        public virtual Stelling Stelling { get; set; }
    }
}