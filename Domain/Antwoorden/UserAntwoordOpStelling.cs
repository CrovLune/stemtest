﻿using ST.BL.Domain.Stellingen;
using ST.BL.Domain.Users;
using Newtonsoft.Json;

namespace ST.BL.Domain.Antwoorden
{
    public class UserAntwoordOpStelling : Antwoord
    {
        public string Antwoord { get; set; }
        public string Argument { get; set; }
        public int StellingId { get; set; }
        [JsonIgnore]
        public virtual AnoniemeGebruiker AnoniemeGebruiker { get; set; }
        public virtual Stelling Stelling { get; set; }
    }
}