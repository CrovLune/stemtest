﻿using System.Collections.Generic;
using ST.BL.Domain.Users;

namespace ST.BL.Domain.Scholen
{
    public class School
    {
        public int Id { get; set; }
        public string Naam { get; set; }
        public virtual Adres Adres { get; set; }
        public string MailContactpersoon { get; set; }
        public virtual ICollection<Gebruiker> Gebruikers { get; set; }
        public virtual ICollection<Klas> Klassen { get; set; }
    }
}