﻿// ReSharper disable IdentifierTypo
// ReSharper disable StringLiteralTypo

namespace ST.BL.Domain.Scholen
{
    public class Adres
    {
        // [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string StraatNaam { get; set; }
        public int HuisNr { get; set; }
        public int BusNr { get; set; }
        public int Postcode { get; set; }
        public string Land { get; set; }
        
        public Adres(string straatNaam, int huisNr, int busNr, int postcode, string land)
        {
            StraatNaam = straatNaam;
            HuisNr = huisNr;
            BusNr = busNr;
            Postcode = postcode;
            Land = land;
        }
        public Adres()
        {
            StraatNaam = "Onbekend";
            HuisNr = 0;
            BusNr = 0;
            Postcode = 0;
            Land = "Onbekend";
        }

        public override string ToString()
        {
            return string.Format($"{StraatNaam} {HuisNr}/{BusNr}, {Postcode} {Land}");
        }
    }
}