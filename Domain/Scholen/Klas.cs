﻿using System.Collections.Generic;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Domain.Sessies;

namespace ST.BL.Domain.Scholen
{
    public class Klas
    {
        public int Id { get; set; }
        public string Naam { get; set; }
        public int AantalLeerlingen { get; set; }
        public int StudieJaar { get; set; }
        public virtual School School { get; set; }
        public virtual ICollection<GebruikerKlassen> GebruikerKlassen { get; set; }
        public virtual ICollection<Sessie> Sessies { get; set; }
        public virtual ICollection<KlasSpellen> KlasSpellen { get; set; }
    }
}