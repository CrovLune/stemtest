﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using ST.BL.Domain.InternMediateClasses;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace ST.BL.Domain.Partijen
{
    public class Partij
    {
        public int Id { get; set; }
        public string Naam { get; set; }
        public string VolledigeNaam { get; set; }
        public string Logo { get; set; }
        public string Beschrijving { get; set; }
        public string VisieOmschrijving { get; set; }
        public virtual PartijInfo PartijInfo { get; set; }
        public virtual List<PartijStellingen> PartijStellingen { get; set; }
    }
}