﻿// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace ST.BL.Domain.Partijen
{
    public class PartijInfo
    {
        public int Id { get; set; }
        public string Info { get; set; }
        public string Video { get; set; }
    }
}