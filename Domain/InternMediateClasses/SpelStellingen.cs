﻿using ST.BL.Domain.Spellen;
using ST.BL.Domain.Stellingen;

namespace ST.BL.Domain.InternMediateClasses
{
    public class SpelStellingen
    {
        public int SpelId { get; set; }
        public virtual Spel Spel { get; set; }
        public int StellingId { get; set; }
        public virtual Stelling Stelling { get; set; }
        public Status Status { get; set; }

        public bool Selected { get; set; }
    }
}