﻿using ST.BL.Domain.Scholen;
using ST.BL.Domain.Users;

namespace ST.BL.Domain.InternMediateClasses
{
    public class GebruikerKlassen
    {
        public int GebruikerId { get; set; }
        public virtual Gebruiker Gebruiker { get; set; }

        public int KlasId { get; set; }
        public virtual Klas klas { get; set; }
    }
}