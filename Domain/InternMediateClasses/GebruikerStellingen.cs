﻿using ST.BL.Domain.Spellen;
using ST.BL.Domain.Stellingen;
using ST.BL.Domain.Users;

namespace ST.BL.Domain.InternMediateClasses
{
    public class GebruikerStellingen
    {
        public int StellingId { get; set; }
        public virtual Stelling Stelling { get; set; }
        
        public int GebruikerId { get; set; }
        public virtual Gebruiker Gebruiker { get; set; }

        public bool Selected { get; set; }
    }
}