﻿using ST.BL.Domain.Partijen;
using ST.BL.Domain.Stellingen;

namespace ST.BL.Domain.InternMediateClasses
{
    public class PartijStellingen
    {
        public int PartijId { get; set; }
        public virtual Partij Partij { get; set; }

        public int StellingId { get; set; }
        public virtual Stelling Stelling { get; set; }
    }
}