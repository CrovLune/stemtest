﻿using ST.BL.Domain.Scholen;
using ST.BL.Domain.Spellen;

namespace ST.BL.Domain.InternMediateClasses
{
    public class KlasSpellen
    {
        public int KlasId { get; set; }
        public virtual Klas Klas { get; set; }
        
        public int SpelId { get; set; }
        public virtual Spel Spel { get; set; }
        
        public bool Selected { get; set; }
    }
}