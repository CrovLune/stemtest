﻿using Microsoft.AspNetCore.Identity;
using ST.BL.Domain.Users;


namespace ST.BL.Domain.Identity
{
    public class ApplicationUser : IdentityUser<int>
    {
        public string FirstName { get; set; }
        public virtual Gebruiker Gebruiker { get; set; }
    }
}