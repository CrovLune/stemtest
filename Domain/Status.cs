﻿// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace ST.BL.Domain
{
    public enum Status : byte
    {
        Open=0,
        Closed,
        Pending
    }
}