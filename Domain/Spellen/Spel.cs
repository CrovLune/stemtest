﻿using System.Collections.Generic;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Domain.Stellingen;
using ST.BL.Domain.Users;

namespace ST.BL.Domain.Spellen
{
    public class Spel
    {
        public string Name { get; set; }
        public int SpelId { get; set; }
        public Status Status { get; set; }
        public virtual Gebruiker Creator { get; set; }
        public virtual Options options { get; set; }
         
        public virtual IList<Stelling> Stellingen { get; set; }
        public virtual ICollection<KlasSpellen> KlasSpellen { get; set; }
        
        public virtual ICollection<SpelStellingen> SpelStellingen { get; set; }
        
    }
}