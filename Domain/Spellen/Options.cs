﻿using System.ComponentModel.DataAnnotations;

namespace ST.BL.Domain.Spellen
{
    public class Options
    {
        public int Id { get; set; }
        
        [Display(Name = "Color The Screen Post Answer")]
        public bool ColorScreenPostAnswer { get; set; } = false;
        
        [Display(Name = "Allow Argument Input")]
        public bool AllowArgumentInput { get; set; } = false;
        
        [Display(Name = "Explain Difficult Words")]
        public bool ExplainDifficultWord { get; set; } = false;
        
        [Display(Name = "Skip Questions")]
        public bool Skip { get; set; } = false;
        
    }
}