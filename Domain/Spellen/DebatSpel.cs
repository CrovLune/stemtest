﻿// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo

using System.Collections.Generic;
using ST.BL.Domain.Stellingen;

namespace ST.BL.Domain.Spellen
{
    public class DebatSpel : Spel
    {
        public string UserArgument { get; set; }
    }
}