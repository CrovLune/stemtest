﻿﻿using System.Collections.Generic;
using ST.BL.Domain.Antwoorden;
using ST.BL.Domain.Partijen;
 using ST.BL.Domain.Sessies;
 namespace ST.BL.Domain.Users
{
    public class AnoniemeGebruiker
    {
        public int Id { get; set; }
        public string Cookie { get; set; }
        public string GroupName { get; set; }
        public virtual Sessie Sessie { get; set; }
      
        public virtual Partij Partij { get; set; }
        public virtual ICollection<UserAntwoordOpStelling> AntwoordenOpStellingen { get; set; }
    }
}