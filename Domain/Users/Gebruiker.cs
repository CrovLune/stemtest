﻿using System.Collections.Generic;
using ST.BL.Domain.Identity;
using ST.BL.Domain.InternMediateClasses;
using ST.BL.Domain.Scholen;
using ST.BL.Domain.Sessies;
using ST.BL.Domain.Spellen;

namespace ST.BL.Domain.Users
{
    public class Gebruiker
    {
        public int Id { get; set; }
        public string Username { get; set; }

        public string Voornaam { get; set; }
        public string Achternaam { get; set; }

        public string Email { get; set; }

        public virtual School School { get; set; }

        public virtual ICollection<GebruikerKlassen> GebruikerKlassen { get; set; }
        public virtual ICollection<Spel> Spellen { get; set; }
        public virtual ICollection<Sessie> Sessies { get; set; }
        public virtual ICollection<GebruikerStellingen> GebruikerStellingen { get; set; }
        public virtual ApplicationUser Identity { get; set; }
    }
}