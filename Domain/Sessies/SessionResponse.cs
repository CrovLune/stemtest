﻿using ST.BL.Domain.Spellen;
using System;
using System.Collections.Generic;
using System.Text;

namespace ST.BL.Domain.Sessies
{
    public class SessionResponse
    {
        public int SessionID { get; set; } = -1;
        public string SessionCookie { get; set; } = "";
        public string SessionType { get; set; } = "";
        public Options SessionOptions { get; set; } = new Options();
        public bool IsValidSession { get; set; } = false;
    }
}
