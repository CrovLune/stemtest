﻿using System.Collections;
using System.Collections.Generic;
using ST.BL.Domain.Scholen;
using ST.BL.Domain.Spellen;
using ST.BL.Domain.Users;

// ReSharper disable IdentifierTypo
// ReSharper disable CommentTypo
// ReSharper disable StringLiteralTypo
namespace ST.BL.Domain.Sessies
{
    public class Sessie
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string UniqueCode { get; set; }
        public int AantalDeelnemers { get; set; }
        public Status Status { get; set; }
        public virtual Spel Spel { get; set; }
        public virtual Klas Klas { get; set; }
        public virtual Gebruiker Creator { get; set; }
        public virtual ICollection<AnoniemeGebruiker> AnoniemeGebruikers { get; set; }
    }
}