﻿using ST.BL.Domain.Partijen;
using System;
using System.Collections.Generic;
using System.Text;

namespace ST.BL.Domain.Sessies
{
    public class PartijScoreModel
    {
        public Partij Partij { get; set; }
        public int Score { get; set; }
    }
}
