﻿using ST.BL.Domain.Stellingen;
using System;
using System.Collections.Generic;
using System.Text;

namespace ST.BL.Domain.Sessies
{
    public class SessionPartijResult
    {
        public Sessie Sessie { get; set; }
        public List<StellingPartijResultModel> StellingModels;
        public int Score;
    }
}
