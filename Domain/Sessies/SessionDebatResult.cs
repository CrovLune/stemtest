﻿using ST.BL.Domain.Stellingen;
using System;
using System.Collections.Generic;
using System.Text;

namespace ST.BL.Domain.Sessies
{
    public class SessionDebatResult
    {
        public Sessie Sessie { get; set; }
        public List<StellingDebatResultModel> StellingModels { get; set; }
        public List<PartijScoreModel> PartijScoreModels;
    }
}
