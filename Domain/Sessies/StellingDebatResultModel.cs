﻿using ST.BL.Domain.Antwoorden;
using System;
using System.Collections.Generic;
using System.Text;

namespace ST.BL.Domain.Stellingen
{
    public class StellingDebatResultModel
    {
        public Stelling Stelling { get; set; }
        public UserAntwoordOpStelling UserAntwoordOpStelling { get; set; }
    }
}
